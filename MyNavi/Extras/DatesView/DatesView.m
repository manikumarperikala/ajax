//
//  DatesView.m
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "DatesView.h"

@implementation DatesView
- (void)loadData{
    
    _fromDate.layer.cornerRadius = 5.0f;
    _fromDate.layer.masksToBounds = YES;
    _fromDate.layer.borderColor = [[UIColor grayColor]CGColor];
    _fromDate.layer.borderWidth = 1.0f;
    
    _toDate.layer.cornerRadius = 5.0f;
    _toDate.layer.masksToBounds = YES;
    _toDate.layer.borderColor = [[UIColor grayColor]CGColor];
    _toDate.layer.borderWidth = 1.0f;
}

@end
