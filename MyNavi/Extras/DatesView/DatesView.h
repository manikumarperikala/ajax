//
//  DatesView.h
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatesView : UIView
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIView *fromDate;
@property (weak, nonatomic) IBOutlet UIView *toDate;

- (void)loadData;

@end
