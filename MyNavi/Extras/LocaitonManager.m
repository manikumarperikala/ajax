//
//  LocaitonManager.m
//  eRakshaPolice
//
//  Created by SridharReddy on 16/09/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import "LocaitonManager.h"

@implementation LocaitonManager
@synthesize currentLocation;

+ (LocaitonManager *)sharedObj
{
    static LocaitonManager *sharedObj = nil;
    if(sharedObj == nil)
    {
        sharedObj = [[self alloc] init];
    }
    return sharedObj;
}
- (id)init {
    self = [super init];
    if(self != nil) {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.activityType = CLActivityTypeOtherNavigation;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        
#ifdef __IPHONE_8_0
        if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1)
        {
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [_locationManager requestWhenInUseAuthorization];
            }
            [_locationManager startUpdatingLocation];
        }
        else
#endif
        {
            [_locationManager startUpdatingLocation];
        }
    }
    return self;
}
- (void)startUpdateLocation
{
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdateLocation
{
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            [self.locationManager startUpdatingLocation];
        } break;
        case kCLAuthorizationStatusDenied: {
            [self.locationManager startUpdatingLocation];
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [self.locationManager startUpdatingLocation];
            
        } break;
        default:
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray*)locations
{
    CLLocation *location = [locations lastObject];
    self.currentLocation = location;
}

@end
