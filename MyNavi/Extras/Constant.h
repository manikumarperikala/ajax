//
//  Constant.h
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


#define controls  (CustomControls*)[CustomControls sharedObj]
#define APP_DELEGATE  ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define kUserDefaults      [NSUserDefaults standardUserDefaults]
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define ServerCompanyID @"AJAXFIORI"
#define KToken @"token"
#define KFirstName @"firstName"
#define KLastName @"lastName"
#define KPhoneNumber @"phone"
#define KEmail @"email"
#define KID @"id"
#define KRoleID @"useType"
#define KLoginName @"loginName"
#define Kcbctranspose @"cbctranspose"
#define KdealerName @"userDealer"

#define kApplicationId @"applicationId"
#define kGet @"GET"
#define kPost @"POST"

#define NavigationBarColor [UIColor colorWithRed:29.0/255.0 green:9.0/255.0 blue:92.0/255.0 alpha:1]



#define KGoogleApiKey @"AIzaSyCudS84V9WUsFJwOx11cl0137nK1Ct-h7Y"
#define KGoogleWebServiceApiKey @"AIzaSyCLwT7DN9nsAulgaXCOV5JVXt-9WYQPWD4"


// Here Geocode SDK Keys
#define kHereMapAppID @"J7lmc7qEJi2l2Q0mtwbG"
#define kHereMapAppCode @"zY_YdRgXM5IFQkdumzoyPA"


//#define KBaseURL @"http://mynavi-dev.trakitnow.com/"
// Production URL
#define KBaseURL @"https://mynavi-ajaxfiori.trakitnow.com"
// Demo URL
//#define KBaseURL @"https://mynavi-ajaxfiori-demo.trakitnow.com"
//#define KBaseURL @"https://sonalika-demo.trakitnow.com"

// API

#define Login @"/auth/myaccount/login"

#define VehiclesList @"/mynavi/dvmap/list?customerCode=%@&limit=1000&page=1"

#define DealerVehiclesList @"/mynavi/dvmap/list?dealerCode=%@&limit=1000&page=1"

#define kGeocodelocation @"/mynavi/getLocationName?lat=%@&lng=%@"

#define EngineHours @"/mynavi/enginehours?deviceID=%@&endDate=%@&limit=20&page=%d&startDate=%@&sortBy=onTimestamp"

#define Reports @"/mynavi/%@?deviceID=%@&endDate=%@&limit=15&page=%d&sortBy=createdAt&startDate=%@"

#define Notifications @"/mynavi/notification/list?sortBy=devicePublishTime?deviceID=%@"

#define Heartbeat @"/mynavi/heartbeat?deviceID=%@&endDate=%@&limit=100&page= &sortBy=devicePublishTime&startDate=%@&extras.ignitionStatus=1"

#define LocationHistory @"/mynavi/heartbeat?deviceID=%@&endDate=%@&limit=100&page= &sortBy=devicePublishTime&startDate=%@"

#define Analytics @"/mynavi/findFuelconsumption?endDate=%@&limit=10&page=%d&sortBy=createdAt&startDate=%@&deviceID=%@"

#define BatchReport @"/mynavi/findBatchreport?endDate=%@&limit=10&page=%d&sortBy=createdAt&startDate=%@&deviceID=%@"

#define ConsumptionReport @"/mynavi/findConsumptionreport?endDate=%@&limit=10&page=%d&sortBy=createdAt&startDate=%@&deviceID=%@"

#define GooglePlacesApi @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=petrol%20bunks&key=%@&location=%f,%f&radius=2000"

#define DeviceDataApi @"/mynavi/devicedata"

#define CustomerDashboardApi @"/mynavi/upcomingservice?deviceModel=%@&customerCode=%@"

/*#define CustomersApi @"/auth/user/edit?active=true&extras.dealerID=%@&limit=100&page=1&roleCodes=CUSTOMER"*/
#define CustomersApi @"/auth/user/list?useType=CUSTOMER&userDealer=%@"

/*#define UpcomingserviceApi @"/mynavi/upcomingservice?customerID=&dealerID=%@&deviceModel=%@"*/
#define UpcomingserviceApi @"/mynavi/upcomingservice?dealerCode=%@&deviceModel=%@"

#define VehicleParkApi @"/mynavi/dashboard?dealerCode=%@&limit=1000"

#define ServiceNotifications @"/mynavi/servicedone/list?serviceEngineer=%@"

#define ServiceUpdate @"/mynavi/servicedone"

#define ServiceDetails @"/mynavi/servicedone/list?id=%@"

#define CommissioningAllocation @"/mynavi/comissioninglist?engineerID=%@&iscomisssionDoneStatus=%@"

#define CommissionedVehicles @"/mynavi/comissioninglist"

#define RemovePushtoken @"/mynavi/removePushToken"

#define SummaryReport @"/mynavi/deviceWise/hourlyReportList?pinno=%@&endDate=%@&startDate=%@&type=%@&outputFields[id]=1&outputFields[hour]=1&outputFields[day]=1&outputFields[month]=1&outputFields[year]=1&outputFields[deviceID]&outputFields[engineHours]=1&outputFields[coolantTemp]=1&outputFields[opsStatus]=1&outputFields[fuelCosumption]=1&outputFields[totalDistance]=1&outputFields[rpm]=1&outputFields[batteryLevel]=1&outputFields[seconds]=1&outputFields[rpmTime]=1&outputFields[coolantTempTime]=1&outputFields[batteryLevelTime]=1"
#define SummaryReportDay @"/mynavi/deviceWise/hourlyReportList?pinno=%@&endDate=%@&startDate=%@&&outputFields[id]=1&outputFields[hour]=1&outputFields[day]=1&outputFields[month]=1&outputFields[year]=1&outputFields[deviceID]&outputFields[engineHours]=1&outputFields[coolantTemp]=1&outputFields[opsStatus]=1&outputFields[fuelCosumption]=1&outputFields[totalDistance]=1&outputFields[rpm]=1&outputFields[batteryLevel]=1&outputFields[seconds]=1&outputFields[rpmTime]=1&outputFields[coolantTempTime]=1&outputFields[batteryLevelTime]=1"

#define kNormalColor  [UIColor colorWithRed:205.0/255.0 green:14.0/255.0 blue:24.0/255.0 alpha:1.0]

#define kHighlitedColor  [UIColor colorWithRed:149.0/255.0 green:0.0/255.0 blue:10.0/255.0 alpha:1.0]

#define kNavigationBarColor [UIColor colorWithRed:22.0f/255.0f green:0.0f/255.0f blue:73.0f/255.0f alpha:1.0f]

#define kCustomerID @"CUSTOMER"
#define kDealerID @"DEALER"
#define kServiceID @"5"
#define kClientID @"4"

#define kMainDashboard @"Main"
#define kDealerDashboard @"Dashboard"
#define kDealerIdentifier @"DashboardView"
#define kCustomerDashboard @"CustomerDashboard"
#define kCustomerIdentifier @"CustomerDashboardView"
#define kServiceManDashboard @"ServiceMan"
#define kServiceManIdentifier @"ServiceManDashboardVC"
#define kClientDashboard @"ClientDashboard"
#define kClientIdentifier @"ClientDashboardView"
#define kServiceDone @"1"
#define kServicePending @"0"
#define kDetail @"ServiceManDetail"
#define kUpdate @"ServiceUpdate"

#define kClusterUrl = @"http://52.66.184.140:3609/#!/?vehicleNumber=%@&companyID=%@&token=%@&APIurl=ajaxfiori-web.trakitnow.com/mynavi/cluster"

#define kServiceApi = @"/mynavi/servicedone/list"

//NSString *isArgo2500 = @"false";

#endif /* Constant_h */
