//
//  HttpManager.h
//  HttpManager
//
//  Created by Mac Book on 21/11/16.
//  Copyright (c) 2016 TrakitNow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^completionBlock)(id responce, NSError *error);

@interface HttpManager : NSObject

+ (void)apiDetails:(id)parameter serviceName:(NSString *)serviceName  httpType:(NSString *)type WithBlock:(completionBlock)block;

+ (void)loginApiAuthorization:(NSString *)credentials  WithBlock:(completionBlock)block;

@end
