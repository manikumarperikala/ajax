//
//  DatePicker.m
//  MyNavi
//
//  Created by sridhar on 26/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "DatePicker.h"
#import "TabBarSingleTon.h"

@implementation DatePicker

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit:frame];
    }
    return self;
}

- (void)baseInit:(CGRect)frame {
    
    NSDate *date = [NSDate date];
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 64, self.frame.size.width, self.frame.size.height-64)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = NO;
    datePicker.maximumDate = [NSDate date];
    datePicker.date = date;
    
    
    NSDateFormatter *displayFormatter = [[NSDateFormatter alloc] init];
    [displayFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [displayFormatter setDateFormat:@"MM月dd日 EEE HH:mm"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 64)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    pickerToolbar.barTintColor = [UIColor redColor];
    [pickerToolbar sizeToFit];
    
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(datePickerDoneClick)];
    doneBtn.tintColor = [UIColor whiteColor];
    
    NSArray *itemArray = [[NSArray alloc] initWithObjects: flexSpace, doneBtn, nil];
    
    [pickerToolbar setItems:itemArray animated:YES];
    
    [self addSubview:pickerToolbar];
    [self addSubview:datePicker];
    self.backgroundColor = [UIColor whiteColor];
}

-(void)setPickerWithTag:(int)tag
{
    datePicker.tag = tag;
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateformatter setTimeZone:[NSTimeZone localTimeZone]];
    if(tag == 11)
    {
       
        if([TabBarSingleTon.sharedObj startDate] != nil)
        {
            
            NSDate *fromDate = [dateformatter dateFromString:[TabBarSingleTon.sharedObj startDate]];
             datePicker.date = [dateformatter dateFromString:[TabBarSingleTon.sharedObj endDate]];

            datePicker.minimumDate = fromDate;
        }
    }
    if(tag == 12)
    {
        NSDate *date = [NSDate date];
        datePicker.maximumDate = date;
    }
    else
    {
        if([TabBarSingleTon.sharedObj endDate] != nil)
        {
            NSDate *toDate = [dateformatter dateFromString:[TabBarSingleTon.sharedObj endDate]];
            datePicker.date = [dateformatter dateFromString:[TabBarSingleTon.sharedObj startDate]];
            
            datePicker.maximumDate = toDate;
        }
 
    }
    
        
}
-(void)datePickerDoneClick
{
    NSDate *selectedDate = datePicker.date;
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:selectedDate];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    if([today day] == [otherDay day] &&
       [today month] == [otherDay month] &&
       [today year] == [otherDay year] &&
       [today era] == [otherDay era]) {
        if(_selectedButton.tag == 100)
        {
            selectedDate = [self beginningOfDay:[NSDate date]];
        }
        else
            selectedDate=[NSDate date];
    }
    if([_delegate respondsToSelector:@selector(datePickerSelectedDate: forTag:)])
    {
        [self.delegate datePickerSelectedDate:selectedDate forTag:(int)datePicker.tag];
    }
    [self removeFromSuperview];

}

-(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
    return [cal dateFromComponents:components];
    
}

@end
