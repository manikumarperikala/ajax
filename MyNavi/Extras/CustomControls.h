//
//  CustomControls.h
//  Kikr
//
//  Created by Mobikasa on 11/17/14.
//  Copyright (c) 2014 mobikasa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CustomControls : NSObject

+ (CustomControls *)sharedObj;

-(UIBarButtonItem*)rightBarButtonMethod:(NSString*)ImgName withTitle:(NSString*)title selector:(SEL)selector delegate:(id)del;
-(UILabel*)createLabelFrame:(CGRect)frame  withTitle:(NSString*)title  withFont:(UIFont*)font textColor:(UIColor*)color;
-(NSArray*)leftBarButtonMethod:(NSString*)ImgName selector:(SEL)selector delegate:(id)del;
-(void)presentAlertControllerWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegatee block:(void (^)(BOOL didCancel))response;
-(void)presentAlertController:(NSString *)message delegate:(id)delegate;
-(void)saveUserInfo:(NSDictionary *)dict;
-(void)showAlert:(NSString *)message;
-(NSString *)endOfDay:(NSDate *)date;
-(NSString *)beginningOfDay:(NSDate *)date;
-(NSString *)getCurrentDate;
-(NSString *)getCurrentDateminusfiveHours;
-(NSString *)addFiveHoursthirtymins:(NSString *)sender;
-(NSString *)getDate:(NSString *)sender;
-(NSString *)getFormattedDate:(NSString*)sender;
-(NSString *)beggingOfMonth:(NSDate *)date;
-(NSString *)endOfMonth:(NSDate *)date;
-(NSString *)beggingOfYear:(NSDate *)date;
-(NSString *)endOfYear:(NSDate *)date;

@end
