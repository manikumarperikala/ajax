//
//  Vehiclesview.h
//  MyNavi
//
//  Created by TrakitNow on 9/18/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedVehicle

-(void)selectedVehicleNumber:(NSString*)number latitude:(NSString *)lat longitude:(NSString *)lng;
-(void)removeView;

@end

@interface Vehicleslistview : UIView<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong) NSMutableArray *vehicleslist;
@property(nonatomic,assign) id<SelectedVehicle>listdelegate;
@end


