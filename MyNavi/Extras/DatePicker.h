//
//  DatePicker.h
//  MyNavi
//
//  Created by sridhar on 26/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerProtocal <NSObject>

-(void)datePickerSelectedDate:(NSDate *)sender forTag:(int)tag;

@end

@interface DatePicker : UIView
{
    UIDatePicker *datePicker;
}
@property(nonatomic,strong)UIView *selectedButton;

-(void)setPickerWithTag:(int)tag;

@property(assign)id<DatePickerProtocal> delegate;

@end
