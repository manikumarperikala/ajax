//
//  LocaitonManager.h
//  AjaxFiori
//
//  Created by SridharReddy on 16/09/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^locationAdddress)(NSString *address);

@interface LocaitonManager : NSObject<CLLocationManagerDelegate>

@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,strong)CLLocation *currentLocation;

+ (LocaitonManager *)sharedObj;

- (void)stopUpdateLocation;
- (void)startUpdateLocation;

@end
