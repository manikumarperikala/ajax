//
//  CustomPickerView.m
//  Locale
//
//  Created by sridhar on 12/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CustomPickerView.h"

@implementation CustomPickerView
@synthesize categoryPickerView,pickerSelectedRow;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit:frame];
    }
    return self;
}

- (void)baseInit:(CGRect)frame {
    
    [self setBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1.0]];
    
    categoryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 64, self.frame.size.width, self.frame.size.height-64)];
    
    categoryPickerView.backgroundColor = [UIColor groupTableViewBackgroundColor];

    [categoryPickerView setDelegate: self];
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 64)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    pickerToolbar.barTintColor = [UIColor redColor];

    [pickerToolbar sizeToFit];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(categoryDoneButtonPressed)];
    doneBtn.tintColor = [UIColor whiteColor];

    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(categoryCancelButtonPressed)];
    cancelBtn.tintColor = [UIColor whiteColor];

    
    [pickerToolbar setItems:@[cancelBtn, flexSpace, doneBtn] animated:YES];
    
    [self addSubview:pickerToolbar];
    [self addSubview:categoryPickerView];
}
-(void)setSelectedValue:(int)row
{
    [categoryPickerView selectRow:row inComponent:0 animated:true];
}
-(void)categoryDoneButtonPressed{
    if([_delegate respondsToSelector:@selector(pickerViewSelectedValue: forRow:)])
    {
        [self.delegate pickerViewSelectedValue:pickerSelectedString forRow:pickerSelectedRow];
    }

    [self removeFromSuperview];
}
-(void)categoryCancelButtonPressed{
    
    [self removeFromSuperview];
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(_dataArray.count)
        pickerSelectedString = [_dataArray objectAtIndex:0];
    return [_dataArray count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
   
    return [_dataArray objectAtIndex: row];
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
//    if(row)
//    {
            pickerSelectedString = [_dataArray objectAtIndex:row];
            pickerSelectedRow = (int)row;
//    }
//    else
//    {
//            pickerSelectedString = @"";
//    }
    
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
@end
