//
//  HttpManager.m
//  HttpManager
//
//  Created by Mac Book on 21/11/16.
//  Copyright (c) 2016 TrakitNow. All rights reserved.
//

#import "HttpManager.h"
#define kUserDefaults      [NSUserDefaults standardUserDefaults]

@implementation HttpManager

+ (void)apiDetails:(id)parameter serviceName:(NSString *)serviceName  httpType:(NSString *)type WithBlock:(completionBlock)block
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",KBaseURL,serviceName];
    NSString* webStringURL = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
//    NSLog(@"%@",webStringURL);
    NSURL *url = [NSURL URLWithString:webStringURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *sessionObj = [NSURLSession sharedSession];
    if(parameter!=nil)
    {
        NSData *data = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        [request setHTTPBody:data];
    }
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:ServerCompanyID forHTTPHeaderField:@"companyID"];
    [request addValue:[NSString stringWithFormat:@"Token %@",[kUserDefaults objectForKey:KToken]] forHTTPHeaderField:@"Authorization"];

    request.HTTPMethod = type;
    NSURLSessionDataTask *task = [sessionObj dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP_DELEGATE hideActivityIndcicator];
        });

        if (connectionError == nil && data!=nil)
        {
            NSError *error1 = nil;
            
            id responseDictionary = [NSJSONSerialization
                                     JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
            if (error1 == nil)
            {
                if (block)
                {
                    block(responseDictionary, nil);
                }
            }
            else
            {
                if (block)
                {
                    block([NSArray array], error1);
                }
            }
        }
        else
        {
            if (block)
            {
                if([connectionError.userInfo objectForKey:@"NSLocalizedDescription"]!=nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[CustomControls sharedObj]showAlert:[[connectionError.userInfo objectForKey:@"NSLocalizedDescription"]capitalizedString]];
                    });
                }
                block([NSArray array], connectionError);
            }
        }
    }];
    [task resume];
}

+ (void)loginApiAuthorization:(NSString *)credentials  WithBlock:(completionBlock)block
{
    NSString *urlString =[NSString stringWithFormat:@"%@%@",KBaseURL,Login];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *sessionObj = [NSURLSession sharedSession];
    NSData *data = [credentials dataUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedCredentials = [data base64EncodedStringWithOptions:0];
    NSString *authHeader =  [NSString stringWithFormat:@"Basic %@", encodedCredentials];
    [request addValue:authHeader forHTTPHeaderField:@"Authorization"];
    [request addValue:ServerCompanyID forHTTPHeaderField:@"companyID"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPMethod = @"POST";
    NSURLSessionDataTask *task = [sessionObj dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [APP_DELEGATE hideActivityIndcicator];
        });
        if (connectionError == nil && data!=nil)
        {
            
            NSError *error1 = nil;
            
            id responseDictionary = [NSJSONSerialization
                                     JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error1];
            if (error1 == nil)
            {
                
                if (block)
                {
                    block(responseDictionary, nil);
                }
            }
            else {
                if (block)
                {
                    block([NSArray array], error1);
                }
            }
        }
        else
        {
            if (block)
            {
                if([connectionError.userInfo objectForKey:@"NSLocalizedDescription"]!=nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[CustomControls sharedObj]showAlert:[[connectionError.userInfo objectForKey:@"NSLocalizedDescription"]capitalizedString]];
                    });
                }
                block([NSArray array], connectionError);
            }
        }
    }];
    [task resume];
}


@end
