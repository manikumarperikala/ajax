//
//  Vehiclesview.m
//  MyNavi
//
//  Created by TrakitNow on 9/18/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import "Vehicleslistview.h"
#import "VehicleObject.h"

@implementation Vehicleslistview

-(void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [_vehicleslist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    VehicleObject *obj = _vehicleslist[indexPath.row];
    cell.textLabel.text = [obj.vehicleNumber uppercaseString];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    VehicleObject *obj = _vehicleslist[indexPath.row];
    NSString *lat = [obj lat];
    NSString *lng = [obj lng];
    [self.listdelegate selectedVehicleNumber:cell.textLabel.text latitude:lat longitude:lng];
}
- (IBAction)removeView:(id)sender {
    [self.listdelegate removeView];
}

@end
