//
//  CustomPickerView.h
//  Locale
//
//  Created by sridhar on 12/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PickerViewProtocal <NSObject>

-(void)pickerViewSelectedValue:(NSString *)sender forRow:(int)row;
@end
@interface CustomPickerView : UIView<UIPickerViewDelegate>
{
    NSString *pickerSelectedString;
}
@property(nonatomic,strong) UIPickerView *categoryPickerView;
@property(nonatomic,assign) int pickerSelectedRow;
@property(nonatomic,strong)NSArray *dataArray;
@property(assign)id<PickerViewProtocal> delegate;

-(void)setSelectedValue:(int)row;
@end
