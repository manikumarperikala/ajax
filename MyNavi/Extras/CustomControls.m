//
//  CustomControls.m
//  Kikr
//
//  Created by Mobikasa on 11/17/14.
//  Copyright (c) 2014 mobikasa. All rights reserved.
//

#import "CustomControls.h"
@implementation CustomControls
+ (CustomControls *)sharedObj
{
    static CustomControls *sharedObj = nil;
    if(sharedObj==nil)
    {
        sharedObj = [[self alloc] init];
    }
    return sharedObj;
}
-(NSArray*)leftBarButtonMethod:(NSString*)ImgName selector:(SEL)selector delegate:(id)del
{
    UIImage *barButtonImage = [[UIImage imageNamed:ImgName] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    UIButton* leftSlideButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
    leftSlideButton.frame = CGRectMake(0.0, 0.0, barButtonImage.size.width, barButtonImage.size.height);
    [leftSlideButton setBackgroundColor:[UIColor clearColor]];
    [leftSlideButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [leftSlideButton setImage:barButtonImage forState:UIControlStateNormal];
    [leftSlideButton addTarget:del action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftBarButton=[[UIBarButtonItem alloc] initWithCustomView:leftSlideButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [negativeSpacer setWidth:-5];
        return  [NSArray arrayWithObjects:negativeSpacer,leftBarButton,nil];;
    
}
-(void)showAlert:(NSString *)message{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
     
     }];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
     
     }];
     
     [alert addAction:okAction];
     [alert addAction:cancelAction];
     */

}

-(void)presentAlertControllerWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate block:(void (^)(BOOL didCancel))response;{
    if ([UIAlertController class])
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            response(YES);
 
        }];
        [alertController addAction:ok];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            response(NO);
        }];
        [alertController addAction:cancel];
        [delegate presentViewController:alertController animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:@"Cancel",nil];
        [alert show];
    }
}

-(void)presentAlertController:(NSString *)message delegate:(id)delegate;
{
        if ([UIAlertController class])
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [delegate presentViewController:alertController animated:YES completion:nil];
        }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
}

-(UIBarButtonItem*)rightBarButtonMethod:(NSString*)ImgName withTitle:(NSString*)title selector:(SEL)selector delegate:(id)del
{
    if (ImgName != nil) {
        
        UIImage *barButtonImage = [[UIImage imageNamed:ImgName] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        UIButton* rightSlideButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightSlideButton.frame = CGRectMake(0.0, 0.0, barButtonImage.size.width, barButtonImage.size.height);
        [rightSlideButton setBackgroundColor:[UIColor clearColor]];
        [rightSlideButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [rightSlideButton setImage:barButtonImage forState:UIControlStateNormal];
        [rightSlideButton addTarget:del action:selector forControlEvents:UIControlEventTouchUpInside];
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            
            [rightSlideButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
        else
        {
            [rightSlideButton setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
        }
        rightSlideButton.userInteractionEnabled = NO;
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightSlideButton];
        
        return rightBarButton;
    }
    else {
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:del action:selector];
        
        return rightBarButton;
    }
}

-(void)saveUserInfo:(NSDictionary *)dict{
    
    [kUserDefaults setObject:[dict objectForKey:@"useType"] forKey:KRoleID];
    [kUserDefaults setObject:[dict objectForKey:kApplicationId] forKey:kApplicationId];
    [kUserDefaults setObject:[dict objectForKey:KToken] forKey:KToken];
    [kUserDefaults setObject:[dict objectForKey:KFirstName] forKey:KFirstName];
    [kUserDefaults setObject:[dict objectForKey:KLastName] forKey:KLastName];
    [kUserDefaults setObject:[dict objectForKey:KPhoneNumber] forKey:KPhoneNumber];
    [kUserDefaults setObject:[dict objectForKey:KEmail] forKey:KEmail];
    [kUserDefaults setObject:[dict objectForKey:KID] forKey:KID];
    [kUserDefaults setObject:[dict objectForKey:KLoginName] forKey:KLoginName];
    [kUserDefaults setObject:[dict objectForKey:KdealerName] forKey:KdealerName];
    [kUserDefaults synchronize];
}

-(NSString *)getCurrentDate
{
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"]; //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];//
    return convertedDateString;
}

-(NSString *)beginningOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *stratTimeDate = [cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:stratTimeDate];
    
    return dateString;
}

-(NSString *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond  | NSCalendarUnitDay) fromDate:date];
    
    [components setHour:18];
    [components setMinute:29];
    [components setSecond:59];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *endTimeDate=[cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:endTimeDate];
    
    return dateString;
}
-(NSString *)beggingOfMonth:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    [components setDay:1];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *stratTimeDate = [cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:stratTimeDate];
    return dateString;
}
-(NSString *)endOfMonth:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    NSRange dayRange = [cal rangeOfUnit:NSCalendarUnitDay
                                             inUnit:NSCalendarUnitMonth
                                            forDate:date];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    [components setDay:dayRange.length];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *stratTimeDate = [cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:stratTimeDate];
    return dateString;
}
-(NSString *)beggingOfYear:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    [components setMonth:1];
    [components setDay:1];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *stratTimeDate = [cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:stratTimeDate];
    return dateString;
}
-(NSString *)endOfYear:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay ) fromDate:date];
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    [components setMonth:12];
    [components setDay:31];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *stratTimeDate = [cal dateFromComponents:components];
    NSString *dateString = [formatter stringFromDate:stratTimeDate];
    return dateString;
}

-(NSString *)getCurrentDateminusfiveHours
{
    NSDate *todayDate = [NSDate date]; // get today date
    todayDate = [todayDate dateByAddingTimeInterval:-(330*60)];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"]; //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];//
    return convertedDateString;
}

-(NSString *)addFiveHoursthirtymins:(NSString *)sender
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    if(date == nil)
    {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        date = [dateFormatter dateFromString:sender];
    }
    if(date == nil)
    {
        [dateFormatter setDateFormat:@"HH:mm:ss-dd/MM/yyyy"];
        date = [dateFormatter dateFromString:sender];
    }
    date = [date dateByAddingTimeInterval:+(330*60)];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}
-(NSString*)getFormattedDate:(NSString *)sender
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [formatter dateFromString:sender];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    return [formatter stringFromDate:date];
}

-(NSString *)getDate:(NSString *)sender
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *local = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:local];
    NSDate *localstartDate = [formatter dateFromString:sender];
    if(localstartDate == nil)
    {
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        localstartDate = [formatter dateFromString:sender];
    }
    NSString *string = [formatter stringFromDate:localstartDate];
    return string;
}

-(UILabel*)createLabelFrame:(CGRect)frame  withTitle:(NSString*)title  withFont:(UIFont*)font textColor:(UIColor*)color
{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:font];
    [label setTextColor:color];
    label.text = title;
    
    return label;
}

@end

