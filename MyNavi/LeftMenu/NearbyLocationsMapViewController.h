//
//  NearbyLocationsMapViewController.h
//  MyNavi
//
//  Created by Vijay on 21/07/17.
//  Copyright © 2017 Trakitnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>


@interface NearbyLocationsMapViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UIButton *vehicleBtn;
@property(nonatomic,strong) IBOutlet UITableView *listView;
@property(nonatomic,strong) IBOutlet UIButton *listButton;
@property (weak, nonatomic) IBOutlet UIView *vehicleview;

@end
