//
//  LeftViewController.m
//  RoyalEnfield
//
//  Created by sridhar on 10/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "LeftViewController.h"
#import "LeftMenuCell.h"
#import "MenuObject.h"
#import "DashboardViewController.h"
#import "ClientDashboardViewController.h"
#import "CustomerDashboardViewController.h"
#import "VehiclesListVC.h"
#import "NearbyLocationsMapViewController.h"


@interface LeftViewController ()
{
    NSMutableArray *menuArray;
    SWRevealViewController *revealController;
}
@end

@implementation LeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuArray = [[NSMutableArray alloc]init];
    [self configureMenuArray];
    
    revealController = [self revealViewController];
    
    _nameLable.text = [NSString stringWithFormat:@"%@ %@",[kUserDefaults objectForKey:KFirstName],[kUserDefaults objectForKey:KLastName]];
    
     _phoneNumberLable.text = [kUserDefaults objectForKey:KPhoneNumber];
    
    _designationLable.text = [kUserDefaults objectForKey:KEmail];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    LeftMenuCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[LeftMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        if(IDIOM == IPAD)
        {
            cell.imgView.frame = CGRectMake(cell.imgView.frame.origin.x, 17, 36, 36);
            CGFloat width = cell.imgView.frame.origin.x+cell.imgView.frame.size.width+10;
            cell.titleLable.frame = CGRectMake(width, 0, cell.superview.frame.size.width-width, 70);
        }

    }
   
    MenuObject *obj = [menuArray objectAtIndex:indexPath.row];
    [cell.imgView setImage:[UIImage imageNamed:obj.imageName]];
    cell.titleLable.text = obj.title;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ( IDIOM == IPAD )
        return 70;
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
            
        case 0:
        {
            if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kDealerDashboard bundle:nil];
                DashboardViewController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:kDealerIdentifier];
                [revealController setFrontViewController:nearbyLocationsMapViewController];
            }
            else if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kCustomerID])
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kCustomerDashboard bundle:nil];
                CustomerDashboardViewController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:kCustomerIdentifier];
                [revealController setFrontViewController:nearbyLocationsMapViewController];
            }
            else if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kClientID])
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kClientDashboard bundle:nil];
                ClientDashboardViewController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:kClientIdentifier];
                [revealController setFrontViewController:nearbyLocationsMapViewController];
            }
            else
            {
                UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kServiceManDashboard bundle:nil];
                CustomerDashboardViewController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:kServiceManIdentifier];
                [revealController setFrontViewController:nearbyLocationsMapViewController];
            }
            [revealController setFrontViewPosition:FrontViewPositionLeftSideMost];
        }
            break;
        case 1:
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
            UINavigationController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:@"HomeNavigationViewController"];
            [revealController setFrontViewController:nearbyLocationsMapViewController];
            [revealController setFrontViewPosition:FrontViewPositionLeftSideMost];
            [revealController panGestureRecognizer];
        }
            break;
            
        case 2:
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
            UINavigationController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:@"Service"];
            [revealController setFrontViewController:nearbyLocationsMapViewController];
            [revealController setFrontViewPosition:FrontViewPositionLeftSideMost];
            [revealController panGestureRecognizer];
        }
            break;
        case 3:
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
            NearbyLocationsMapViewController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:@"Near"];
            [self presentViewController:nearbyLocationsMapViewController animated:YES completion:nil];
        }
            break;

        case 4:
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
            UINavigationController *nearbyLocationsMapViewController = [storyBoard instantiateViewControllerWithIdentifier:@"Notification"];
            [revealController setFrontViewController:nearbyLocationsMapViewController];
            [revealController setFrontViewPosition:FrontViewPositionLeftSideMost];
            [revealController panGestureRecognizer];
        }
            break;
        case 5:
                [self signOutAction];
            break;
               }
}

- (void)signOutAction {
    
    [controls presentAlertControllerWithTitle:@"Log Out" message:@"Are you sure you want to logout?" delegate:self block:^(BOOL didOk) {
        if(didOk)
        {
            [kUserDefaults removeObjectForKey:@"logged_in"];
            [self removeDeviceforPushNotifications];
            [APP_DELEGATE popToViewController:@"LoginNavigationController"];
        }
    }];
}

-(void)removeDeviceforPushNotifications
{
    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] != nil)
    [HttpManager apiDetails:@{@"deviceId":deviceID,@"pushToken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],@"platform":@"IOS",@"active":@"true"} serviceName:[NSString stringWithFormat:@"%@",RemovePushtoken] httpType:@"POST" WithBlock:^(id responce, NSError *error) {
            NSLog(@"%@RESPONSE:",responce);
        }];
}


-(void)configureMenuArray
{
    MenuObject *dashboard = [[MenuObject alloc]init];
    dashboard.imageName = @"symbol";
    dashboard.title = @"Dashboard";
    
    MenuObject *vehicle = [[MenuObject alloc]init];
    vehicle.imageName = @"vehicles";
    vehicle.title = @"Vehicles";
    
    MenuObject *service = [[MenuObject alloc]init];
    service.imageName = @"service";
    service.title = @"Service";
    
    MenuObject *obj1 = [[MenuObject alloc]init];
    obj1.imageName = @"petrolMarker";
    obj1.title = @"Nearest petrol stations";
    
    MenuObject *notifications = [[MenuObject alloc]init];
    notifications.imageName = @"notifications-color";
    notifications.title = @"Notifications";
    
    MenuObject *obj = [[MenuObject alloc]init];
    obj.imageName = @"signOut";
    obj.title = @"Sign Out";
   
    [menuArray addObject:dashboard];
    [menuArray addObject:vehicle];
    [menuArray addObject:service];
    [menuArray addObject:obj1];
    [menuArray addObject:notifications];
    [menuArray addObject:obj];
    
    obj = nil;
}

@end
