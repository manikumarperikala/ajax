//
//  MenuCell.m
//  eRakshaPolice
//
//  Created by Sridhar Reddy on 7/1/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
