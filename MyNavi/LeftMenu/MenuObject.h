//
//  MenuObject.h
//  eRakshaPolice
//
//  Created by Sridhar Reddy on 7/1/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuObject : NSObject
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *imageName;

@end
