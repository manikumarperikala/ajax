//
//  NearbyLocationsMapViewController.m
//  MyNavi
//
//  Created by Vijay on 21/07/17.
//  Copyright © 2017 Trakitnow. All rights reserved.
//

#import "NearbyLocationsMapViewController.h"
#import "AppDelegate.h"
#import "VehicleObject.h"
#import "Vehicleslistview.h"


@interface NearbyLocationsMapViewController ()<SelectedVehicle>
{
    GMSCameraPosition *camera;
    CLLocationManager *locationManager;
    AppDelegate *appDelegate;
    NSArray *places;
    NSArray *placeNames;
    CLLocation *location;
    CGFloat fontSize;
    CGFloat subfontSize;
    BOOL isMapView;
    NSMutableArray *vehicleListArray;
    Vehicleslistview *vehicleslistview;
    NSString *lat,*lng;
    GMSMarker *vehicleloc;

}
@end

@implementation NearbyLocationsMapViewController
@synthesize mapView_;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.view.backgroundColor = [UIColor whiteColor];
    self.listView.rowHeight = UITableViewAutomaticDimension;
    self.listView.estimatedRowHeight = 140;
    [self getVechiclesList];
    if(IDIOM == IPAD)
    {
        fontSize = 20.0f;
        subfontSize = 16.0f;
    }
    else
    {
        fontSize = 15.0f;
        subfontSize = 12.0f;
    }

   // [self addGoogleMapView];
    [self.listButton setHidden:YES];
    isMapView = NO;
    
}

- (IBAction)close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)showList:(UIButton*)sender {

    if(!isMapView)
    {
        [sender setTitle:@"Mapview" forState:UIControlStateNormal];
        
        [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            
            [self.listView setHidden:NO];
            isMapView = !isMapView;
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [self.listView reloadData];
                [self.mapView_ setHidden:YES];

            });

        } completion:^(BOOL finished) {
            
        }];
    }
    else
    {
        [self showPlacesonMap:places];
        [sender setTitle:@"Listview" forState:UIControlStateNormal];
        
        [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
            
            [self.mapView_ setHidden:NO];
            isMapView = !isMapView;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.listView setHidden:YES];
            });
        } completion:^(BOOL finished) {
            
        }];
    }
}

-(void)addGoogleMapView
{
    [self.listView setHidden:YES];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 100, self.view.frame.size.width, self.view.frame.size.height - 100) camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = NO;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    [self.view addSubview:mapView_];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    //location = locations.lastObject;
    double latf = [lat doubleValue];
    double lngf = [lng doubleValue];
    CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:latf longitude:lngf];
    location = LocationAtual;
    camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:12.0];
    [self.mapView_ animateToCameraPosition:camera];
    [manager stopUpdatingLocation];
    manager.delegate = nil;
    [self getNearByLocationsforLocation:location];
}

-(void)getNearByLocationsforLocation:(CLLocation*)locat{
    NSString *placesApi = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=petrol stations&key=%@&location=%f,%f&radius=20000",KGoogleWebServiceApiKey,locat.coordinate.latitude,locat.coordinate.longitude];
    placesApi = [placesApi stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    placeNames = nil;
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:placesApi]];
    request.HTTPMethod = kGet;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        if(!error)
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
           placeNames = [dict valueForKeyPath:@"results"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if([placeNames count] > 0)
                {
                    [self showPlacesonMap:placeNames];
                    [self.listButton setHidden:NO];
                }
                else
                {
                    [self.listButton setHidden:YES];
                }
                [APP_DELEGATE hideActivityIndcicator];
            });
        }
    }];
    [task resume];
}

-(void)showPlacesonMap:(NSArray*)locations
{
    [mapView_ clear];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:12.0];
    GMSMarker *sourceMarker = [[GMSMarker alloc] init];
    sourceMarker.icon = [UIImage imageNamed:@"mapMarker48"];
    sourceMarker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude);
    sourceMarker.map = mapView_;
    [self.mapView_ animateToCameraPosition:camera];
    for (NSDictionary *locDict in locations) {
        
        GMSMarker *sourceMarker = [[GMSMarker alloc] init];
        sourceMarker.icon = [UIImage imageNamed:@"petrolMarker-1"];
        CGFloat latitude = [[[[locDict objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"] doubleValue];
        CGFloat longitude = [[[[locDict objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"] doubleValue];
        sourceMarker.position = CLLocationCoordinate2DMake(latitude,longitude);
        sourceMarker.title = [locDict objectForKey:@"name"];
        sourceMarker.snippet = [locDict objectForKey:@"vicinity"];
        bounds = [bounds includingCoordinate:sourceMarker.position];
        sourceMarker.appearAnimation = kGMSMarkerAnimationPop;
        sourceMarker.tracksViewChanges = NO;
        sourceMarker.map = mapView_;
    }
}
-(void)getVechiclesList
{
    NSString *type;
    NSString *servicename;
    NSMutableArray *list = [[NSMutableArray alloc] init];
    if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
    {
        type = DealerVehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KdealerName]];
    }
    else
    {
        type = VehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KLoginName]];
    }
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:servicename httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            [vehicleListArray removeAllObjects];
            NSArray *rows = [responce objectForKey:@"rows"];
            for(NSDictionary *dict in rows )
            {
                VehicleObject *obj = [[VehicleObject alloc]init];
                obj.vehicleName = [self checkNilString:[dict objectForKey:@"vehicleName"]];
                obj.vehicleNumber = [self checkNilString:[dict objectForKey:@"vehicleNumber"]];
                obj.lat = [self checkNilString:[dict objectForKey:@"lat"]];
                obj.lng = [self checkNilString:[dict objectForKey:@"lng"]];
                [list addObject:obj];
                
                obj = nil;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
               // [_listView reloadData];
                vehicleListArray = list;
                _vehicleBtn.titleLabel.text = [list.firstObject vehicleNumber];
                lat = [list.firstObject lat];
                lng = [list.firstObject lng];
                [self addGoogleMapView];
                [APP_DELEGATE hideActivityIndcicator];
            });
        }
    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([placeNames count])
    return placeNames.count;
    else
        return  0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
        NSDictionary *addressDict = placeNames[indexPath.row];
        NSString *name = addressDict[@"name"];
        NSString *address = addressDict[@"vicinity"];
    
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@",name,address]];
           
        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize]  range:NSMakeRange(0, name.length)];
    
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(name.length+1, address.length)];
           
        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:subfontSize]  range:NSMakeRange(name.length+1, address.length)];
    
        UILabel *nameLbl = (UILabel*)[cell viewWithTag:10];
        [nameLbl setAttributedText:attributedString];
    
    return cell;
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [mapView setSelectedMarker:marker];
    
    return YES;
}


-(void)removeView
{
    [vehicleslistview removeFromSuperview];
    vehicleslistview = nil;
}

-(void)selectedVehicleNumber:(NSString *)number latitude:(NSString *)lat longitude:(NSString *)lng
{
    _vehicleBtn.titleLabel.text = number;
    double latf = [lat doubleValue];
    double lngf = [lng doubleValue];
    CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:latf longitude:lngf];
    location = LocationAtual;
    [self getNearByLocationsforLocation:location];
    [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        vehicleslistview.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        [vehicleslistview removeFromSuperview];
    } completion:^(BOOL finished) {
        vehicleslistview = nil;
    }];
}

-(NSString*)checkNilString:(NSString*)val
{
    if (val != nil)
    {
        return val;
    }
    else
    {
        return @"NA";
    }
}
- (IBAction)vehicleSelect:(UIButton *)sender {
    
   
    if ([vehicleListArray count])

            vehicleslistview = [[NSBundle mainBundle]loadNibNamed:@"Vehicleslistview" owner:self options:nil].firstObject;
            vehicleslistview.vehicleslist = vehicleListArray;
            vehicleslistview.listdelegate = self;
            vehicleslistview.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            
            [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                vehicleslistview.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                [self.view addSubview:vehicleslistview];
                
            } completion:^(BOOL finished) {
                
            }];
            //                [APP_DELEGATE hideActivityIndcicator];
//        });
}


@end
