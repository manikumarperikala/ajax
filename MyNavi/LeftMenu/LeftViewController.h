//
//  LeftViewController.h
//  RoyalEnfield
//
//  Created by sridhar on 10/04/17.
//  Copyright © 2017 Trakitnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *nameLable;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberLable;
@property (strong, nonatomic) IBOutlet UILabel *designationLable;
@property (strong, nonatomic) IBOutlet UILabel *versionLable;

@end
