//
//  MenuCell.h
//  eRakshaPolice
//
//  Created by Sridhar Reddy on 7/1/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@end
