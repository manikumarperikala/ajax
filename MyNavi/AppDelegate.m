//
//  AppDelegate.m
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "AppDelegate.h"
#import "LocaitonManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
@import Crashlytics;
@import Fabric;
@import Firebase;
@import NMAKit;


@interface AppDelegate ()
{
    SWRevealViewController *revealController;
    NSInteger badge;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [Crashlytics startWithAPIKey:@"786cd002227b4a31407db76f58962d4eca18da3b"];
    [Fabric with:@[[Crashlytics class]]];
    
    [GMSServices provideAPIKey:KGoogleApiKey];
    [GMSPlacesClient provideAPIKey:KGoogleApiKey];
    [NMAApplicationContext setAppId:kHereMapAppID appCode:kHereMapAppCode];
    
    NSDictionary *textAttributes;
    UIFont *font;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        font = [UIFont systemFontOfSize:26];
    }
    else{
        font = [UIFont systemFontOfSize:18];
    }
    textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                      [UIColor whiteColor],NSForegroundColorAttributeName,
                      font, NSFontAttributeName,nil];
    
    [UINavigationBar appearance].titleTextAttributes = textAttributes;
    
    [[UINavigationBar appearance] setBarTintColor:NavigationBarColor];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"]) {
        
        [self setStoryBoardWithName:kMainDashboard withIdentifier:@"LoginNavigationController"];
    }
    else
    {
        if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
        {
            [self setStoryBoardWithName:kDealerDashboard withIdentifier:kDealerIdentifier];
        }
        else if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kCustomerID])
        {
            [self setStoryBoardWithName:kCustomerDashboard withIdentifier:kCustomerIdentifier];
        }
        else if ([[kUserDefaults objectForKey:KRoleID]isEqualToString:kClientID])
        {
            
            [self setStoryBoardWithName:kClientDashboard withIdentifier:kClientIdentifier];
        }
        else
        {
            [self setStoryBoardWithName:kServiceManDashboard withIdentifier:kServiceManIdentifier];
        }
    }
    
    
    // Use Firebase library to configure APIs
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
   /* UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if( !error ){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
        }
    }];*/
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    [LocaitonManager sharedObj];
  
    [self loguser];
    
    return YES;
}

-(void)loguser{
    
    [CrashlyticsKit setUserName:[kUserDefaults objectForKey:[NSString stringWithFormat:@"%@ %@",[kUserDefaults objectForKey:KFirstName],[kUserDefaults objectForKey:KLastName]]]];
    [CrashlyticsKit setUserEmail:[kUserDefaults objectForKey:KEmail]];
    [CrashlyticsKit setUserIdentifier:[kUserDefaults objectForKey:KID]];
}


-(void)setStoryBoardWithName:(NSString*)name withIdentifier:(NSString*)identifier{
    
    UIStoryboard *leftStoryBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
    UIViewController *slidMenu = (UIViewController *)[leftStoryBoard instantiateViewControllerWithIdentifier:@"LeftViewController"];
    UINavigationController *navigationController;
    
    if([name isEqualToString:kMainDashboard])
    {
         navigationController = (UINavigationController *)[leftStoryBoard instantiateViewControllerWithIdentifier:identifier];
    }
    else
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:name bundle:nil];
        navigationController = (UINavigationController *)[storyBoard instantiateViewControllerWithIdentifier:identifier];
    }
    
    
    revealController = [[SWRevealViewController alloc] initWithRearViewController:slidMenu frontViewController:navigationController];
    
    self.window.rootViewController = revealController;

}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    const unsigned *tokenData = deviceToken.bytes;
    deviceTokenString = [NSString stringWithFormat:@"%08x %08x %08x %08x %08x %08x %08x %08x", ntohl(tokenData[0]), ntohl(tokenData[1]), ntohl(tokenData[2]), ntohl(tokenData[3]), ntohl(tokenData[4]), ntohl(tokenData[5]), ntohl(tokenData[6]), ntohl(tokenData[7])];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"device token %@",deviceTokenString);
    [[NSUserDefaults standardUserDefaults]setObject:deviceTokenString forKey:@"deviceToken"];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    
//    NSLog(@"Userinfo %@",notification.request.content.userInfo);
//    
//    completionHandler(UNNotificationPresentationOptionAlert);
//}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    badge += [[[userInfo objectForKey:@"aps"] valueForKey:@"badge"] integerValue];
    [application setApplicationIconBadgeNumber:badge];    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    badge += [[[userInfo objectForKey:@"aps"] valueForKey:@"badge"] integerValue];
    [application setApplicationIconBadgeNumber:badge];
}
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [application setApplicationIconBadgeNumber:0];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}
-(void)popToViewController:(NSString *)identifier
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:kMainDashboard bundle:nil];
    UIViewController *slidMenu = (UIViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"LeftViewController"];
    UINavigationController *navigationController;
    
    navigationController = (UINavigationController *)[storyBoard instantiateViewControllerWithIdentifier:identifier];
    revealController = [[SWRevealViewController alloc] initWithRearViewController:slidMenu frontViewController:navigationController];

    self.window.rootViewController = revealController;
}

-(void)showActivityIndicatorOnFullScreen
{
    if (!backgroundView) {
        
        backgroundView = [[UIView alloc] initWithFrame:self.window.frame];
        
        backgroundView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [backgroundView addSubview:activityView];
        activityView.center = backgroundView.center;
    }
    
    dispatch_async(dispatch_get_main_queue(),
    ^{
        if ([activityView isAnimating]) {
            
            [activityView stopAnimating];
        }
        
        [activityView startAnimating];
        
    });
    [self.window addSubview:backgroundView];
}
-(void)hideActivityIndcicator{
    if ([backgroundView superview]) {
        [backgroundView removeFromSuperview];
    }
}

@end
