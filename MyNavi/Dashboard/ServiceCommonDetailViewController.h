//
//  ServiceCommonDetailViewController.h
//  MyNavi
//
//  Created by Vijay on 27/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCommonDetailViewController : UITableViewController

@property(nonatomic,weak) NSString *serviceId;
@property(nonatomic,assign) NSDictionary *details;

@end
