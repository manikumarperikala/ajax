//
//  PaddingLabel.m
//  MyNavi
//
//  Created by Vijay on 17/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "PaddingLabel.h"

@implementation PaddingLabel

-(void)awakeFromNib{
    [super awakeFromNib];
}

-(void)drawRect:(CGRect)rect{
    UIEdgeInsets insets = {0, 10, 0, 10};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
