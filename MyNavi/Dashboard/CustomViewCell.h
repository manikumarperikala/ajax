//
//  CustomViewCell.h
//  AjaxDashboard
//
//  Created by Vijay on 14/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *name,*value;

@end
