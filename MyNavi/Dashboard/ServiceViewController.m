//
//  ExpandViewController.m
//  AjaxDashboard
//
//  Created by Vijay Bhaskar on 15/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import "ServiceViewController.h"
#import "ServiceDetailViewController.h"

@interface ServiceViewController ()
{
    NSMutableArray *arr;
    NSArray *allLoad;
    NSArray *serviceList;
    NSMutableArray *filteredList;
}

@end

@implementation ServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
    self.title = @"Upcoming Service";
    
    arr = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO], nil];
    NSString *title = [self.firstBtn currentTitle];
    filteredList = [[NSMutableArray alloc] init];
    [self upcomingServices:@""];
}

-(void)popAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)upcomingServices: (NSString*) str
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    NSString *api = [NSString stringWithFormat:UpcomingserviceApi,[kUserDefaults objectForKey:KdealerName],str];
    [HttpManager apiDetails:nil serviceName:api httpType:@"GET" WithBlock:^(id responce, NSError *error) {
        if(error == nil)
        {
            serviceList = responce[@"rows"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self filterdata:@"ARGO1000"];
                [self.tableView reloadData];
                [APP_DELEGATE hideActivityIndcicator];
            });
        }
    }];
}

-(IBAction)performSelection:(UIButton*)sender
{
    switch (sender.tag) {
            
        case 10:
            [sender setBackgroundColor:kHighlitedColor];
            [self.secondBtn setBackgroundColor:kNormalColor];
            [self.thirdBtn setBackgroundColor:kNormalColor];
            [self.forthBtn setBackgroundColor:kNormalColor];
            [self filterdata:@"ARGO1000"];
        break;
            
        case 11:
            [sender setBackgroundColor:kHighlitedColor];
            [self.firstBtn setBackgroundColor:kNormalColor];
            [self.thirdBtn setBackgroundColor:kNormalColor];
            [self.forthBtn setBackgroundColor:kNormalColor];
            [self filterdata:@"ARGO2000"];
        break;
            
        case 12:
            [sender setBackgroundColor:kHighlitedColor];
            [self.secondBtn setBackgroundColor:kNormalColor];
            [self.firstBtn setBackgroundColor:kNormalColor];
            [self.forthBtn setBackgroundColor:kNormalColor];
            [self filterdata:@"ARGO2500"];
        break;
            
        case 13:
            [sender setBackgroundColor:kHighlitedColor];
            [self.secondBtn setBackgroundColor:kNormalColor];
            [self.thirdBtn setBackgroundColor:kNormalColor];
            [self.firstBtn setBackgroundColor:kNormalColor];
            [self filterdata:@"ARGO4000"];
        break;
            
        default:
            break;
    }
}
-(void)filterdata: (NSString*) str
{
    [filteredList removeAllObjects];
    for (int i = 0; i < serviceList.count; i++) {
        NSDictionary *dict = serviceList[i];
        NSDictionary *itemdict = [dict objectForKey:@"item"];
        NSString *model = [itemdict objectForKey:@"deviceModel"];
        if ([str isEqualToString:model])
        {
           [filteredList addObject:dict];
        }
    }
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IDIOM == IPAD)
        return 60.0f;
    return 40.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (filteredList.count > 0)
    {
        return filteredList.count;
    }
    else
    {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:tableView.frame];
        messageLabel.text = @"Services are not available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        tableView.backgroundView = messageLabel;
    }
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}



/*-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0f;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *headerView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    headerView.tag = section;
    headerView.text = [NSString stringWithFormat:@"Section %ld",(long)section];
    headerView.textColor = [UIColor whiteColor];
    headerView.textAlignment = NSTextAlignmentCenter;
    headerView.backgroundColor = [UIColor blackColor];
    if(section % 2 != 0)
        headerView.backgroundColor = kHighlitedColor;
    
    headerView.layer.borderColor = [UIColor whiteColor].CGColor;
    headerView.layer.borderWidth = 1.0;
    headerView.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(expand:)];
    recognizer.numberOfTapsRequired = 1;
    [headerView addGestureRecognizer:recognizer];
    
    return headerView;
}

-(void)expand:(UIGestureRecognizer*)recognizer{
    
    if([arr objectAtIndex:recognizer.view.tag] == [NSNumber numberWithBool:NO])
    {
        [arr replaceObjectAtIndex:recognizer.view.tag withObject:[NSNumber numberWithBool:YES]];
    }
    else
    {
        [arr replaceObjectAtIndex:recognizer.view.tag withObject:[NSNumber numberWithBool:NO]];
    }
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:recognizer.view.tag] withRowAnimation:UITableViewRowAnimationFade];
    
switch (recognizer.view.tag) {
        case 0:
            allLoad = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO], nil];

            break;
        case 1:
            allLoad = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO], nil];
            
            break;
        case 2:
            allLoad = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO], nil];
            
            break;
        case 3:
            allLoad = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES], nil];
            
            break;
            
        default:
            break;
    }
    
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        [self.tableView reloadData];
    } completion:nil];
    
}*/
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = filteredList[indexPath.row];
    NSDictionary *itemdict = [dict objectForKey:@"item"];
    [(UILabel*)[cell viewWithTag:10] setText:[self dict:itemdict iskeyexists:@"vehicleNumber"]];
    [(UILabel*)[cell viewWithTag:11] setText:[self dict:dict iskeyexists:@"serviceNumber"]];
    [(UILabel*)[cell viewWithTag:12] setText:[self dict:itemdict iskeyexists:@"alertMobile"]];
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [self performSegueWithIdentifier:@"ServiceDetail" sender:nil];
    NSDictionary *dict = filteredList[indexPath.row];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    ServiceDetailViewController *serviceDetailObj = (ServiceDetailViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"ServiceDetail"];
    serviceDetailObj.dict1 = dict;
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:serviceDetailObj];
    [self.navigationController presentViewController:navi animated:YES completion:nil];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
-(NSString *)dict:(NSDictionary *)dict iskeyexists:(NSString *)key
{
    if (([dict objectForKey:key]) != nil)
    {
        NSString *value = [dict objectForKey:key];
        if ((![value  isEqual: @""]) && (![value  isEqual: @"(null)"]))
        {
            return value;
        }
        else
        {
            return @"NA";
        }
    }
    return @"NA";
}
@end
