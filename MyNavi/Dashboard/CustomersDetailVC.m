//
//  CustomersDetailVC.m
//  MyNavi
//
//  Created by Manikumar on 04/12/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import "CustomersDetailVC.h"


@interface CustomersDetailVC ()
{
    NSArray *titlearr;
    NSDictionary *dict;
    NSString *alertmobile;
}
@end

@implementation CustomersDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self validatefields];
    self.navigationItem.title = @"Customer Details";
    UIBarButtonItem *rightbarBtn = [[CustomControls sharedObj]rightBarButtonMethod:nil withTitle:@"Close" selector:@selector(popAction) delegate:self];
    rightbarBtn.tintColor = [UIColor whiteColor];
    _customerGridView.tableFooterView = [UIView new];
    self.navigationItem.rightBarButtonItem = rightbarBtn;
}

- (void)validatefields
{
    NSDictionary *extradict = [self.dict1 objectForKey:@"extras"];
    NSString *firstname = [self dict:_dict1 iskeyexists:@"firstName"];
    NSString *lastname = [self dict:_dict1 iskeyexists:@"lastName"];
    NSString *str = [NSString stringWithFormat: @"%@ %s %@", firstname, " ",lastname];
    NSString *dateStr = [self dict:_dict1 iskeyexists:@"createdAt"];
    NSString *created = [self getDateString:dateStr];
    titlearr = @[@{@"Key":@"CustomerID",@"Value":[self dict:extradict iskeyexists:@"customerID"]},@{@"Key":@"Customer Name",@"Value":str},@{@"Key":@"Login Name",@"Value":[self dict:_dict1 iskeyexists:@"loginName"]},@{@"Key":@"Created Date",@"Value":created},@{@"Key":@"Contact",@"Value":[self dict:_dict1 iskeyexists:@"phone"]},@{@"Key":@"Alternate Contact",@"Value":[self dict:extradict iskeyexists:@"alertMobile"]},@{@"Key":@"Address",@"Value":[self dict:extradict iskeyexists:@"address"]}];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (titlearr.count)
    {
        return titlearr.count;
    }
    else
    {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:tableView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        tableView.backgroundView = messageLabel;
    }
    return  0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = titlearr[indexPath.row];
    [(UILabel*)[cell viewWithTag:10] setText:[dict objectForKey:@"Key"]];
    [(UILabel*)[cell viewWithTag:11] setText:[dict objectForKey:@"Value"]];
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 6)
    {
        return 120.0f;
    }
    else
    {
        return 50.0f;
    }
}

-(void)popAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}
-(NSString *)dict:(NSDictionary *)dict iskeyexists:(NSString *)key
{
    if (([dict objectForKey:key]) != nil)
    {
        NSString *value = [dict objectForKey:key];
        if ((![value  isEqual: @""]) && (![value  isEqual: @"(null)"]))
        {
            return value;
        }
        else
        {
            return @"NA";
        }
    }
    return @"NA";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
