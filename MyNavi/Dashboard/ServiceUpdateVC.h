//
//  ServiceUpdateVC.h
//  MyNavi
//
//  Created by Vijay on 25/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceUpdateVC : UIViewController<UITextFieldDelegate>

@property(nonatomic,strong)NSString *serviceId;

@property(nonatomic,weak)IBOutlet UITextField *machineFaults;
@property(nonatomic,weak)IBOutlet UITextField *engineFaults;
@property(nonatomic,weak)IBOutlet UITextField *maintainDetails;
@property(nonatomic,weak)IBOutlet UITextField *cost;
@property(nonatomic,weak)IBOutlet UITextField *rectifiedDate;

@end
