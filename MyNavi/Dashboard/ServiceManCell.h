//
//  ServiceManCell.h
//  MyNavi
//
//  Created by Vijay on 27/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceManCell : UICollectionViewCell

@property(nonatomic,strong)IBOutlet UIImageView *imageView;
@property(nonatomic,strong)IBOutlet UILabel *nameLabel;

@end
