//
//  ServiceDetailModel.h
//  MyNavi
//
//  Created by Vijay on 22/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetailModel : NSObject

@property(nonatomic,copy) NSString* name;
@property(assign) NSInteger age;
@property(nonatomic,copy) NSString* place;

@end
