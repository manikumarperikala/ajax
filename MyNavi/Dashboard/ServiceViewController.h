//
//  ServiceViewController.h
//  AjaxDashboard
//
//  Created by Vijay Bhaskar on 15/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIButton *firstBtn,*secondBtn,*thirdBtn,*forthBtn;


@end
