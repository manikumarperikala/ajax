//
//  MachinesViewController.m
//  MyNavi
//
//  Created by Vijay on 11/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import "MachinesViewController.h"

@interface MachinesViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *list;
}

@end

@implementation MachinesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Commissioned Reports";
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    [self getMachineCommissionDetails];
}
-(void)getMachineCommissionDetails{
    
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat:CommissioningAllocation,[kUserDefaults objectForKey:kApplicationId],@"true"] httpType:kGet WithBlock:^(id responce, NSError *error) {
        
        if(error == nil)
        {
            list = responce[@"rows"];
            if(list.count == 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UILabel *label = [[UILabel alloc]initWithFrame:self.machineCommissionlistView.bounds];
                    label.text = @"No Data Available";
                    label.textAlignment = NSTextAlignmentCenter;
                    self.machineCommissionlistView.backgroundView = label;
                });
            }
            else
                self.machineCommissionlistView.backgroundView = nil;
        }
        else
        {}
    }];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return list != nil ? 1 : 0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row % 2 != 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"Machine1" sender:nil];
}

@end
