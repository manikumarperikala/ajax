//
//  ClusterViewController.h
//  MyNavi
//
//  Created by Vijay on 23/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClusterViewController : UIViewController<UIWebViewDelegate>

@property(nonatomic,strong) IBOutlet UIWebView *clusterView;
@property(nonatomic,strong) NSString *loadedString;
@property(nonatomic,strong) NSString *isArgo2500;

@end
