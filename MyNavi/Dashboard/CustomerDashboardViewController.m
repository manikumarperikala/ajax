//
//  CustomerDashboardViewController.m
//  MyNavi
//
//  Created by Vijay on 18/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CustomerDashboardViewController.h"
#import "MenuListViewController.h"
#import "ClusterViewController.h"
#import "CustomerDashboardCell.h"
#import "VehiclesView.h"

@interface CustomerDashboardViewController ()<UITableViewDataSource,UITableViewDelegate,SWRevealViewControllerDelegate,MenuDelegate,SelectionDelegate>
{
    UIButton *selectedButton;
    NSArray *ownedVehiclesList;
    VehiclesView *vehiclesView;
    NSString *deviemodel;
    NSString *isSelected;
    NSString *displayname;
    
}
@property (strong, nonatomic) IBOutlet UIButton *selectVehicleBtn;

@end

@implementation CustomerDashboardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Customer Dashboard";
    [self setslideViewController];
    isSelected = @"false";
    NSString *api = [NSString stringWithFormat:CustomerDashboardApi,@"",[kUserDefaults objectForKey:KLoginName]];
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    
    [HttpManager apiDetails:nil serviceName:api httpType:@"GET" WithBlock:^(id responce, NSError *error) {
        if(error == nil)
        {
            ownedVehiclesList = responce[@"rows"];
            if (ownedVehiclesList.count)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.vehicleListView reloadData];
                    [APP_DELEGATE hideActivityIndcicator];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UILabel *label = [[UILabel alloc]initWithFrame:self.vehicleListView.frame];
                    label.text = @"Upcoming Services are not availble";
                    label.textAlignment = NSTextAlignmentCenter;
                    self.vehicleListView.backgroundView = label;
                });
            }
        }
    }];
    
    if(self.view.frame.size.width == 375.0)
        self.selectVehicleBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 200, 0, 0);
    else if(self.view.frame.size.width > 375.0)
        self.selectVehicleBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 250, 0, 0);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarHidden = NO;

    self.navigationController.navigationBar.hidden = NO;

}
-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
- (IBAction)selectType:(UIButton *)sender {
    
    selectedButton = sender;
    NSString *type;
    switch (sender.tag) {
        case 14:
            type = @"Select Model";
            break;
            
        case 15:
            type = @"Select Vehicle";
            break;

        default:
            break;
    }
    [self performSegueWithIdentifier:@"Menu" sender:type];
}


-(void)selectMenuItem:(NSString *)item
{
    [selectedButton setTitle:item forState:UIControlStateNormal];
}

-(IBAction)clusterView:(id)sender
{
    if(![[self.selectVehicleBtn currentTitle] isEqualToString:@"Select Vehicle"])
        [self performSegueWithIdentifier:@"Cluster" sender:self.selectVehicleBtn.currentTitle];
    else
    {
        [[CustomControls sharedObj] presentAlertController:@"Please select vehicle number" delegate:self];
    }
}

- (IBAction)selectVehicle:(UIButton *)sender
{
    if ([isSelected  isEqual: @"false"])
    {
        [self getVechiclesList];
    }

}

-(void)getVechiclesList
{
    isSelected = @"true";
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSString *type;
    NSString *servicename;
    if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
    {
        type = DealerVehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KdealerName]];
    }
    else
    {
        type = VehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KLoginName]];
    }
    
//    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName: servicename httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            NSArray *rows = [responce objectForKey:@"rows"];
            for(NSDictionary *dict in rows )
            {
                VehicleObject *obj = [[VehicleObject alloc]init];
                obj.vehicleName = [self checkNilString:[dict objectForKey:@"vehicleName"]];
                obj.vehicleNumber = [self checkNilString:[dict objectForKey:@"vehicleNumber"]];
                obj.vehicleType = [self checkNilString:[dict objectForKey:@"vehicleType"]];
                obj.latitude = [[self checkNilString:[dict objectForKey:@"lat"]] doubleValue];
                obj.longitude = [[self checkNilString:[dict objectForKey:@"lng"]] doubleValue];
                
                //details Array
                obj.deviceModel = [self checkNilString:[dict objectForKey:@"deviceModel"]];
                obj.deviceID = [self checkNilString:[dict objectForKey:@"deviceID"]];
                NSString *datestring = [self checkNilString:[dict objectForKey:@"lastEngineOn"]];
                
                if(![obj.engineLastOn isEqualToString:@"NA"] && ![obj.engineLastOn isEqualToString:@""])
                {
                    NSString *actualDate = [[CustomControls sharedObj]addFiveHoursthirtymins:datestring];
                    obj.engineLastOn = [self getDateString:actualDate];;
                }
                else
                {
                    obj.engineLastOn = @"NA";
                }
                
                obj.totalEngineHours = [self checkNilString:[dict objectForKey:@"totalEngineHours"]];
                
                obj.vehicleLocation = [self checkNilString:[dict objectForKey:@"vehicleLocation"]];
                
                NSString *dateString = [self checkNilString:[dict objectForKey:@"lastDataReceivedAt"]];
                
                if(![dateString isEqualToString:@""] && ![dateString isEqualToString:@"NA"])
                {
                    NSString *actualDate = [[CustomControls sharedObj]addFiveHoursthirtymins:dateString];
                    
                    obj.lastDataReceivedAt = [self getDateString:actualDate];
                }
                else
                {
                    obj.lastDataReceivedAt = @"NA";
                }
                
                [list addObject:obj];
                
                obj = nil;
            }
            if (list.count > 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    vehiclesView = [[NSBundle mainBundle]loadNibNamed:@"VehiclesView" owner:self options:nil].firstObject;
                    vehiclesView.vehicles = list;
                    vehiclesView.delegate = self;
                    vehiclesView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                    
                    [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                        vehiclesView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                        [self.view addSubview:vehiclesView];
                        
                    } completion:^(BOOL finished) {
                        
                    }];
                    //                [APP_DELEGATE hideActivityIndcicator];
                });
            }
            else
            {
              [controls presentAlertController:@"Vehicles are not available" delegate:self];
            }
            isSelected = @"false";
        }
    }];
}

-(void)removeView
{
    [vehiclesView removeFromSuperview];
    vehiclesView = nil;
}

-(NSString*)checkNilString:(NSString*)val
{
    if (val != nil)
    {
        return val;
    }
    else
    {
        return @"NA";
    }
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

-(void)selectedVehicleNumber:(NSString *)number devicemodel:(NSString *)model
{
    [self.selectVehicleBtn setTitle:number forState:UIControlStateNormal];
    deviemodel = model;
    [UIView transitionWithView:self.view duration:1.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        vehiclesView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        [vehiclesView removeFromSuperview];
        
    } completion:^(BOOL finished) {
        vehiclesView = nil;
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IDIOM == IPAD)
        return 60.0f;
    return 44.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ownedVehiclesList.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomerDashboardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *dict = ownedVehiclesList[indexPath.row];
    cell.nextService.text  = dict[@"serviceNumber"];
    
    NSDictionary *itemDict = dict[@"item"];
    cell.model.text = [itemDict[@"deviceModel"] uppercaseString];
    cell.vehicleNumber.text = [itemDict[@"vehicleNumber"] uppercaseString];
    
    if(itemDict[@"totalEngineHours"] != nil)
    {
        cell.engineHours.text = [NSString stringWithFormat:@"%@",itemDict[@"totalEngineHours"]];
    }
    else
    {
        cell.engineHours.text = @"-";
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = ownedVehiclesList[indexPath.row];
    NSDictionary *item = [dict objectForKey:@"item"];
    NSString *lat = [NSString stringWithFormat:@"%@",[item objectForKey:@"lat"]];
    NSString *lng = [NSString stringWithFormat:@"%@",[item objectForKey:@"lng"]];
    [self getLocationName:lat longitude:lng dict:dict];
    
}

-(void)getLocationName:(NSString*)lat longitude:(NSString*)lng dict: (NSDictionary*)dict
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    NSString *api = [NSString stringWithFormat:kGeocodelocation,lat,lng];
    [HttpManager apiDetails:nil serviceName:api httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            NSDictionary *result = [responce objectForKey:@"result"];
            NSString *address = [result objectForKey:@"display_name"];
            [self getaddress:address dict:dict];
        }
        else
        {
            [self getaddress:@"-" dict:dict];
        }
    }];
}
-(void)getaddress: (NSString*)address dict:(NSDictionary*)dict
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [APP_DELEGATE hideActivityIndcicator];
        displayname = address;
        [self performSegueWithIdentifier:@"Menu" sender:dict];
    });
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier] isEqualToString:@"Menu"])
    {
        MenuListViewController *menuListview = (MenuListViewController*)segue.destinationViewController;
    
        menuListview.displayname = displayname;
        if([sender isKindOfClass:[NSString class]])
        {
            menuListview.delegate = self;
            menuListview.menuType = [sender description];
        }
        else
        {
            menuListview.menuType = @"Detail";
            menuListview.vehicleDetailDict = sender;
        }
    }
    else if ([[segue identifier] isEqualToString:@"Cluster"])
    {
        ClusterViewController *clusterView = (ClusterViewController*)segue.destinationViewController;
        clusterView.loadedString = sender;
        if ([deviemodel isEqualToString:@"ARGO2500"] || [deviemodel isEqualToString:@"ARGO4500"])
        {
           clusterView.isArgo2500 = @"true";
        }
        else
        {
            clusterView.isArgo2500 = @"false";
        }
        
    }
}

@end
