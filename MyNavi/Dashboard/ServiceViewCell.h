//
//  ServiceViewCell.h
//  MyNavi
//
//  Created by Vijay on 07/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Selection

-(void)selectedAction:(NSInteger)selectedService forType:(NSString*)type;

@end

@interface ServiceViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel *vehicleNumber,*details;
@property(nonatomic,weak)IBOutlet UIButton *viewButton,*acknowledgeBtn;
- (IBAction)viewService:(UIButton *)sender;
- (IBAction)acknowledge:(UIButton *)sender;
@property(nonatomic,assign)id<Selection>delegate;

@end
