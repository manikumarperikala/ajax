//
//  DetailsViewController.m
//  MyNavi
//
//  Created by Vijay on 07/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "DetailsViewController.h"
#import "CustomViewCell.h"

@interface DetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary *requiredDetails;
}
@end


@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _details[@"vehicleNumber"];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
    self.detailView.rowHeight = UITableViewAutomaticDimension;
    self.detailView.estimatedRowHeight = 150.0f;
    self.detailView.tableFooterView = [UIView new];

    requiredDetails = [[NSMutableDictionary alloc]init];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",[self getDateString:_details[@"givenDate"]]] forKey:@"Service Date"];
    if(_details[@"maintanancedetails"] != nil)
        [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"maintanancedetails"]] forKey:@"Maintenance Details"];
    if(_details[@"rectifiedDate"] != nil)
        [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",[self getDateString:_details[@"rectifiedDate"]]] forKey:@"Rectified Date"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"serviceNumber"]] forKey:@"Service No"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"actualHours"]] forKey:@"service schedule hours"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"varient"]] forKey:@"Variant Code"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n%@\n", _details[@"dealercode"],_details[@"dealerlocation"]] forKey:@"Dealer Code & Location"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"engineHours"]] forKey:@"Actual hours"];
    [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"serviceType"]] forKey:@"Service Type"];
    if(_details[@"machinefaults"] != nil){
        if([[NSString stringWithFormat:@"%@",_details[@"machinefaults"]] containsString:@","])
        {
            [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",[[_details[@"machinefaults"] componentsJoinedByString:@","]stringByReplacingOccurrencesOfString:@"," withString:@"\n"]] forKey:@"Machine Faults"];
        }
        else{
            [requiredDetails setObject:[NSString stringWithFormat:@"%@",_details[@"machinefaults"]] forKey:@"Machine Faults"];
        }
    }
    if(_details[@"cost"] != nil)
        [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",_details[@"cost"]] forKey:@"Cost"];
    
    if(_details[@"enginefaults"] != nil){
        if([[NSString stringWithFormat:@"%@",_details[@"enginefaults"]] containsString:@","])
        {
            [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n",[[_details[@"enginefaults"] componentsJoinedByString:@","]stringByReplacingOccurrencesOfString:@"," withString:@"\n"]] forKey:@"Engine Faults"];
        }
        else{
            [requiredDetails setObject:[NSString stringWithFormat:@"%@",_details[@"enginefaults"]] forKey:@"Engine Faults"];
        }
    }
    if(_details[@"varienceinHours"] != nil)
        [requiredDetails setObject:[NSString stringWithFormat:@"\n%@\n", _details[@"varienceinHours"]]  forKey:@"Variance in terms of hours "];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 50.0f;
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return requiredDetails.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.name.text = [[NSString stringWithFormat:@"%@ :",[[requiredDetails allKeys] objectAtIndex:indexPath.row]] capitalizedString];
    NSString *str = [NSString stringWithFormat:@"%@",[requiredDetails objectForKey:[requiredDetails allKeys][indexPath.row]]];
    if(![str isEqualToString:@"<null>"] && ![str isEqualToString:@""])
        cell.value.text = [[NSString stringWithFormat:@"%@",[requiredDetails objectForKey:[requiredDetails allKeys][indexPath.row]]] capitalizedString];
    else
        cell.value.text = @"NA";
    return cell;
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd MMM, yyyy"];
    
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
