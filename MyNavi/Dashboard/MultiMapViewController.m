//
//  MultiMapViewController.m
//  AjaxDashboard
//
//  Created by Vijay on 14/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import "MultiMapViewController.h"

@interface MultiMapViewController ()

@end

@implementation MultiMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Multi Map View";
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];

    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectcustomer:)];
    [tapGesture setNumberOfTapsRequired:1];
    
    [self.selectCustomerView addGestureRecognizer:tapGesture];
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
    self.mapView.delegate = self;
    
    self.total.layer.cornerRadius = 5.0;
    self.total.layer.masksToBounds = YES;
    [self.mapView bringSubviewToFront:self.total];
}

-(void)popAction:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    CLLocation *location = locations.firstObject;
    [self setLocationWith:location];
    [manager stopUpdatingLocation];
    manager.delegate = nil;
}

-(void)setLocationWith:(CLLocation*)location{
    
    GMSCameraPosition *position = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:5.0];
    self.mapView.camera = position;
//    [self.mapView setMyLocationEnabled:YES];
    
    GMSMarker *marker = [[GMSMarker alloc]init];
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mapMarker"]];
    image.frame = CGRectMake(0, 0, 40, 40);
    image.contentMode = UIViewContentModeScaleToFill;
    marker.iconView = image;
    marker.position = location.coordinate;
    marker.tracksViewChanges = YES;
    marker.appearAnimation = kGMSMarkerAnimationPop;

    marker.map = self.mapView;
    
}
-(void)selectcustomer:(UIGestureRecognizer*)recognizer{
    
    
}



@end
