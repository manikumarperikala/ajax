//
//  AssignmentsViewController.h
//  MyNavi
//
//  Created by Vijay on 26/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)IBOutlet UITableView *listView;

@end
