//
//  FeedbackView.h
//  MyNavi
//
//  Created by Vijay on 10/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMTextView.h"

@protocol CustomAlertDelegate

-(void)selectedActionType:(NSString*)type forId:(NSString*)iD message:(NSString*)msg;

@end

@interface FeedbackView : UIView<UITextViewDelegate>

@property(nonatomic,assign) NSString *iD;
@property(nonatomic,weak) IBOutlet SAMTextView *textView;
@property(nonatomic,assign) id<CustomAlertDelegate>delegate;

@end
