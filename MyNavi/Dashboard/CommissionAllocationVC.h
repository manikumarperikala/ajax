//
//  CommissionAllocationVC.h
//  MyNavi
//
//  Created by Vijay on 27/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommissionAllocationVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UITableView *listView;

@end
