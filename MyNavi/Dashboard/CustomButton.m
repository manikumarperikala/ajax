//
//  CustomButton.m
//  MyNavi
//
//  Created by Vijay on 17/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

- (void)drawRect:(CGRect)rect {
    
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 0.5f;
}


@end
