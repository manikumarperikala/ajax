//
//  ServiceManDashboardVC.m
//  MyNavi
//
//  Created by Vijay on 26/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ServiceManDashboardVC.h"
#import "ServiceManCell.h"
#import "ServiceDetailModel.h"

@interface ServiceManDashboardVC ()
{
    NSArray *itemsArray;
}
@end

@implementation ServiceManDashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setslideViewController];
    
    itemsArray = @[@{@"Name":@"Service Notifications",@"Image":[UIImage imageNamed:@"ServiceNotification-512"]},@{@"Name":@"Commissioning Allocation",@"Image":[UIImage imageNamed:@"CommissioningAllocation-512"]},@{@"Name":@"Service Reports",@"Image":[UIImage imageNamed:@"MachineCommission-512"]},@{@"Name":@"Commissioned Reports",@"Image":[UIImage imageNamed:@"MachinesDetails-512"]}];

   self.welcomeLabel.text = [[NSString stringWithFormat:@"%@ to %@ %@",self.welcomeLabel.text,[kUserDefaults objectForKey:KFirstName],[kUserDefaults objectForKey:KLastName]] capitalizedString];
}

-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return itemsArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceManCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.layer.borderWidth = 0.5;
    NSDictionary *dict = itemsArray[indexPath.row];
    cell.imageView.image = dict[@"Image"];
    cell.nameLabel.text = dict[@"Name"];

    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize size = CGSizeMake((collectionView.frame.size.width)/2, (collectionView.frame.size.height)/2);
    
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
                [self performSegueWithIdentifier:@"Assignment" sender:nil];
            break;
        case 1:
                [self performSegueWithIdentifier:@"Allocation" sender:nil];
            break;
        case 2:
                [self performSegueWithIdentifier:@"ServiceCommision" sender:nil];
            break;
        case 3:
                [self performSegueWithIdentifier:@"Machine" sender:nil];
            break;
            
        default:
            break;
    }
}

-(void)intWithNumber:(int)x second:(int)y completionHandler:(void(^)(int result,NSError* error))completionHandler
{
    completionHandler(x+y,nil);
}


@end
