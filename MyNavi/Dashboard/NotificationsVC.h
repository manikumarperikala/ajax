//
//  NotificationsVC.h
//  MyNavi
//
//  Created by TrakitNow on 9/18/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,SWRevealViewControllerDelegate>
@property(nonatomic,weak) IBOutlet UITableView *notificationList;
@end

NS_ASSUME_NONNULL_END
