//
//  VehicleParkViewController.h
//  AjaxDashboard
//
//  Created by Vijay on 14/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehicleParkViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (nonatomic,strong) IBOutlet UIButton *firstBtn,*secondBtn,*thirdBtn,*forthBtn;

@end
