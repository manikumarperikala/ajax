//
//  DetailsViewController.h
//  MyNavi
//
//  Created by Vijay on 07/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property(nonatomic,strong) NSDictionary *details;
@property(nonatomic,strong) IBOutlet UITableView *detailView;

@end
