//
//  ClientDashboardViewController.h
//  MyNavi
//
//  Created by Manikumar on 06/12/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientDashboardViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,weak) IBOutlet UILabel *welcomeLabel;
@end
