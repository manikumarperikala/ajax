//
//  ClientDashboardViewController.m
//  MyNavi
//
//  Created by Manikumar on 06/12/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import "ClientDashboardViewController.h"
#import "CollectionViewCell.h"

@interface ClientDashboardViewController ()<SWRevealViewControllerDelegate>
{
    NSArray *itemsArray;
}
@end

@implementation ClientDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Client Dashboard";
    [self setslideViewController];
    itemsArray = @[@{@"Name":@"Vehicle Park",@"Image":[UIImage imageNamed:@"vehiclePark"]},@{@"Name":@"Upcoming Service",@"Image":[UIImage imageNamed:@"upComingServices"]},@{@"Name":@"Customers",@"Image":[UIImage imageNamed:@"customers"]},@{@"Name":@"Multi Map View",@"Image":[UIImage imageNamed:@"multiMapView"]}];
    self.welcomeLabel.text = [[NSString stringWithFormat:@"%@ to %@ %@",self.welcomeLabel.text,[kUserDefaults objectForKey:KFirstName],[kUserDefaults objectForKey:KLastName]] capitalizedString];
}


-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return itemsArray.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.layer.borderWidth = 0.5;
    
    NSDictionary *dict = itemsArray[indexPath.row];
    cell.nameLabel.text = dict[@"Name"];
    cell.imageView.image = dict[@"Image"];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGSize size = CGSizeMake((collectionView.frame.size.width-10)/2, (collectionView.frame.size.height)/2);
    
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   /* switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"Vehicle Park" sender:nil];
            break;
        case 1:
            [self performSegueWithIdentifier:@"Service" sender:nil];
            break;
        case 2:
            [self performSegueWithIdentifier:@"Customer" sender:nil];
            break;
        case 3:
            [self performSegueWithIdentifier:@"MultiMapView" sender:nil];
            break;
            
        default:
            break;
    }*/
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
