//
//  AssignmentViewCell.h
//  MyNavi
//
//  Created by Vijay on 23/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssignmentViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pinNo;
@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UILabel *serviceNo;

@end
