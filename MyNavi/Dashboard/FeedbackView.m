//
//  FeedbackView.m
//  MyNavi
//
//  Created by Vijay on 10/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "FeedbackView.h"

@implementation FeedbackView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    _textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _textView.layer.borderWidth = 0.5f;
    _textView.placeholder = @"Please enter your feedback";
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    
    if ([text length] == 1 && resultRange.location != NSNotFound)
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)okAction:(id)sender {
    [_delegate selectedActionType:@"OK" forId:_iD message:_textView.text];
}

- (IBAction)cancelAction:(id)sender {
    [_delegate selectedActionType:@"Cancel" forId:_iD message:_textView.text];
}



@end
