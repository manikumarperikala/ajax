//
//  CustomerDashboardViewController.h
//  MyNavi
//
//  Created by Vijay on 18/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaddingLabel.h"

@interface CustomerDashboardViewController : UIViewController

@property (nonatomic,strong) IBOutlet UIButton *firstBtn,*secondBtn,*thirdBtn,*forthBtn;
@property (nonatomic,strong) IBOutlet UITableView *vehicleListView;
@property (strong, nonatomic) IBOutlet PaddingLabel *clusterHeader;

@end
