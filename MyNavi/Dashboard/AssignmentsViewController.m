//
//  AssignmentsViewController.m
//  MyNavi
//
//  Created by Vijay on 26/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "AssignmentsViewController.h"
#import "AssignmentViewCell.h"
#import "ServiceCommonDetailViewController.h"
#import "ServiceUpdateVC.h"




@interface AssignmentsViewController()
{
    NSMutableArray *notificationsList;
}

@end

@implementation AssignmentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Service Notifications";
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    notificationsList = [NSMutableArray array];
    [self getServiceAssignments];
}

-(void)getServiceAssignments
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat: ServiceNotifications,[kUserDefaults objectForKey:kApplicationId]] httpType:kGet WithBlock:^(id responce, NSError *error) {
        
        if(error == nil)
        {
            for(NSDictionary *dict in responce[@"rows"])
            {
                if([[NSString stringWithFormat:@"%@",dict[@"servicedone"]] isEqualToString:kServicePending])
                {
                    [notificationsList addObject:dict];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(notificationsList.count == 0)
            {
                UILabel *label = [[UILabel alloc]initWithFrame:self.listView.bounds];
                label.text = @"No Data Available";
                label.textAlignment = NSTextAlignmentCenter;
                self.listView.backgroundView = label;
            }
            else
            {
                self.listView.backgroundView = nil;
            }
            [self.listView reloadData];
            [APP_DELEGATE hideActivityIndcicator];
        });
    }];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return notificationsList != nil ? 1 : 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notificationsList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssignmentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row % 2 != 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *dict = notificationsList[indexPath.row];
    if(dict[@"pin"] != nil || [[NSString stringWithFormat:@"%@",dict[@"pin"]] isEqualToString:@"(null)"])
        cell.pinNo.text = @"-";
    else
        cell.pinNo.text = [NSString stringWithFormat:@"%@",dict[@"pin"]];
    cell.customerName.text = [dict[@"customername"] capitalizedString];
    cell.serviceNo.text = [NSString stringWithFormat:@"%@",dict[@"serviceNumber"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict = notificationsList[indexPath.row];

    [self performSegueWithIdentifier:kUpdate sender:dict[@"id"]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:kUpdate])
    {
        ServiceUpdateVC *updateVC = [segue destinationViewController];
        updateVC.serviceId = sender;
    }
}


@end
