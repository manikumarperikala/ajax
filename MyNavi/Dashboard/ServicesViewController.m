//
//  ServicesViewController.m
//  MyNavi
//
//  Created by Vijay on 07/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ServicesViewController.h"
#import "ServiceViewCell.h"
#import "DetailsViewController.h"
#import "FeedbackView.h"

@interface ServicesViewController ()<Selection,CustomAlertDelegate>
{
    NSArray *services;
    FeedbackView *feedbackView;
}

@end

@implementation ServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Services";
    [self setslideViewController];
    [self getServices];
}

-(void)getServices{
   /* NSString *type;
    NSString *servicename;
    if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
    {
        type = DealerVehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KdealerName]];
    }
    else
    {
        type = VehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KLoginName]];
    }*/
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat:@"/mynavi/servicedone/list?customerCode=%@",[kUserDefaults objectForKey:KLoginName]] httpType:kGet WithBlock:^(id responce, NSError *error)
    {
    
        if(error == nil)
        {
            services = responce[@"rows"];
        }
            dispatch_async(dispatch_get_main_queue(), ^{
                if(services.count > 0)
                    [self.servicesListView reloadData];
                else
                {
                    UILabel *label = [[UILabel alloc]initWithFrame:self.servicesListView.frame];
                    label.text = @"No Pending Services";
                    label.textAlignment = NSTextAlignmentCenter;
                    self.servicesListView.backgroundView = label;
                }
                [APP_DELEGATE hideActivityIndcicator];
            });
    }];
    
}

-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return services.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ServiceViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.viewButton.tag = indexPath.row;
    cell.acknowledgeBtn.tag = indexPath.row;
    NSDictionary *dict = [services objectAtIndex:indexPath.row];
    cell.vehicleNumber.text = dict[@"vehicleNumber"];
    cell.details.text = [NSString stringWithFormat:@"%@\nDate of Service : %@",dict[@"deviceModel"],[self getDateString:dict[@"givenDate"]]];
    if(![[NSString stringWithFormat:@"%@",dict[@"servicedone"]] isEqualToString:@"1"])
    {
        [cell.viewButton setTitle:@"In Progress" forState:UIControlStateNormal];
        [cell.viewButton setBackgroundColor:[UIColor orangeColor]];
    }
    else
    {
        [cell.viewButton setTitle:@"View" forState:UIControlStateNormal];
        if([[NSString stringWithFormat:@"%@",dict[@"feedbackbycustomer"]] isEqualToString:@"1"])
        {
            cell.acknowledgeBtn.hidden = YES;
        }
        else
            cell.acknowledgeBtn.hidden = NO;
    }
    
    return cell;
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd MMM, yyyy"];
    
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

-(void)selectedAction:(NSInteger)selectedService forType:(NSString *)type{
    
    if([type isEqualToString:@"View"])
    {
        [self performSegueWithIdentifier:@"Detail" sender:services[selectedService]];
    }
    else
    {
        NSDictionary* dict = services[selectedService];

       
        [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            feedbackView = (FeedbackView*)[[[NSBundle mainBundle] loadNibNamed:@"FeedbackView" owner:self options:nil] firstObject];
            
            feedbackView.frame = self.view.bounds;
            feedbackView.delegate = self;
            feedbackView.iD = dict[@"id"];
            [self.view addSubview:feedbackView];
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
}

-(void)selectedActionType:(NSString *)type forId:(NSString *)iD message:(NSString *)msg
{
    if([type isEqualToString:@"OK"])
    {
        [HttpManager apiDetails:@{@"id":iD,@"message":msg} serviceName:@"/mynavi/servicefeedback" httpType:kPost WithBlock:^(id responce, NSError *error) {
         if(error == nil)
         {
             [self getServices];
         }
         }];
        
        [feedbackView removeFromSuperview];
        feedbackView = nil;
    }
    else
    {
        [feedbackView removeFromSuperview];
        feedbackView = nil;
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"Detail"])
    {
        DetailsViewController *detailView = (DetailsViewController*)segue.destinationViewController;
        detailView.details = sender;
    }
}







@end
