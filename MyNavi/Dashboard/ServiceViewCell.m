//
//  ServiceViewCell.m
//  MyNavi
//
//  Created by Vijay on 07/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ServiceViewCell.h"



@implementation ServiceViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.viewButton.layer.borderWidth = 0.5;
    self.viewButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.acknowledgeBtn.layer.borderWidth = 0.5;
    self.acknowledgeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.acknowledgeBtn.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)viewService:(UIButton *)sender {
    if([[sender currentTitle] isEqualToString:@"View"])
        [self.delegate selectedAction:sender.tag forType:@"View"];
}

- (IBAction)acknowledge:(UIButton *)sender {
    
    [self.delegate selectedAction:sender.tag forType:@"Acknowledge"];
}
@end
