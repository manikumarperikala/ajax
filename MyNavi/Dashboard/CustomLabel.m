//
//  CustomLabel.m
//  MyNavi
//
//  Created by Vijay on 17/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5f;
}



@end
