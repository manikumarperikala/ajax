//
//  ServiceManDashboardVC.h
//  MyNavi
//
//  Created by Vijay on 26/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceManDashboardVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property(nonatomic,strong) IBOutlet UICollectionView *collectionView;

@end
