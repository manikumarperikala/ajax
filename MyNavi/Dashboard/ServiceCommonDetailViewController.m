//
//  ServiceCommonDetailViewController.m
//  MyNavi
//
//  Created by Vijay on 27/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ServiceCommonDetailViewController.h"
#import "CustomControls.h"

@interface ServiceCommonDetailViewController ()
{
    NSMutableDictionary *requiredDetails;
    NSArray *requiredKeys;
}
@end

@implementation ServiceCommonDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Details";
    requiredDetails = [NSMutableDictionary dictionary];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150.0f;
    self.tableView.tableFooterView = [UIView new];

requiredKeys = @[@"deviceModel",@"varient",@"vehicleNumber",@"engineHours",@"serviceNumber",@"serviceType",@"serviceEngineername",@"givenDate",@"rectifiedDate",@"machinefaults",@"enginefaults",@"maintanancedetails",@"cost"];
    
    for(NSString *key in requiredKeys)
    {
        if([key isEqualToString:@"givenDate"] || [key isEqualToString:@"rectifiedDate"])
        {
            [requiredDetails setObject:[[CustomControls sharedObj]getFormattedDate:self.details[key]] forKey:key];
        }
        else
        {
            if(![self.details[key] isKindOfClass:[NSArray class]])
            {
                [requiredDetails setObject:[[NSString stringWithFormat:@"%@", self.details[key]]capitalizedString] forKey:key];
            }
            else
            {
                [requiredDetails setObject:[NSString stringWithFormat:@"%@", [self.details[key] componentsJoinedByString:@","]] forKey:key];
            }
        }
    }
//    [self getServiceDetails];
  
    
}
-(void)getServiceDetails{
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat:ServiceDetails,_serviceId] httpType:@"GET" WithBlock:^(id responce, NSError *error) {
            for(NSString *key in requiredKeys)
            {
               [requiredDetails setObject:[NSString stringWithFormat:@"%@", responce[key]] forKey:key];
            }
    }];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return requiredKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [(UILabel*)[cell viewWithTag:10] setText:[NSString stringWithFormat:@"%@ :",[requiredKeys[indexPath.row] capitalizedString]]];
    [(UILabel*)[cell viewWithTag:11] setText:requiredDetails[requiredKeys[indexPath.row]]];
    
    return cell;
}



@end
