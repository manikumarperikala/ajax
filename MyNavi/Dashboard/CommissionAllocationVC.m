//
//  CommissionAllocationVC.m
//  MyNavi
//
//  Created by Vijay on 27/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CommissionAllocationVC.h"
#import "AssignmentViewCell.h"

@interface CommissionAllocationVC ()
{
    NSArray *allocationsList;
}

@end

@implementation CommissionAllocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Commissioning Allocation";
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    [self getCommissionAllocation];
}

-(void)getCommissionAllocation{
    
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat:CommissioningAllocation,[kUserDefaults objectForKey:kApplicationId],@"false"] httpType:kGet WithBlock:^(id responce, NSError *error) {
        
        if(error == nil)
        {
            allocationsList = responce[@"rows"];
            if(allocationsList.count == 0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UILabel *label = [[UILabel alloc]initWithFrame:self.listView.bounds];
                    label.text = @"No Data Available";
                    label.textAlignment = NSTextAlignmentCenter;
                    self.listView.backgroundView = label;
                });
            }
            else
                self.listView.backgroundView = nil;
        }
        else
        {}
    }];
    
}

#pragma mark UITableViewDatasource & Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (allocationsList == nil) ? 0 : 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allocationsList.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AssignmentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(indexPath.row % 2 != 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"AllocationDetail" sender:nil];
}

-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
