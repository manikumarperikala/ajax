//
//  VehiclesView.h
//  MyNavi
//
//  Created by Vijay Bhaskar on 05/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectionDelegate

-(void)selectedVehicleNumber:(NSString*)number devicemodel:(NSString *)model;
-(void)removeView;

@end

@interface VehiclesView : UIView<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSMutableArray *vehicles;
@property(nonatomic,assign) id<SelectionDelegate>delegate;

@end
