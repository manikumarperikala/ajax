//
//  VehicleParkViewController.m
//  AjaxDashboard
//
//  Created by Vijay on 14/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import "VehicleParkViewController.h"
#import "CustomViewCell.h"


@interface VehicleParkViewController ()
{
    NSArray *list;
    NSArray *vehicleParkList;
    NSMutableArray *filteredList;
}

@end

@implementation VehicleParkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
    self.title = @"Vehicle Park";
    [self vehicleParkDetails];
    list = @[@{@"Name":@"Standard",@"Value":@"04"},@{@"Name":@"Drum Swivel",@"Value":@"02"},@{@"Name":@"Bucket Sliding Door",@"Value":@"04"},@{@"Name":@"AdMixture",@"Value":@"03"}];
}

-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)vehicleParkDetails
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    NSString *api = [NSString stringWithFormat:VehicleParkApi,[kUserDefaults objectForKey:KdealerName]];
    [HttpManager apiDetails:nil serviceName:api httpType:@"GET" WithBlock:^(id responce, NSError *error) {
        if(error == nil)
        {
            vehicleParkList = responce[@"rows"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self filterdata:@"ARGO1000"];
                [self.tableView reloadData];
                [APP_DELEGATE hideActivityIndcicator];
            });
        }
    }];
}
-(IBAction)performSelection:(UIButton*)sender
{
    
    switch (sender.tag) {
            
        case 10:
                [sender setBackgroundColor:kHighlitedColor];
                [self.secondBtn setBackgroundColor:kNormalColor];
                [self.thirdBtn setBackgroundColor:kNormalColor];
                [self.forthBtn setBackgroundColor:kNormalColor];
            break;
            
        case 11:
                [sender setBackgroundColor:kHighlitedColor];
                [self.firstBtn setBackgroundColor:kNormalColor];
                [self.thirdBtn setBackgroundColor:kNormalColor];
                [self.forthBtn setBackgroundColor:kNormalColor];
            break;
            
        case 12:
                [sender setBackgroundColor:kHighlitedColor];
                [self.secondBtn setBackgroundColor:kNormalColor];
                [self.firstBtn setBackgroundColor:kNormalColor];
                [self.forthBtn setBackgroundColor:kNormalColor];
            break;
            
        case 13:
                [sender setBackgroundColor:kHighlitedColor];
                [self.secondBtn setBackgroundColor:kNormalColor];
                [self.thirdBtn setBackgroundColor:kNormalColor];
                [self.firstBtn setBackgroundColor:kNormalColor];
            break;
            
        default:
            break;
    }
}
-(void)filterdata: (NSString*) str
{
    [filteredList removeAllObjects];
    for (int i = 0; i < vehicleParkList.count; i++) {
        NSDictionary *dict = vehicleParkList[i];
        NSLog(@"%@",dict);
        NSDictionary *itemdict = [dict objectForKey:@"item"];
        NSString *model = [itemdict objectForKey:@"deviceModel"];
        if ([str isEqualToString:model])
        {
            [filteredList addObject:dict];
        }
    }
    [self.tableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return list.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *dict = list[indexPath.row];
    cell.name.text = dict[@"Name"];
    cell.value.text = dict[@"Value"];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(IDIOM == IPAD)
        return  60.0f;
    
    return 40.0f;
}

@end
