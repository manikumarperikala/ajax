//
//  MenuListViewController.m
//  MyNavi
//
//  Created by Vijay on 23/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "MenuListViewController.h"

@interface MenuListViewController ()
{
  
}
@end

@implementation MenuListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:self.menuType];
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
}

-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 9)
    {
        if(IDIOM == IPAD)
            return 100.0f;
        
        return 80.0f;
    }
    if(IDIOM == IPAD)
        return 64.0f;
    
    return 44.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel *nameLabel = [(UILabel*)cell viewWithTag:10];
    UILabel *descLabel = [(UILabel*)cell viewWithTag:11];
    
    NSDictionary *item = [_vehicleDetailDict objectForKey:@"item"];
    if([self.menuType isEqualToString:@"Detail"])
    {
        switch (indexPath.row) {
            case 0:
                nameLabel.text = @"Model   :";
                descLabel.text = [[item objectForKey:@"deviceModel"] uppercaseString];
                break;
            case 1:
                nameLabel.text = @"Vehicle Number   :";
                descLabel.text = [[item objectForKey:@"vehicleNumber"] uppercaseString];
                break;
            case 2:
                nameLabel.text = @"Varient Code   :";
                if([item objectForKey:@"varient"] != nil)
                {
                    
                    descLabel.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"varient"]];
                    
                    if([descLabel.text isEqualToString:@"<null>"])
                        {
                            descLabel.text = @"-";
                        }
                }
                else
                    descLabel.text = @"-";
                break;
            case 3:
                nameLabel.text = @"PIN No   :";
                if([item objectForKey:@"pin"] != nil)
                    descLabel.text = [item objectForKey:@"pin"];
                else
                    descLabel.text = @"-";
                break;
            case 4:
                nameLabel.text = @"Purchase/Install Date   :";
                descLabel.text = [self setShortDate:[item objectForKey:@"createdAt"]];
                break;
            case 5:
                nameLabel.text = @"# of Days Owned   :";
            //    descLabel.text = [self calculateDays:[item objectForKey:@"createdAt"]];
                descLabel.text = [self calculateDays:[[CustomControls sharedObj]addFiveHoursthirtymins:[item objectForKey:@"createdAt"]]];
                break;
            case 6:
                nameLabel.text = @"Schedule Run Hours   :";
                if([_vehicleDetailDict objectForKey:@"schedulerunHours"] != nil)
                    descLabel.text = [NSString stringWithFormat:@"%@",[_vehicleDetailDict objectForKey:@"schedulerunHours"]];
                else
                    descLabel.text = @"-";
                    
                break;
            case 7:
                nameLabel.text = @"Total engine hours  :";
                if([item objectForKey:@"totalEngineHours"] != nil)
                    descLabel.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"totalEngineHours"]];
                else
                    descLabel.text = @"-";
                
                break;
            case 8:
                nameLabel.text = @"Last engine ON   :";
                if([item objectForKey:@"lastEngineOn"] != nil)
                    descLabel.text = [self setShortDate:[item objectForKey:@"lastEngineOn"]];
                else
                    descLabel.text = @"-";
                
                break;
            case 9:
                nameLabel.text = @"Last location name   :";
//                if([item objectForKey:@"vehicleLocation"] != nil)
//                    descLabel.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"vehicleLocation"]];
//                else
//                    descLabel.text = @"-";
//
//                break;
                descLabel.text = _displayname;
                
                break;
            default:
                break;
        }
    }
    else
    {
        nameLabel.text = [NSString stringWithFormat:@"Index %ld",(long)indexPath.row+1];
        nameLabel.textAlignment = NSTextAlignmentLeft;
        descLabel.hidden = YES;
    }
    
    return cell;
}

-(NSString*)calculateDays:(NSString*)createdDate
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *startDate = [f dateFromString:createdDate];
    NSDate *endDate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    return [NSString stringWithFormat:@"%ld",[components day]];

}

-(NSString*)setShortDate:(NSString*)date
{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd'T'hh:mm:ss.SSSZ"];
    NSDate *startDate = [f dateFromString:date];
    [f setDateFormat:@"dd-MM-yyyy"];
    
    return [f stringFromDate:startDate];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(![self.menuType isEqualToString:@"Detail"])
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
        [self.delegate selectMenuItem:[(UILabel*)[cell viewWithTag:10]text]];
    
        [self cancelView:nil];
    }
}

-(void)cancelView:(id)sender{
     [self dismissViewControllerAnimated:YES completion:nil];
}

@end
