//
//  HeaderLabel.m
//  MyNavi
//
//  Created by Vijay on 18/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "HeaderLabel.h"

@implementation HeaderLabel

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 0.5f;
}

-(void)drawRect:(CGRect)rect{
    
    UIEdgeInsets insets = {0, 2, 0, 2};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
