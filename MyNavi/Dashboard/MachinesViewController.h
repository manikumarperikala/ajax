//
//  MachinesViewController.h
//  MyNavi
//
//  Created by Vijay on 11/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MachinesViewController : UIViewController

@property(nonatomic,weak) IBOutlet UITableView *machineCommissionlistView;

@end
