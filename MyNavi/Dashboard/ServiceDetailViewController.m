//
//  ServiceDetailViewController.m
//  MyNavi
//
//  Created by Vijay on 18/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ServiceDetailViewController.h"
#import "CustomViewCell.h"

@interface ServiceDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *details;
    NSArray *keys;
}

@end

@implementation ServiceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    self.title = @"Vehicle Service Detail";
    [self getdetails];
}

-(void)getdetails
{
    NSDictionary *dict = _dict1;
    NSDictionary *itemdict = [dict objectForKey:@"item"];
    details = @{@"Vehicle Number":[self dict:itemdict iskeyexists:@"vehicleNumber"],@"Current Engine":[self dict:itemdict iskeyexists:@"engineNumber"],@"Upcoming Service":[self dict:dict iskeyexists:@"serviceNumber"],@"Schedule Run Hours":[self dict:dict iskeyexists:@"schedulerunHours"],@"Phone Number":[self dict:itemdict iskeyexists:@"alertMobile"],@"CumM Engine Run Hours":[self dict:itemdict iskeyexists:@"totalEngineHours"],@"Fuel Status":[self dict:itemdict iskeyexists:@"currentFuelStatus"],@"Current Location":[self dict:itemdict iskeyexists:@"vehicleLocation"]};
    
    keys = @[@"Vehicle Number",@"Current Engine",@"Upcoming Service",@"Schedule Run Hours",@"Phone Number",@"CumM Engine Run Hours",@"Fuel Status",@"Current Location"];
    [self.tableView reloadData];
}

-(void)popAction:(id)sender{

  //  [self.navigationController popViewControllerAnimated:YES];
     [self dismissViewControllerAnimated:YES completion:nil];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(IDIOM == IPAD)
//        return 70.0f;
//    return 44.0f;
    if (indexPath.row == 7)
    {
        return  120.0f;
    }
    return  50.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (keys.count)
    {
        return keys.count;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.name.text = [NSString stringWithFormat:@"%@ :",keys[indexPath.row]];
    cell.value.text = [details objectForKey:keys[indexPath.row]];
    
    return cell;
    
}
-(NSString *)dict:(NSDictionary *)dict iskeyexists:(NSString *)key
{
    if (([dict objectForKey:key]) != nil)
    {
        if ([dict[key] isKindOfClass:[NSString class]]) {
            NSString *value = [dict objectForKey:key];
            if ((![value  isEqual: @""]) && (![value  isEqual: @"(null)"]))
            {
                return value;
            }
            else
            {
                return @"NA";
            }
        }
        else
        {
            if ([dict[key] isKindOfClass:[NSNumber class]])
            {
            NSNumber *myInteger = [dict objectForKey:key];
            NSString* myNewString = [NSString stringWithFormat:@"%@", myInteger];
            return  myNewString;
            }
        }
       
    }
    return @"NA";
}

@end
