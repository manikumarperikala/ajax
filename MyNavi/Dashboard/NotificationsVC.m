//
//  NotificationsVC.m
//  MyNavi
//
//  Created by TrakitNow on 9/18/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import "NotificationsVC.h"
#import "NotificationViewCell.h"

@interface NotificationsVC ()
{
    NSArray *notifications;
    AppDelegate *delegate;
}
@end

@implementation NotificationsVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.title = @"Notifications";
    [self setslideViewController];
     [self getnotifications];
    
}

-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
-(void)getnotifications
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
   // NSString *notificationsurl = [NSString stringWithFormat:Notifications,delegate.vehicleObject.deviceID];
   
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat:@"%@", Notifications] httpType:kGet WithBlock:^(id responce, NSError *error)
     {
         if(error == nil)
         {
             notifications = responce[@"notificationList"];
         }
         dispatch_async(dispatch_get_main_queue(), ^{
             if(notifications.count > 0)
                 [self.notificationList reloadData];
             else
             {
                 UILabel *label = [[UILabel alloc]initWithFrame:self.notificationList.frame];
                 label.text = @"Notifications are not available";
                 label.textAlignment = NSTextAlignmentCenter;
                 self.notificationList.backgroundView = label;
             }
             [APP_DELEGATE hideActivityIndcicator];
         });
     }];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return notifications.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = [notifications objectAtIndex:indexPath.row];
    cell.notificationdate.text = [NSString stringWithFormat:@"%@", [self getDateString: dict[@"createdAt"]]];
    cell.details.text = dict[@"message"];
    NSString *parameter = [NSString stringWithFormat:@"%@",dict[@"parameter"]];
    if ([parameter isEqualToString:@"opsStatus"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"OPS STATUS"]];
    }
    if ([parameter isEqualToString:@"coolantTemp"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"CoolantTemperature"]];
    }
    if ([parameter isEqualToString:@"hydraulicOilFilter"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"Hydraulic Oil Filter"]];
    }
    if ([parameter isEqualToString:@"batteryLevel"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"BatteryLevel"]];
    }
    if ([parameter isEqualToString:@"fuelLevel"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"FuelLevel"]];
    }
    if ([parameter isEqualToString:@"Parking Switch"])
    {
        [cell.imageview setImage:[UIImage imageNamed:@"Parking Switch"]];
    }
    return cell;
}
-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd MMM, yyyy HH:mm:ss"];
    date = [date dateByAddingTimeInterval:+(330*60)];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
