//
//  CustomersViewController.m
//  MyNavi
//
//  Created by Vijay on 16/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import "CustomersViewController.h"
#import "CustomersDetailVC.h"



@interface CustomersViewController ()
{
    NSArray *customersList;
}
@end

@implementation CustomersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Customers";
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    self.tableView.tableFooterView = [UIView new];
    [self getcustomers];

}
-(void)getcustomers
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
     NSString *api = [NSString stringWithFormat:CustomersApi,[kUserDefaults objectForKey:KdealerName]];
    [HttpManager apiDetails:nil serviceName:api httpType:@"GET" WithBlock:^(id responce, NSError *error) {
        if(error == nil)
        {
            customersList = responce[@"rows"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self.totalCustomers setText: [NSString stringWithFormat:@"Total Customers: %lu",(unsigned long)customersList.count]];
         
                [APP_DELEGATE hideActivityIndcicator];
            });
        }
    }];
}

-(void)popAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IDIOM == IPAD)
        return 60.0f;
    
    return 40.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (customersList.count > 0)
    {
        tableView.backgroundView = nil;
        return customersList.count;
    }
    else
    {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:tableView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        tableView.backgroundView = messageLabel;
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict = customersList[indexPath.row];
    NSDictionary *extradict = [dict objectForKey:@"extras"];
    NSString *firstname = dict[@"firstName"];
    NSString *lastname = dict[@"lastName"];
    NSString *str = [NSString stringWithFormat: @"%@ %s %@", firstname, " ",lastname];
    NSString *ID = dict[@"loginName"];
    [(UILabel*)[cell viewWithTag:10] setText:ID];
    [(UILabel*)[cell viewWithTag:11] setText:str];
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = customersList[indexPath.row];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    CustomersDetailVC *custDetailsVCObj = (CustomersDetailVC *)[storyBoard instantiateViewControllerWithIdentifier:@"CustomersDetailVC"];
    custDetailsVCObj.dict1 = dict;
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:custDetailsVCObj];
    [self.navigationController presentViewController:navi animated:YES completion:nil];
}
@end
