//
//  ClusterViewController.m
//  MyNavi
//
//  Created by Vijay on 23/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ClusterViewController.h"


@interface ClusterViewController ()
{
   NSString *url;
}

@end

@implementation ClusterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    self.navigationController.navigationBar.hidden = YES;
    
    self.title = @"Cluster View";
    
    self.clusterView.scalesPageToFit = YES;
    [self.clusterView setContentMode:UIViewContentModeCenter];

    if ([_isArgo2500  isEqual: @"true"])
    {
        url = [[NSString stringWithFormat:@"http://52.66.184.140:3609/#!/argo2500?vehicleNumber=%@&companyID=AJAXFIORI&token=%@&APIurl=ajaxfiori-web.trakitnow.com/mynavi/cluster",_loadedString,[kUserDefaults objectForKey:KToken]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    else
    {
        url = [[NSString stringWithFormat:@"http://52.66.184.140:3609/#!/?vehicleNumber=%@&companyID=AJAXFIORI&token=%@&APIurl=ajaxfiori-web.trakitnow.com/mynavi/cluster",_loadedString,[kUserDefaults objectForKey:KToken]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }

    NSURLRequest *newRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self.clusterView loadRequest:newRequest];
  
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    [UIApplication sharedApplication].statusBarHidden = YES;
    self.view.center = CGPointMake(([UIScreen mainScreen].bounds.size.width/2), [UIScreen mainScreen].bounds.size.height/2);
    CGFloat angle = 90 * M_PI / 180;
    self.view.transform = CGAffineTransformMakeRotation(angle);
    self.view.bounds = CGRectMake(0, 0,self.view.frame.size.height , self.view.frame.size.width);
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];

}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APP_DELEGATE hideActivityIndcicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

-(IBAction)back:(id)sender
{
    [self popAction:sender];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
