//
//  MachineCommissionVC.m
//  MyNavi
//
//  Created by Vijay on 28/12/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "MachineCommissionVC.h"

@interface MachineCommissionVC ()

@end

@implementation MachineCommissionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UITableViewDatasource & Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 13;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 34.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}
- (IBAction)sendDetails:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"MachineDetail" sender:nil];
}


@end
