//
//  MenuListViewController.h
//  MyNavi
//
//  Created by Vijay on 23/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuDelegate

-(void)selectMenuItem:(NSString*)item;

@end

@interface MenuListViewController : UITableViewController

@property (nonatomic,strong) id<MenuDelegate>delegate;

@property (strong, nonatomic) IBOutlet UITableView *menutableView;
@property (nonatomic,strong) NSDictionary *vehicleDetailDict;
@property (nonatomic,strong) NSString *displayname;
@property (nonatomic,strong) NSString *menuType;

@end
