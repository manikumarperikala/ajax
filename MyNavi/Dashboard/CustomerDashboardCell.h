//
//  CustomerDashboardCell.h
//  MyNavi
//
//  Created by Vijay on 24/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerDashboardCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *model,*vehicleNumber,*nextService,*engineHours;


@end
