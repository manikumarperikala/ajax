//
//  VehiclesView.m
//  MyNavi
//
//  Created by Vijay Bhaskar on 05/11/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "VehiclesView.h"
#import "VehicleObject.h"



@implementation VehiclesView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _vehicles.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    VehicleObject *obj = _vehicles[indexPath.row];
    cell.textLabel.text = [obj.vehicleNumber uppercaseString];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    VehicleObject *obj = _vehicles[indexPath.row];
    NSString *model = [obj.deviceModel uppercaseString];
    [self.delegate selectedVehicleNumber:cell.textLabel.text devicemodel: model];
}
- (IBAction)removeView:(id)sender {
    [self.delegate removeView];
}

@end
