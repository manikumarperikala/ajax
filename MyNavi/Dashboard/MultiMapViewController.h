//
//  MultiMapViewController.h
//  AjaxDashboard
//
//  Created by Vijay on 14/08/17.
//  Copyright © 2017 Vijay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface MultiMapViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate>{
    
    CLLocationManager *locationManager;
    
}

@property (nonatomic,strong) IBOutlet UIView *selectCustomerView;
@property (nonatomic,strong) IBOutlet GMSMapView *mapView;

@property (nonatomic,strong) IBOutlet UILabel *agro1000;
@property (nonatomic,strong) IBOutlet UILabel *agro2000;
@property (nonatomic,strong) IBOutlet UILabel *agro2500;
@property (nonatomic,strong) IBOutlet UILabel *agro4000;

@property (nonatomic,strong) IBOutlet UILabel *total;


@end
