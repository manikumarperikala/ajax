//
//  CustomersDetailVC.h
//  MyNavi
//
//  Created by Manikumar on 04/12/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomersDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *customerGridView;
@property (nonatomic,strong) NSDictionary *dict1;
@end
