//
//  ServiceDetailViewController.h
//  MyNavi
//
//  Created by Vijay on 18/08/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetailViewController : UIViewController

@property (nonatomic,strong) IBOutlet UITableView *tableView;
@property (weak,nonatomic) NSDictionary *dict1;
@end
