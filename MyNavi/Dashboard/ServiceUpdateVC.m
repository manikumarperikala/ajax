//
//  ServiceUpdateVC.m
//  MyNavi
//
//  Created by Vijay on 25/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import "ServiceUpdateVC.h"

@interface ServiceUpdateVC ()

@end

@implementation ServiceUpdateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
    self.title = @"Add Vehicle Details";
    
    self.rectifiedDate.text = [self getDate:[NSDate date]];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (IBAction)update:(id)sender {
    
    if([self.machineFaults.text isEqualToString:@""]||[self.engineFaults.text isEqualToString:@""]||[self.maintainDetails.text isEqualToString:@""]||[self.cost.text isEqualToString:@""])
    {
        [[CustomControls sharedObj]presentAlertController:@"Please enter all vehicle details" delegate:self];
    }
    else
    {
        NSDictionary *params = @{@"id":self.serviceId,@"machinefaults":self.machineFaults.text,@"enginefaults":self.engineFaults.text,@"maintanancedetails":self.maintainDetails.text,@"cost":self.cost.text,@"rectifieddate":self.rectifiedDate.text};
        [HttpManager apiDetails:params serviceName:ServiceUpdate httpType:kPost WithBlock:^(id responce, NSError *error) {
            
            if(error == nil)
            {
                if([responce[@"status"] isEqualToString:@"success"])
                    [[CustomControls sharedObj]presentAlertControllerWithTitle:[responce[@"status"] capitalizedString] message:@"Service updated successfully" delegate:self block:^(BOOL didCancel) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
            }
            else
                [[CustomControls sharedObj]presentAlertController:@"Request Failed" delegate:self];
        }];
    }
}

-(NSString*)getDate:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    return [formatter stringFromDate:date];
}

@end
