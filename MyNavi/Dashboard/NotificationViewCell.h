//
//  NotificationViewCell.h
//  MyNavi
//
//  Created by TrakitNow on 9/18/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface NotificationViewCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UILabel *notificationdate,*details;
@property(nonatomic,strong)IBOutlet UIImageView *imageview;


@end


