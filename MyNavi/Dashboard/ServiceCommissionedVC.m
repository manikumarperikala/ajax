//
//  ServiceCommissionedVC.m
//  MyNavi
//
//  Created by Vijay on 11/01/18.
//  Copyright © 2018 Ramakrishna. All rights reserved.
//

#import "ServiceCommissionedVC.h"
#import "AssignmentViewCell.h"
#import "ServiceCommonDetailViewController.h"


@interface ServiceCommissionedVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *list;
}
@end

@implementation ServiceCommissionedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Service Reports";
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    list = [NSMutableArray array];
    [self getServiceAssignments];
}
-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getServiceAssignments{
    
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    
    [HttpManager apiDetails:nil serviceName:[NSString stringWithFormat: ServiceNotifications,[kUserDefaults objectForKey:kApplicationId]] httpType:kGet WithBlock:^(id responce, NSError *error) {
        
        if(error == nil)
        {
            for(NSDictionary *dict in responce[@"rows"])
            {
                if([[NSString stringWithFormat:@"%@",dict[@"servicedone"]] isEqualToString:kServiceDone])
                {
                    [list addObject:dict];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(list.count == 0)
            {
                UILabel *label = [[UILabel alloc]initWithFrame:self.listView.bounds];
                label.text = @"No Data Available";
                label.textAlignment = NSTextAlignmentCenter;
                self.listView.backgroundView = label;
            }
            else
            {
                self.listView.backgroundView = nil;
            }
            [self.listView reloadData];
            [APP_DELEGATE hideActivityIndcicator];
        });
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return list.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssignmentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(indexPath.row % 2 != 0)
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *dict = list[indexPath.row];
    if(dict[@"pin"] != nil || [[NSString stringWithFormat:@"%@",dict[@"pin"]] isEqualToString:@"(null)"])
        cell.pinNo.text = @"-";
    else
        cell.pinNo.text = [NSString stringWithFormat:@"%@",dict[@"pin"]];
    cell.customerName.text = [dict[@"customername"] capitalizedString];
    cell.serviceNo.text = [NSString stringWithFormat:@"%@",dict[@"serviceNumber"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = [list objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:kDetail sender:dict];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:kDetail])
    {
        ServiceCommonDetailViewController *detailView = [segue destinationViewController];
        detailView.serviceId = [NSString stringWithFormat:@"%@",sender[@"id"]];
        detailView.details = sender;
    }
    
}

@end
