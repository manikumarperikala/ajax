//
//  BikeLocationListVC.h
//  AjaxFiori
//
//  Created by sridhar on 07/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehiclesListVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *listView;

@end
