//
//  BikeLocationListVC.m
//  AjaxFiori
//
//  Created by sridhar on 07/04/17.
//  Copyright © 2017 Trakitnow. All rights reserved.
//

#import "VehiclesListVC.h"
#import "TabViewController.h"
#import "HomeCell.h"
#import "VehicleObject.h"
#import "CustomControls.h"


@interface VehiclesListVC ()<SWRevealViewControllerDelegate>
{
    NSMutableArray *vehicleListArray;
}
@end

@implementation VehiclesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    vehicleListArray = [[NSMutableArray alloc]init];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    self.title = @"Vehicles";
    
    [self setslideViewController];
    
    self.listView.tableFooterView = [UIView new];
    
    //[self registerDeviceforPushNotifications];
}

-(void)poproot:(id)sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self getVechiclesList];
}

//-(void)registerDeviceforPushNotifications
//{
//    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] != nil)
//
//        [HttpManager apiDetails:@{@"deviceId":deviceID,@"pushToken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],@"platform":@"IOS"} serviceName:[NSString stringWithFormat:@"%@",DeviceDataApi] httpType:@"POST" WithBlock:^(id responce, NSError *error) {
//            NSLog(@"%@RESPONSE:",responce);
//    }];
//}

-(void)getVechiclesList
{
    NSString *type;
    NSString *servicename;
    if([[kUserDefaults objectForKey:KRoleID] isEqualToString:kDealerID])
    {
        type = DealerVehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KdealerName]];
    }
    else
    {
        type = VehiclesList;
        servicename = [NSString stringWithFormat:type,[kUserDefaults objectForKey:KLoginName]];
    }
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:servicename httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            [vehicleListArray removeAllObjects];
            NSArray *rows = [responce objectForKey:@"rows"];
            for(NSDictionary *dict in rows )
            {
                VehicleObject *obj = [[VehicleObject alloc]init];
                obj.vehicleName = [self checkNilString:[dict objectForKey:@"vehicleName"]];
                obj.vehicleNumber = [self checkNilString:[dict objectForKey:@"vehicleNumber"]];
                obj.vehicleType = [self checkNilString:[dict objectForKey:@"vehicleType"]];
                obj.latitude = [[self checkNilString:[dict objectForKey:@"lat"]] doubleValue];
                obj.longitude = [[self checkNilString:[dict objectForKey:@"lng"]] doubleValue];
                if (([dict objectForKey:@"cbcTranspose"]) != nil)
                {
                    obj.cbctranspose = [dict objectForKey:@"cbcTranspose"];
                }
                else
                {
                    obj.cbctranspose = false;
                }

                //details Array
                obj.deviceModel = [self checkNilString:[dict objectForKey:@"deviceModel"]];
                obj.deviceID = [self checkNilString:[dict objectForKey:@"deviceID"]];
                NSString *datestring = [self checkNilString:[dict objectForKey:@"lastEngineOn"]];
                
                if(![datestring isEqualToString:@"NA"] && ![datestring isEqualToString:@""])
                {
                    
                    NSString *actualDate = [[CustomControls sharedObj]addFiveHoursthirtymins:datestring];                    obj.engineLastOn = [self getDateString:actualDate];
                }
                else
                {
                    obj.engineLastOn = @"NA";
                }
                obj.totalEngineHours = [self checkNilString:[dict objectForKey:@"totalEngineHours"]];
                
                obj.vehicleLocation = [self checkNilString:[dict objectForKey:@"vehicleLocation"]];
                
                NSString *dateString = [self checkNilString:[dict objectForKey:@"lastDataReceivedAt"]];
                
                if(![dateString isEqualToString:@""] && ![dateString isEqualToString:@"NA"])
                {
                    NSString *actualDate = [[CustomControls sharedObj]addFiveHoursthirtymins:dateString];
                    
                    obj.lastDataReceivedAt = [self getDateString:actualDate];
                }
                else
                {
                    obj.lastDataReceivedAt = @"NA";
                }
                obj.lat = [self checkNilString:[dict objectForKey:@"lat"]];
                obj.lng = [self checkNilString:[dict objectForKey:@"lng"]];
                obj.pinno = [self checkNilString:[dict objectForKey:@"pinno"]];
                
                [vehicleListArray addObject:obj];
                
                obj = nil;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_listView reloadData];
            });
        }
    }];
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}


-(NSString*)checkNilString:(NSString*)val
{
    if (val != nil)
    {
        return val;
    }
    else
    {
        return @"NA";
    }
}

-(void)setslideViewController
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController setDelegate:self];
    [revealController setRightViewController:nil];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (vehicleListArray.count > 0)
    {
        tableView.backgroundView = nil;
        return vehicleListArray.count;
    }
    else
    {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:tableView.frame];
        messageLabel.text = @"Vechicles are not available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        tableView.backgroundView = messageLabel;
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    HomeCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[HomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    VehicleObject *obj = [vehicleListArray objectAtIndex:indexPath.row];
    cell.vehicleNumberlable.text = [obj.vehicleNumber uppercaseString];
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabViewController *tabViewControllerObj = (TabViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"TabViewController"];
    tabViewControllerObj.vehicleObject = [vehicleListArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:tabViewControllerObj animated:YES];
}

@end
