//
//  AnalyticsViewController.h
//  MyNavi
//
//  Created by Vijay on 17/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalyticsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *analyticsGridView;

@end
