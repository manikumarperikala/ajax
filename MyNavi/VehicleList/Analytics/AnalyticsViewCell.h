//
//  AnalyticsViewCell.h
//  MyNavi
//
//  Created by Vijay on 17/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalyticsViewCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UILabel *date;
@property(nonatomic,strong) IBOutlet UILabel *fuel;
@property(nonatomic,strong) IBOutlet UILabel *engine;
@property(nonatomic,strong) IBOutlet UILabel *kms;

@end
