//
//  AnalyticsViewController.m
//  MyNavi
//
//  Created by Vijay on 17/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "AnalyticsViewController.h"
#import "AnalyticsViewCell.h"
#import "AnalyticsObject.h"
#import "SVPullToRefresh.h"

#define Norecords -1

@interface AnalyticsViewController ()
{
    AppDelegate *delegate;
    NSMutableArray *analyticsList;
    int pageNumber;
}
@end

@implementation AnalyticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    
    [self getAnalyticsData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
    
    analyticsList = [[NSMutableArray alloc]init];
    
    __weak AnalyticsViewController *weakSelf = self;

    [_analyticsGridView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];

}
-(void)searchAction
{
//    if([TabBarSingleTon.sharedObj compareDates]){
    pageNumber = 1;
    [analyticsList removeAllObjects];

    [self getAnalyticsData];
/*}
else
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
}*/

}

- (void)insertRowAtBottom {
    if(pageNumber != Norecords)
    {
        pageNumber+=1;
        [self getAnalyticsData];
    }
    else
        [_analyticsGridView.infiniteScrollingView stopAnimating];
    
}


-(void)getAnalyticsData
{
    NSString *analyticsReport=[NSString stringWithFormat:Analytics,[TabBarSingleTon sharedObj].endDate,pageNumber,[TabBarSingleTon sharedObj].startDate,delegate.vehicleObject.deviceID];
    
    if(!analyticsList.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:analyticsReport httpType:kGet  WithBlock:^(id responce, NSError *error) {
        
        if(!error)
        {
            if([responce[@"rows"] count] > 0)
            {
                for (NSDictionary *dict in responce[@"rows"]) {
                    
                    AnalyticsObject *obj = [[AnalyticsObject alloc]init];
                    
                    obj.date = dict[@"dates"];
                    obj.kmsDriven = [NSString stringWithFormat:@"%@",dict[@"distance"]];
                    obj.engineHours = dict[@"engineHours"];
                    obj.fuelConsumption = [NSString stringWithFormat:@"%@",dict[@"fuelConsumption"]];
                    
                    [analyticsList addObject:obj];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_analyticsGridView reloadData];
                    _analyticsGridView.backgroundView = nil;
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UILabel *label = [[UILabel alloc]initWithFrame:_analyticsGridView.frame];
                    label.text = @"No Records Found";
                    label.textAlignment = NSTextAlignmentCenter;
                    _analyticsGridView.backgroundView = label;
                    [_analyticsGridView reloadData];
                    
                });
            }
        }
    }];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return analyticsList.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AnalyticsViewCell *cell = (AnalyticsViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    AnalyticsObject *analyticsObject = analyticsList[indexPath.row];
    
    cell.date.text = analyticsObject.date;
    cell.fuel.text = analyticsObject.fuelConsumption;
    cell.engine.text = analyticsObject.engineHours;
    cell.kms.text = analyticsObject.kmsDriven;
    
    return cell;
}
@end
