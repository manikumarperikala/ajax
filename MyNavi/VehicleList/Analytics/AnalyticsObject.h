//
//  AnalyticsObject.h
//  MyNavi
//
//  Created by Vijay on 17/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalyticsObject : NSObject

@property(nonatomic,strong)NSString *date;
@property(nonatomic,strong)NSString *fuelConsumption;
@property(nonatomic,strong)NSString *engineHours;
@property(nonatomic,strong)NSString *kmsDriven;

@end
