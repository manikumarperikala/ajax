//
//  IncidentObject.h
//  eRakshaPolice
//
//  Created by Sridhar Reddy on 6/30/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VehicleObject : NSObject

@property(nonatomic,strong)NSString *vehicleName;
@property(nonatomic,strong)NSString *vehicleNumber;
@property(nonatomic,strong)NSString *vehicleType;
@property(assign)double latitude;
@property(assign)double longitude;
@property(nonatomic,strong)NSString *deviceModel;
@property(nonatomic,strong)NSString *deviceID;
@property(nonatomic,strong)NSString *engineLastOn;
@property(nonatomic,strong)NSString *totalEngineHours;
@property(nonatomic,strong)NSString *lastDataReceivedAt;
@property(nonatomic,strong)NSString *vehicleLocation;
@property (nonatomic, assign)BOOL cbctranspose;
@property(nonatomic,strong)NSString *lat;
@property(nonatomic,strong)NSString *lng;
@property(nonatomic,strong)NSString *pinno;

@end
