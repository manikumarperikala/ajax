//
//  AppDelegate.h
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VehicleObject.h"
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
{
    UIView *backgroundView;
    UIView *btnBackgroundView;
    NSString *deviceTokenString;
    UIActivityIndicatorView *activityView;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VehicleObject *vehicleObject;


-(void)showActivityIndicatorOnFullScreen;
-(void)hideActivityIndcicator;
-(void)popToViewController:(NSString *)identifier;

@end

