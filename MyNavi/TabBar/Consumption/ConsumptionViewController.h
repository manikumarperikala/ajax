//
//  ConsumptionViewController.h
//  MyNavi
//
//  Created by sridhar on 20/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsumptionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *consumptionGridView;

@end
