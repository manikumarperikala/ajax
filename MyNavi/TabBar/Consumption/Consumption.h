//
//  Consumption.h
//  MyNavi
//
//  Created by sridhar on 21/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Consumption : NSObject

@property(nonatomic,strong)NSString *totalRecords;
@property(nonatomic,strong)NSDictionary *rows;

@end
