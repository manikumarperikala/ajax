//
//  ConsumptionViewController.m
//  MyNavi
//
//  Created by sridhar on 20/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ConsumptionViewController.h"
#import "ConsumptionCell.h"
#import "Consumption.h"

@interface ConsumptionViewController ()
{
    AppDelegate *delegate;
    NSDictionary *consumptionList;
    int pageNumber;

}
@end

@implementation ConsumptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    [self getConsumptionData];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
}

-(void)searchAction
{
    if([TabBarSingleTon.sharedObj compareDates]){
        pageNumber = 1;
        [self getConsumptionData];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}

-(void)getConsumptionData
{
    NSString *consumptionReport=[NSString stringWithFormat:ConsumptionReport,[TabBarSingleTon sharedObj].endDate,pageNumber,[TabBarSingleTon sharedObj].startDate,delegate.vehicleObject.deviceID];
    
    if(!consumptionList.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:consumptionReport httpType:kGet  WithBlock:^(id responce, NSError *error) {          
        
        if(!error)
        {
            if([(NSDictionary*)responce allKeys].count > 2)
            {
                consumptionList = responce;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_consumptionGridView reloadData];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UILabel *label = [[UILabel alloc]initWithFrame:_consumptionGridView.frame];
                    label.text = @"No Records Found";
                    label.textAlignment = NSTextAlignmentCenter;
                    _consumptionGridView.backgroundView = label;
                    
                });
            }
        }
    }];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 12;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ConsumptionCell *cell = (ConsumptionCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSArray *dictarr = consumptionList[@"rows"];
    NSDictionary *rowdict = dictarr[0];
    switch (indexPath.row) {
        case 0:
            cell.keyLabel.text = @"No of Batches";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",consumptionList[@"totalRecords"]];
            break;
        case 1:
            cell.keyLabel.text = @"AGGT10mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"aggt10mm"]];
            break;
        case 2:
            cell.keyLabel.text = @"AGGT20mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"aggt20mm"]];
            break;
        case 3:
            cell.keyLabel.text = @"AGGT30mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"aggt30mm"]];
            break;
        case 4:
            cell.keyLabel.text = @"SAND01mm";
            if (delegate.vehicleObject.cbctranspose)
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"cement01mm"]];
            }
            else
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"sand01mm"]];
            }
            break;
        case 5:
            cell.keyLabel.text = @"SAND02mm";

            if (delegate.vehicleObject.cbctranspose)
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"cement02mm"]];
            }
            else
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"sand02mm"]];
            }
            break;
        case 6:
            cell.keyLabel.text = @"CEMT01";
            if (delegate.vehicleObject.cbctranspose)
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"sand01mm"]];
            }
            else
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"cement01mm"]];
            }
            break;
        case 7:
            cell.keyLabel.text = @"CEMT02";
            if (delegate.vehicleObject.cbctranspose)
            {
                cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"sand02mm"]];
            }
            else
            {
               cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"cement02mm"]];
            }
            break;
        case 8:
            cell.keyLabel.text = @"WATER";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"water"]];
            
            break;
        case 9:
            cell.keyLabel.text = @"ADDITIVE";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"additive"]];
            
            break;
        case 10:
            cell.keyLabel.text = @"Total WT";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"totalWT"]];
            break;
        case 11:
            cell.keyLabel.text = @"CuM";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",[rowdict objectForKey:@"cum"]];
            break;
            
        default:
            break;
    }
    if([cell.valueLabel.text isEqualToString:@"(null)"])
    {
        cell.valueLabel.text = @"NA";
    }

    return cell;
}


@end
