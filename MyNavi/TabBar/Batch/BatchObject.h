//
//  BatchObject.h
//  MyNavi
//
//  Created by sridhar on 20/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BatchObject : NSObject

@property(nonatomic,strong)NSString *batchNo;
@property(nonatomic,strong)NSString *matchID;
@property(nonatomic,strong)NSString *dateAndTime;
@property(nonatomic,strong)NSString *batchTime;
@property(nonatomic,strong)NSString *totalWeight;
@property(nonatomic,strong)NSString *cum;
@property(nonatomic,strong)NSString *AGGT10mm;
@property(nonatomic,strong)NSString *AGGT20mm;
@property(nonatomic,strong)NSString *AGGT30mm;
@property(nonatomic,strong)NSString *SAND01mm;
@property(nonatomic,strong)NSString *SAND02mm;
@property(nonatomic,strong)NSString *CEMT01;
@property(nonatomic,strong)NSString *CEMT02;
@property(nonatomic,strong)NSString *WATER;
@property(nonatomic,strong)NSString *ADDITIVE;

@end

