//
//  BatchViewController.m
//  MyNavi
//
//  Created by sridhar on 20/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "BatchViewController.h"
#import "SVPullToRefresh.h"
#import "BatchObject.h"
#import "BatchCell.h"
#import "BatchDetailViewController.h"
#define Norecords -1

@interface BatchViewController ()
{
    AppDelegate *delegate;
    NSMutableArray *batchArray;
    int pageNumber;

}
@end

@implementation BatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageNumber=1;
    batchArray=[[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self getBatchReports];
[self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    
    __weak BatchViewController *weakSelf = self;
    [_batchGridView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];


    // Do any additional setup after loading the view.
}
-(void)searchAction
{
//    if([TabBarSingleTon.sharedObj compareDates]){
    pageNumber=1;
    [batchArray removeAllObjects];
    [self getBatchReports];
/*}
else
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
    
}*/

}

- (void)insertRowAtBottom {
    if(pageNumber!=Norecords)
    {
        pageNumber+=1;
        [self getBatchReports];
    }
    else
        [_batchGridView.infiniteScrollingView stopAnimating];
    
}
-(void)getBatchReports
{
   
    NSString *batchReport=[NSString stringWithFormat:BatchReport,[TabBarSingleTon sharedObj].endDate,pageNumber,[TabBarSingleTon sharedObj].startDate,delegate.vehicleObject.deviceID];
    
    if(!batchArray.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:batchReport httpType:kGet  WithBlock:^(id responce, NSError *error) {
        
        if(!error)
        {
            NSArray *rows=[responce objectForKey:@"rows"];
            if(rows.count)
            {
                for(NSDictionary *dict in rows )
                {
                    BatchObject *obj = [[BatchObject alloc]init];
                    obj.batchNo = [dict objectForKey:@"batchNo"];
                    obj.matchID = [dict objectForKey:@"machineID"];
                    obj.dateAndTime = [dict objectForKey:@"batchDate"];
                    obj.batchTime = [dict objectForKey:@"batchTime"];
                    obj.totalWeight = [dict objectForKey:@"totalWT"];
                    obj.cum = [dict objectForKey:@"cum"];
                    obj.AGGT10mm = [dict objectForKey:@"aggt10mm"];
                    obj.AGGT20mm = [dict objectForKey:@"aggt20mm"];
                    obj.AGGT30mm = [dict objectForKey:@"aggt30mm"];
                    obj.ADDITIVE = [dict objectForKey:@"additive"];
                    obj.WATER = [dict objectForKey:@"water"];
                    if (delegate.vehicleObject.cbctranspose)
                    {
                        obj.CEMT02 = [dict objectForKey:@"sand02mm"];
                        obj.CEMT01 = [dict objectForKey:@"sand01mm"];
                        obj.SAND01mm = [dict objectForKey:@"cemt01"];
                        obj.SAND02mm = [dict objectForKey:@"cemt02"];
                        
                    }
                    else
                    {
                        obj.CEMT02 = [dict objectForKey:@"cemt02"];
                        obj.CEMT01 = [dict objectForKey:@"cemt01"];
                        obj.SAND01mm = [dict objectForKey:@"sand01mm"];
                        obj.SAND02mm = [dict objectForKey:@"sand02mm"];
                    }
                    [batchArray addObject:obj];
                    obj = nil;
                }
            }
            else
            {
                pageNumber=Norecords;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_batchGridView.infiniteScrollingView stopAnimating];
                [_batchGridView reloadData];
            });
        }
    }];

}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0, 0,0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = (IDIOM == IPAD)?80:60;
    
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/5,height );
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    // Return the number of sections.
    if (batchArray.count) {
        collectionView.backgroundView = nil;
        return batchArray.count;
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:collectionView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        collectionView.backgroundView = messageLabel;
        
    }
    
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionview cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BatchCell *cell =(BatchCell *) [collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    BatchObject *obj =[batchArray objectAtIndex:indexPath.section];
    
    if(indexPath.row%5 == 0)
        cell.dateLable.text = obj.batchNo;
    else if(indexPath.row%5 == 1)
        cell.dateLable.text = obj.matchID;
    else if(indexPath.row%5 == 2)
        cell.dateLable.text = [NSString stringWithFormat:@"%@\n%@",[self getDateString:obj.dateAndTime],obj.batchTime];
    else if(indexPath.row%5 == 3)
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.totalWeight];
    else if(indexPath.row%5 == 4)
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.cum];

    [cell.dateLable setTextColor:[UIColor blackColor]];
    
    if([cell.dateLable.text isEqualToString:@"(null)"])
        cell.dateLable.text = @"0";
    
    return cell;
}
-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPat
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BatchDetailViewController *detailView = [storyBoard instantiateViewControllerWithIdentifier:@"Detail"];
    detailView.batchObj = [batchArray objectAtIndex:indexPat.section];
    UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:detailView];
    [self.navigationController presentViewController:navi animated:YES completion:nil];
}
@end
