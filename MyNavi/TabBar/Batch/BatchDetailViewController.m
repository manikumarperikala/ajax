//
//  BatchDetailViewController.m
//  MyNavi
//
//  Created by sridhar on 21/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "BatchDetailViewController.h"
#import "ConsumptionCell.h"
#import "BatchObject.h"

@interface BatchDetailViewController ()


@end

@implementation BatchDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Batch Detailed Report";
    UIBarButtonItem *rightbarBtn=[[CustomControls sharedObj]rightBarButtonMethod:nil withTitle:@"Close" selector:@selector(popAction) delegate:self];
    rightbarBtn.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightbarBtn;

}

-(void)popAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableView DataSources & Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 14;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ConsumptionCell *cell = (ConsumptionCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.keyLabel.text = @"Batch No";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.batchNo ];
            break;
        case 1:
            cell.keyLabel.text = @"Mach ID";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.matchID];
            break;
        case 2:
            cell.keyLabel.text = @"Date";
            cell.valueLabel.text = [self getDateString:_batchObj.dateAndTime];
            break;
        case 3:
            cell.keyLabel.text = @"AGGT10mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.AGGT10mm];
            break;
        case 4:
            cell.keyLabel.text = @"AGGT20mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.AGGT20mm];
            break;
        case 5:
            cell.keyLabel.text = @"AGGT30mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.AGGT30mm];
            break;
        case 6:
            cell.keyLabel.text = @"SAND01mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.SAND01mm];
            break;
        case 7:
            cell.keyLabel.text = @"SAND02mm";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.SAND02mm];
            break;
        case 8:
            cell.keyLabel.text = @"CEMT01";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.CEMT01];
            break;
        case 9:
            cell.keyLabel.text = @"CEMT02";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.CEMT02 ];
            break;
        case 10:
            cell.keyLabel.text = @"WATER";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.WATER];
            break;
        case 11:
            cell.keyLabel.text = @"ADDITIVE";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.ADDITIVE];
            break;
        case 12:
            cell.keyLabel.text = @"Total WT";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.totalWeight];
            break;
        case 13:
            cell.keyLabel.text = @"CuM";
            cell.valueLabel.text = [NSString stringWithFormat:@"%@",_batchObj.cum];
            break;
            
        default:
            break;
    }

    if([cell.valueLabel.text isEqualToString:@"(null)"])
    {
        cell.valueLabel.text = @"0";
    }
    return  cell;
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date =[dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd/MM/yyyy\nHH:mm:ss a"];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

@end
