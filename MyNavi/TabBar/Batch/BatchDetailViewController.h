//
//  BatchDetailViewController.h
//  MyNavi
//
//  Created by sridhar on 21/06/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BatchObject.h"

@interface BatchDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *detailListView;
@property(nonatomic,strong)BatchObject *batchObj;


@end
