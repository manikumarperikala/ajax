//
//  TabViewController.m
//  eRakshaPolice
//
//  Created by SridharReddy on 04/08/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import "TabViewController.h"
#import "DatesView.h"
#import "DatePicker.h"


@interface TabViewController ()<UITabBarDelegate,UITabBarControllerDelegate,DatePickerProtocal,UINavigationControllerDelegate>
{
    AppDelegate *delegate;
    DatePicker *datePickerView;
}
@end

@implementation TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItems = [[CustomControls sharedObj] leftBarButtonMethod:@"arrow_left" selector:@selector(popAction:) delegate:self];
    
    self.tabBar.barTintColor = [UIColor whiteColor];
 
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11], NSFontAttributeName,  [UIColor grayColor], NSForegroundColorAttributeName,nil]
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:11], NSFontAttributeName,  [UIColor redColor], NSForegroundColorAttributeName,nil]
                                             forState:UIControlStateSelected];
 
    [self setDelegate:self];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.vehicleObject = _vehicleObject;
    // Date View
    
    datesView = [[[NSBundle mainBundle] loadNibNamed:@"DatesView" owner:self options:nil] objectAtIndex:0];
    datesView.frame = CGRectMake(0, 67, self.view.frame.size.width, 40);

    [self setDatesIntialvalue:datesView.fromDate];
    
    UITapGestureRecognizer *fromDateGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(datePickerAction:)];
    fromDateGesture.numberOfTapsRequired = 1;
    datesView.fromDate.tag = 10;
    [datesView.fromDate addGestureRecognizer:fromDateGesture];
    
    UITapGestureRecognizer *toDateGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(datePickerAction:)];
    toDateGesture.numberOfTapsRequired = 1;
    datesView.toDate.tag = 11;
    [datesView.toDate addGestureRecognizer:toDateGesture];
    
    [self setDatesIntialvalue:datesView.toDate];
    
    [datesView.searchButton addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.view.frame.size.width <= 320.0)
    {
        [(UILabel*)datesView.toDate.subviews[1] setFont:[UIFont systemFontOfSize:12.0f]];
        [(UILabel*)datesView.fromDate.subviews[1] setFont:[UIFont systemFontOfSize:12.0f]];
    }
    [datesView loadData];
    [self.view addSubview:datesView];
    [self hideDatesView];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:12], NSFontAttributeName,NavigationBarColor,NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    [self getOriginalImages];
    
    self.moreNavigationController.navigationBar.barTintColor = [UIColor orangeColor];
    self.moreNavigationController.view.tintColor = [UIColor colorWithRed:29.0/255.0 green:9.0/255.0 blue:91.0/255.0 alpha:1];
}



-(void)getOriginalImages
{
    UITabBarItem *detailsTabBarItem = [[self.tabBar items] objectAtIndex:0];
    detailsTabBarItem.selectedImage = [[UIImage imageNamed:@"list-outline_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    detailsTabBarItem.image = [detailsTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *mapsTabBarItem = [[self.tabBar items] objectAtIndex:1];
    mapsTabBarItem.selectedImage = [[UIImage imageNamed:@"location_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    mapsTabBarItem.image = [mapsTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *graphsTabBarItem = [[self.tabBar items] objectAtIndex:2];
    graphsTabBarItem.selectedImage = [[UIImage imageNamed:@"arrow-graph-up-right_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    graphsTabBarItem.image = [graphsTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
   UITabBarItem *summaryreportsTabBarItem = [[self.tabBar items] objectAtIndex:3];
    summaryreportsTabBarItem.selectedImage = [[UIImage imageNamed:@"document-text_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    summaryreportsTabBarItem.image = [summaryreportsTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *engineTabBarItem = [[self.tabBar items] objectAtIndex:4];
    engineTabBarItem.selectedImage = [[UIImage imageNamed:@"cog-outline_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    engineTabBarItem.image = [engineTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *batchTabBarItem = [[self.tabBar items] objectAtIndex:5];
    batchTabBarItem.selectedImage = [[UIImage imageNamed:@"document-text_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    batchTabBarItem.image = [batchTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *consumptionTabBarItem = [[self.tabBar items] objectAtIndex:6];
    consumptionTabBarItem.selectedImage = [[UIImage imageNamed:@"document-text_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    consumptionTabBarItem.image = [consumptionTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *analyticsTabBarItem = [[self.tabBar items] objectAtIndex:7];
    analyticsTabBarItem.selectedImage = [[UIImage imageNamed:@"arrow-graph-up-right_sel"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    analyticsTabBarItem.image = [analyticsTabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    

}

-(void)searchAction:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchAction" object:nil];
}

-(void )setDatesIntialvalue:(UIView *)sender
{
    NSDate *selectedDate = [NSDate date];
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:selectedDate];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    if([today day] == [otherDay day] &&
       [today month] == [otherDay month] &&
       [today year] == [otherDay year] &&
       [today era] == [otherDay era]) {
        if([sender isEqual:datesView.fromDate])
        {
            selectedDate = [self beginningOfDay:[NSDate date]];
        }
        else
            selectedDate = [NSDate date];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    NSString *dateString = [formatter stringFromDate:selectedDate];
    
    if([sender isEqual:datesView.fromDate])
        [TabBarSingleTon sharedObj].startDate = dateString;
    else
        [TabBarSingleTon sharedObj].endDate = dateString;
    
    [formatter setDateFormat:@"MMM dd, yyyy"];
    dateString = [formatter stringFromDate:selectedDate];
    [(UILabel*)datesView.toDate.subviews[1] setText:dateString];
    [(UILabel*)datesView.fromDate.subviews[1] setText:dateString];
}

-(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay) fromDate:date];
    
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
    return [cal dateFromComponents:components];
}

- (void)datePickerAction:(UITapGestureRecognizer*)sender {
    
    if(![datePickerView superview])
    {
        datePickerView = [[DatePicker alloc]initWithFrame:CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height - 110)];
        datePickerView.selectedButton = sender.view;
        datePickerView.delegate = self;
        [datePickerView setPickerWithTag:(int)sender.view.tag];
        [self.view addSubview:datePickerView];
    }
}

-(void)datePickerSelectedDate:(NSDate *)sender forTag:(int)tag
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSString *dateString = [formatter stringFromDate:sender];
    [(UILabel*)datePickerView.selectedButton.subviews[1] setText:dateString];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    dateString = [formatter stringFromDate:sender];
    
    if([datePickerView.selectedButton isEqual:datesView.fromDate])
        [TabBarSingleTon sharedObj].startDate = dateString;
    else
        [TabBarSingleTon sharedObj].endDate = dateString;
    
    datePickerView = nil;
}

- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    
    if((indexOfTab == 0) || (indexOfTab == 3))
    {
        [self hideDatesView];
        
    }
    else if ([viewController isKindOfClass:self.moreNavigationController.class])
    {
        self.title = @"More";
        
        if (viewController == self.moreNavigationController)
        {
            self.moreNavigationController.delegate = self;
        }
    }
    else
        [self showDatesView];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if([viewController.title isEqualToString:@"More"])
        [self hideDatesView];
    else
        [self showDatesView];
}

-(void)hideDatesView
{
    datesView.hidden = YES;
}

-(void)showDatesView
{
    datesView.hidden = NO;
}

- (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContext(size);
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    [image drawInRect:rect];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)popAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
