//
//  EngineHoursVC.h
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EngineHoursVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *engineHoursGridView;

@end
