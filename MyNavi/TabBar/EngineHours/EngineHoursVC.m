//
//  EngineHoursVC.m
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "EngineHoursVC.h"
#import "EngineHoursCell.h"
#import "EngineHoursObject.h"
#import "SVPullToRefresh.h"

#define Norecords -1

@interface EngineHoursVC ()
{
    AppDelegate *delegate;
    NSMutableArray *engineHoursArray;
    int pageNumber;
}

@end

@implementation EngineHoursVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageNumber=1;
    __weak EngineHoursVC *weakSelf = self;
    [_engineHoursGridView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];

    engineHoursArray = [[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
    [self getEngineHours];
}

-(void)searchAction
{
//    if([TabBarSingleTon.sharedObj compareDates]){
        pageNumber = 1;
        [engineHoursArray removeAllObjects];
        [self getEngineHours];
 /*   }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }*/

}

- (void)insertRowAtBottom {
    if(pageNumber != Norecords)
    {
        pageNumber += 1;
        [self getEngineHours];
    }
    else
        [_engineHoursGridView.infiniteScrollingView stopAnimating];
    
    
}

-(void)getEngineHours
{
    NSString *engineHours = [NSString stringWithFormat:EngineHours,delegate.vehicleObject.deviceID,[TabBarSingleTon sharedObj].endDate,pageNumber,[TabBarSingleTon sharedObj].startDate,delegate.vehicleObject.vehicleNumber];
        if(!engineHoursArray.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
        [HttpManager apiDetails:nil serviceName:engineHours httpType:kGet  WithBlock:^(id responce, NSError *error) {
            if(!error)
            {
                NSArray *rows = [responce objectForKey:@"rows"];
                if(rows.count)
                {
                    for(NSDictionary *dict in rows )
                    {
                        EngineHoursObject *obj = [[EngineHoursObject alloc]init];
                        obj.startDate = [[CustomControls sharedObj] addFiveHoursthirtymins:[dict objectForKey:@"onTimestamp"]];
                        obj.endDate =  [[CustomControls sharedObj] addFiveHoursthirtymins:[dict objectForKey:@"offTimestamp"]];
                        obj.hours = [dict objectForKey:@"engineHours"];
                        [engineHoursArray addObject:obj];
                        obj = nil;
                    }
                }
                else
                {
                    pageNumber = Norecords;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_engineHoursGridView.infiniteScrollingView stopAnimating];
                    [_engineHoursGridView reloadData];
                });
            }
        }];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5,5, 5,5);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = (IDIOM == IPAD)?64:44;
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/3-10,height );
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    if (engineHoursArray.count) {
        collectionView.backgroundView = nil;
        return engineHoursArray.count;
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:collectionView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        collectionView.backgroundView = messageLabel;
        
    }
    
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionview cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EngineHoursCell *cell = (EngineHoursCell *) [collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    EngineHoursObject *obj = [engineHoursArray objectAtIndex:(engineHoursArray.count-1)-(indexPath.section)];
    
    cell.dateLable.textAlignment = NSTextAlignmentCenter;
    
    if(indexPath.row%3 == 0)
        cell.dateLable.text = [self getDateString:obj.startDate];
    
    else if(indexPath.row%3 == 1)
        cell.dateLable.text = [self getDateString:obj.endDate];
    else
        cell.dateLable.text = obj.hours;

        return cell;
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss a"];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

@end
