//
//  EngineHoursObject.h
//  MyNavi
//
//  Created by sridhar on 03/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EngineHoursObject : NSObject

@property(nonatomic,strong)NSString *startDate;
@property(nonatomic,strong)NSString *endDate;
@property(nonatomic,strong)NSString *hours;

@end
