//
//  TabViewController.h
//  eRakshaPolice
//
//  Created by SridharReddy on 04/08/16.
//  Copyright © 2016 rocks & INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VehicleObject;
@class DatesView;

@interface TabViewController : UITabBarController
{
    DatesView *datesView;
}

@property(nonatomic,assign)int tag;
@property(nonatomic,strong)VehicleObject *vehicleObject;


@end
