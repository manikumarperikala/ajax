//
//  MapVC.h
//  MyNavi
//
//  Created by sridhar on 26/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GMSMapView;

@interface MapVC : UIViewController

@property(nonatomic,strong)GMSMapView *mapView_;

@end
