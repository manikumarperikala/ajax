//
//  MapHistoryViewController.m
//  MyNavi
//
//  Created by Vijay on 14/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "MapHistoryViewController.h"
#import "Constant.h"
#import "HeartBeatObject.h"

@interface MapHistoryViewController ()
{
    NSMutableArray *heartBeatArray;
    AppDelegate *delegate;
}

@end

@implementation MapHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Location History";
    self.view.backgroundColor = [UIColor lightGrayColor];
    heartBeatArray = [[NSMutableArray alloc]init];
    [self getHeartBeat];
    
}

-(void)drawPolyline{
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:delegate.vehicleObject.latitude
                                                            longitude:delegate.vehicleObject.longitude
                                                                 zoom:18];
    GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.delegate = self;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    GMSMutablePath *multiPath = [GMSMutablePath path];
    
    for(int i = 0;i<heartBeatArray.count;i++)
    {
        HeartBeatObject *obj = [heartBeatArray objectAtIndex:i];

        [multiPath addLatitude:obj.latitude longitude:obj.longitude];
    }
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:multiPath];
    polyline.strokeColor = [UIColor purpleColor];
    polyline.strokeWidth = 5.f;
    polyline.map = mapView;

        
        HeartBeatObject *obj = [heartBeatArray objectAtIndex:0];
        HeartBeatObject *obj1 = [heartBeatArray objectAtIndex:heartBeatArray.count-1];
        
        GMSMarker *sourceMarker = [[GMSMarker alloc] init];
        sourceMarker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
        CGFloat latitude = obj.latitude;
        CGFloat longitude = obj.longitude;
        sourceMarker.appearAnimation = kGMSMarkerAnimationPop;
        sourceMarker.position = CLLocationCoordinate2DMake(latitude,longitude);
        bounds = [bounds includingCoordinate:sourceMarker.position];
        sourceMarker.map = mapView;
    
        GMSMarker *destMarker = [[GMSMarker alloc] init];
        destMarker.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
        CGFloat latitude1 = obj1.latitude;
        CGFloat longitude1 = obj1.longitude;
        destMarker.appearAnimation = kGMSMarkerAnimationPop;
        destMarker.position = CLLocationCoordinate2DMake(latitude1,longitude1);
        bounds = [bounds includingCoordinate:destMarker.position];
        destMarker.map = mapView;
    
    self.view = mapView;
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    [mapView setSelectedMarker:marker];
    
    return  YES;
}



-(void)getHeartBeat
{
    [heartBeatArray removeAllObjects];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSString *heartBeat = [NSString stringWithFormat:LocationHistory,delegate.vehicleObject.deviceID,[TabBarSingleTon sharedObj].endDate,[TabBarSingleTon sharedObj].startDate];
    
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:heartBeat httpType:kGet  WithBlock:^(id responce, NSError *error) {
        
        if(!error)
        {
            NSArray *rows = [responce objectForKey:@"rows"];
            if(rows.count)
            {
                for(NSDictionary *dict in rows )
                {
                    HeartBeatObject *obj = [[HeartBeatObject alloc]init];
                    if([[dict objectForKey:@"lat"] length] != 0)
                        obj.latitude = [[dict objectForKey:@"lat"] doubleValue];
                    if([[dict objectForKey:@"lng"] length] != 0)
                        obj.longitude = [[dict objectForKey:@"lng"] doubleValue];
                    
                    [heartBeatArray addObject:obj];
                    obj = nil;
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self drawPolyline];
                });
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location" message:@"Vehicle Location History is not availble" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                    [self.navigationController popViewControllerAnimated:true];
                }];
                [alertController addAction:action];
                [self presentViewController:alertController animated:YES completion:nil];

            }
        }
    }];
}

@end
