//
//  MapHistoryViewController.h
//  MyNavi
//
//  Created by Vijay on 14/07/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@class GMSMapView;

@interface MapHistoryViewController : UIViewController<GMSMapViewDelegate>

@property(nonatomic,strong)GMSMapView *mapView_;

@end
