//
//  MapVC.m
//  MyNavi
//
//  Created by sridhar on 26/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "MapVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "LocaitonManager.h"
#import "MapHistoryViewController.h"

@interface MapVC ()<GMSMapViewDelegate>
{
    AppDelegate *delegate;
}
@end

@implementation MapVC
@synthesize mapView_;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

     [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
  //   [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
   if (delegate.vehicleObject.latitude != 0 && delegate.vehicleObject.longitude != 0)
   {
       [self addGoogleMapView];
       [self drawPins];
   }
   else
   {
       UILabel *messageLabel = [[UILabel alloc] initWithFrame:self.view.frame];
       messageLabel.text = @"Lat Long's are not avaible for this vehicle .";
       messageLabel.textAlignment = NSTextAlignmentCenter;
       self.view = messageLabel;
   }

}
-(void)drawPins
{
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
        GMSMarker *marker = [[GMSMarker alloc] init];
        CGFloat latitude = delegate.vehicleObject.latitude;
        CGFloat longitude = delegate.vehicleObject.longitude;
        marker.position = CLLocationCoordinate2DMake(latitude,longitude);
        bounds = [bounds includingCoordinate:marker.position];
        marker.map = self->mapView_;
}
-(void)addGoogleMapView
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:delegate.vehicleObject.latitude longitude:delegate.vehicleObject.longitude
                                                                 zoom:15.0];
    CGRect frame = self.view.frame;
    frame.origin.y = 109;
    frame.size.height = self.view.frame.size.height-156;
    
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    mapView_.settings.compassButton = YES;
    [self.view addSubview: mapView_];
}

-(BOOL) mapView:(GMSMapView *) mapView didTapMarker:(GMSMarker *)marker
{
    if(delegate.vehicleObject.latitude != 0 && delegate.vehicleObject.longitude != 0)
    {
        MapHistoryViewController *mapHistory = [[MapHistoryViewController alloc]initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:mapHistory animated:YES];
        return YES;
    }
    else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location" message:@"Location Undefined" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    return NO;
}



@end
