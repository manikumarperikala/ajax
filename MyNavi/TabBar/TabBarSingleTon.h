//
//  CustomControls.h
//  Kikr
//
//  Created by Vijay on 07/10/17.
//  Copyright (c) 2014 TrakitNow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface TabBarSingleTon : NSObject

+ (TabBarSingleTon *)sharedObj;

@property(nonatomic,strong)NSString *startDate;
@property(nonatomic,strong)NSString *endDate;
@property(nonatomic,strong)NSString *chartTypeString;
@property(nonatomic,strong)NSString *chartTypeFullString;

-(BOOL)compareDates;


@end
