//
//  CustomControls.m
//  Kikr
//
//  Created by Mobikasa on 11/17/14.
//  Copyright (c) 2014 mobikasa. All rights reserved.
//

#import "TabBarSingleTon.h"
@implementation TabBarSingleTon
+ (TabBarSingleTon *)sharedObj
{
    static TabBarSingleTon *sharedObj = nil;
    if(sharedObj == nil)
    {
        sharedObj = [[self alloc] init];
    }
    return sharedObj;
}

-(BOOL)compareDates
{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [dateformatter setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *fromDate = [dateformatter dateFromString:_startDate];
    NSDate *toDate = [dateformatter dateFromString:_endDate];

    if([fromDate compare:toDate] == NSOrderedSame || [fromDate compare:toDate] == NSOrderedAscending)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end

