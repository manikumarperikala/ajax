//
//  GraphsVC.m
//  MyNavi
//
//  Created by sridhar on 02/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "GraphsVC.h"
#import "UUChart.h"
#import "HeartBeatObject.h"
#import "CustomPickerView.h"
@interface GraphsVC ()<UUChartDataSource,PickerViewProtocal>
{
    UUChart *chartView;
    NSMutableArray *heartBeatArray;
    AppDelegate *delegate;
    CustomPickerView *customPickerViewObj;
    NSString *pickerViewSelectedString;
    NSString *filterKey;
    int selectedRow;
}
@end

@implementation GraphsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pickerViewSelectedString = @"Battery Voltage level";
    selectedRow = 0;
    filterKey = @"batteryLevel";
    
    [[TabBarSingleTon sharedObj]setChartTypeString:@"V"];
    [[TabBarSingleTon sharedObj]setChartTypeFullString:pickerViewSelectedString];

    heartBeatArray = [[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
  //  [self.tableView tableFooterView] = UIView;

    [self getHeartBeat];
}
-(void)searchAction
{
//    if([TabBarSingleTon.sharedObj compareDates]){
    [self getHeartBeat];
/*}
else
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
    
}*/

}
-(void)drawGraph
{
    chartView = [[UUChart alloc]initWithFrame:CGRectMake(0, 153, self.view.frame.size.width, self.view.frame.size.height-200)
                                   dataSource:self
                                        style:UUChartStyleLine];
    [chartView showInView:self.view];

}
-(void)getHeartBeat
{
    [heartBeatArray removeAllObjects];
    NSString *heartBeat = [NSString stringWithFormat:Heartbeat,delegate.vehicleObject.deviceID,[TabBarSingleTon sharedObj].endDate,[TabBarSingleTon sharedObj].startDate];
    
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:heartBeat httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            NSArray *rows = [responce objectForKey:@"rows"];
            if(rows.count)
            {
                for(NSDictionary *dict in rows )
                {
                    NSDictionary *extradict = [dict objectForKey:@"extras"];
                    HeartBeatObject *obj = [[HeartBeatObject alloc]init];
                    obj.time = [self dict:dict iskeyexists:@"createdAt"];
                    obj.fuelLevel = [self dict:extradict iskeyexists:@"fuelLevel"];
                    obj.rpm = [self dict:extradict iskeyexists:@"rpm"];
                    obj.opsStatus = [self dict:extradict iskeyexists:@"opsStatus"];
                    obj.coolantTemp = [self dict:extradict iskeyexists:@"coolantTemp"];
                    obj.batteryLevel = [self dict:extradict iskeyexists:@"batteryLevel"];
                    if (![[self dict:dict iskeyexists:@"devicePublishTime"]  isEqual: @"0.0"])
                    {
                        obj.devicePublishTime =   [[CustomControls sharedObj] addFiveHoursthirtymins:[dict objectForKey:@"devicePublishTime"]];
                    }
                    else
                    {
                        obj.devicePublishTime = @"NA";
                    }
                    [heartBeatArray addObject:obj];
                    obj = nil;
                }
                heartBeatArray = [[[heartBeatArray reverseObjectEnumerator] allObjects] mutableCopy];

                dispatch_async(dispatch_get_main_queue(), ^{
                    if([chartView superview])
                        [chartView removeFromSuperview];
                        [self drawGraph];
                    [self.tableView reloadData];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UILabel *label = [[UILabel alloc]initWithFrame:self.view.frame];
                    label.text = @"No Records Found in Date limit";
                    label.textAlignment = NSTextAlignmentCenter;
                    self.view = label;
                });
            }
        }
    }];
}

- (NSArray *)chartConfigAxisXLabel:(UUChart *)chart
{
    return [self fliterWithKey:@"devicePublishTime"];
}
- (NSArray *)chartConfigAxisYValue:(UUChart *)chart
{
    NSArray *ary = [self fliterWithKey:filterKey];
    
    return @[ary];
}

-(NSArray *)fliterWithKey:(NSString *)sender{
    NSArray *matches = [heartBeatArray valueForKey:sender];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:matches];
  
    if([sender isEqualToString:@"devicePublishTime"])
    tempArray = [self convertStringToDateFormat:tempArray];
    
    return tempArray;
}

-(NSMutableArray *)convertStringToDateFormat:(NSMutableArray *)sender
{
    for(int i = 0;i<sender.count;i++)
    {
        NSString *str = [sender objectAtIndex:i];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        NSDate *date = [dateFormatter dateFromString:str];
        [dateFormatter setDateFormat:@"yyyy-MM-dd  HH:mm:ss"];
        NSString *convertedDateString = [dateFormatter stringFromDate:date];
        [sender replaceObjectAtIndex:i withObject:convertedDateString];
    }
    return sender;
}
- (BOOL)chart:(UUChart *)chart showHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}
- (IBAction)graphTypeAction:(id)sender {
    NSArray *array = @[@"Battery Voltage level",@"Fuel level",@"RPM",@"Coolant temparature",@"Oil Pressure"];
    if(![customPickerViewObj superview])
    {
        customPickerViewObj = [[CustomPickerView alloc]initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, self.view.frame.size.height-150)];
        customPickerViewObj.dataArray = array;
        customPickerViewObj.delegate = self;
        [customPickerViewObj setSelectedValue:(int)selectedRow];
        [self.view addSubview:customPickerViewObj];
    }
    
}
-(void)pickerViewSelectedValue:(NSString *)sender forRow:(int)row{
    
    if(sender.length)
    {
        pickerViewSelectedString = sender;
        selectedRow = row;
        
        [[TabBarSingleTon sharedObj]setChartTypeFullString:pickerViewSelectedString];

    [_graphTypeButton setTitle:sender forState:UIControlStateNormal];
    
    if([pickerViewSelectedString isEqualToString:@"Battery Voltage level"])
    {
        filterKey = @"batteryLevel";
        [[TabBarSingleTon sharedObj]setChartTypeString:@"V"];
    }
    else if([pickerViewSelectedString isEqualToString:@"Fuel level"])
    {
        filterKey = @"fuelLevel";
        [[TabBarSingleTon sharedObj]setChartTypeString:@""];
    }
    else if([pickerViewSelectedString isEqualToString:@"RPM"])
    {
        filterKey = @"rpm";
        [[TabBarSingleTon sharedObj]setChartTypeString:@"RPM"];

    } else if([pickerViewSelectedString isEqualToString:@"Coolant temparature"])
    {
        filterKey = @"coolantTemp";
        [[TabBarSingleTon sharedObj]setChartTypeString:@"C"];
    }
    else if([pickerViewSelectedString isEqualToString:@"Oil Pressure"])
    {
        filterKey = @"opsStatus";
        [[TabBarSingleTon sharedObj]setChartTypeString:@"bar"];
    }
    if([chartView superview])
        [chartView removeFromSuperview];
        [self drawGraph];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (heartBeatArray.count > 0)
    {
       return  1;
    }
    else
    {
       return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UIView *view = [(UIView*)cell viewWithTag:10];
    chartView = [[UUChart alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)
                                   dataSource:self
                                        style:UUChartStyleLine];
    [chartView showInView: view];
    return  cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  (self.view.frame.size.height - 150);
}

-(NSString *)dict:(NSDictionary *)dict iskeyexists:(NSString *)key
{
    if (([dict objectForKey:key]) != nil)
    {
        NSString *value = [dict objectForKey:key];
        if ((![value  isEqual: @""]) && (![value  isEqual: @"(null)"]))
        {
            return value;
        }
        else
        {
            return @"0.0";
        }
    }
    return @"0.0";
}
@end
