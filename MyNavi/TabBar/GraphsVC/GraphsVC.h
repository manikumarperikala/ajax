//
//  GraphsVC.h
//  MyNavi
//
//  Created by sridhar on 02/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphsVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)graphTypeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *graphTypeButton;

@end
