//
//  UULineChart.m
//  UUChartDemo
//
//  Created by shake on 14-7-24.
//  Copyright (c) 2014年 uyiuyao. All rights reserved.
//

#import "UULineChart.h"
#import "UUChartConst.h"
#import "UUChartLabel.h"
#import "CMPopTipView.h"
#import "CustomChartView.h"

#define kStrokeColor  [UUColor colorWithRed:13.0/255.0 green:83.0/255.0 blue:0 alpha:1]

@implementation UULineChart {
    NSHashTable *_chartLabelsForX;
        CMPopTipView *customView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}

-(void)setYValues:(NSArray *)yValues
{
    _yValues = yValues;
    [self setYLabels:yValues];
}

- (void)setYLabels:(NSArray *)yLabels
{
    
    int heightRpm = 0;
    if([[TabBarSingleTon sharedObj].chartTypeString isEqualToString:@"RPM"])
        heightRpm = 30;
    
    NSInteger max = 0;
    NSInteger min = 1000000000;

    for (NSArray * ary in yLabels) {
        for (NSString *valueString in ary) {
            
            NSInteger value;
            if(![[NSString stringWithFormat:@"%@",valueString] isEqualToString:@"<null>"])
                value = [valueString integerValue];
            else
                value = 0;
                if (value > max) {
                    max = value;
                }
                if (value < min) {
                    min = value;
                }
        }
    }
    max = max<5 ? 5:max;
    _yValueMin = 0;
    _yValueMax = (int)max+10;
    
    if (_chooseRange.max != _chooseRange.min) {
        _yValueMax = _chooseRange.max;
        _yValueMin = _chooseRange.min;
    }

    float level = (_yValueMax-_yValueMin) /4.0;
    CGFloat chartCavanHeight = self.frame.size.height-100- UULabelHeight*3;
    CGFloat levelHeight = chartCavanHeight /4.0;

    for (int i=0; i<5; i++) {
       
        UUChartLabel * label = [[UUChartLabel alloc] initWithFrame:CGRectMake(0.0,chartCavanHeight-i*levelHeight+5, UUYLabelwidth+heightRpm, UULabelHeight)];
		label.text = [NSString stringWithFormat:@"%d %@",(int)(level * i+_yValueMin),[TabBarSingleTon sharedObj].chartTypeString];
		[self addSubview:label];
    }
    if ([super respondsToSelector:@selector(setMarkRange:)]) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(UUYLabelwidth+heightRpm, (1-(_markRange.max-_yValueMin)/(_yValueMax-_yValueMin))*chartCavanHeight+UULabelHeight, self.frame.size.width-UUYLabelwidth-heightRpm, (_markRange.max-_markRange.min)/(_yValueMax-_yValueMin)*chartCavanHeight)];
        view.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.1];
        [self addSubview:view];
    }

    //画横线
    for (int i=0; i<5; i++) {
        if ([_showHorizonLine[i] integerValue]>0) {
            
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:CGPointMake(UUYLabelwidth+heightRpm,UULabelHeight+i*levelHeight)];
            [path addLineToPoint:CGPointMake(self.frame.size.width,UULabelHeight+i*levelHeight)];
            [path closePath];
            shapeLayer.path = path.CGPath;
            shapeLayer.strokeColor = [[[UIColor blackColor] colorWithAlphaComponent:0.1] CGColor];
            shapeLayer.fillColor = [[UIColor whiteColor] CGColor];
            shapeLayer.lineWidth = 1;
            [self.layer addSublayer:shapeLayer];
        }
    }
}

- (void)setXLabels:(NSArray *)xLabels
{
    if( !_chartLabelsForX ){
        _chartLabelsForX = [NSHashTable weakObjectsHashTable];
    }
    
    _xLabels = xLabels;
    CGFloat num = 0;
    if (xLabels.count >= 20) {
        num = 20.0;
    }else if (xLabels.count <= 1){
        num = 1.0;
    }else{
        num = xLabels.count;
    }
    
    int heightRpm = 0;
    if([[TabBarSingleTon sharedObj].chartTypeString isEqualToString:@"RPM"])
        heightRpm = 30;
    
    _xLabelWidth = (self.frame.size.width - UUYLabelwidth-heightRpm)/num;
    
    for (int i=0; i<xLabels.count; i++) {
      
        NSString *labelText = xLabels[i];
        UUChartLabel * label = [[UUChartLabel alloc] initWithFrame:CGRectMake(i * _xLabelWidth-10+heightRpm, self.frame.size.height - 60, 110, UULabelHeight)];
        label.transform = CGAffineTransformMakeRotation(30);//270º
        label.text = labelText;
        label.textColor = [UIColor  grayColor];
        [self addSubview:label];
        [_chartLabelsForX addObject:label];
    }
    
  /*  for (int i=0; i<xLabels.count; i++) {
        UILabel *lbl =[[UILabel alloc]initWithFrame:CGRectMake(i * _xLabelWidth+52+heightRpm, self.frame.size.width-73, 1, 10)];
        lbl.backgroundColor=[UIColor grayColor];
        [self addSubview:lbl];
    }*/
  
}

- (void)setColors:(NSArray *)colors
{
	_colors = colors;
}

- (void)setMarkRange:(CGRange)markRange
{
    _markRange = markRange;
}

- (void)setChooseRange:(CGRange)chooseRange
{
    _chooseRange = chooseRange;
}

- (void)setshowHorizonLine:(NSMutableArray *)showHorizonLine
{
    _showHorizonLine = showHorizonLine;
}


- (void)strokeChart
{
    for (int i=0; i<_yValues.count; i++) {
        NSArray *childAry = _yValues[i];
        if (childAry.count==0) {
            return;
        }
        //获取最大最小位置
        CGFloat max = [childAry[0] floatValue];
        CGFloat min = [childAry[0] floatValue];
        NSInteger max_i = 0;
        NSInteger min_i = 0;
        
        for (int j=0; j<childAry.count; j++){
            CGFloat num = [childAry[j] floatValue];
            if (max<=num){
                max = num;
                max_i = j;
            }
            if (min>=num){
                min = num;
                min_i = j;
            }
        }
        
        //划线
        CAShapeLayer *_chartLine = [CAShapeLayer layer];
        _chartLine.lineCap = kCALineCapRound;
        _chartLine.lineJoin = kCALineJoinBevel;
        _chartLine.fillColor   = [[UIColor whiteColor] CGColor];
        _chartLine.lineWidth   = 2.0;
        _chartLine.strokeEnd   = 0.0;
        [self.layer addSublayer:_chartLine];
        
        UIBezierPath *progressline = [UIBezierPath bezierPath];
        CGFloat firstValue = [[childAry objectAtIndex:0] floatValue];
        int heightRpm = 0;
        if([[TabBarSingleTon sharedObj].chartTypeString isEqualToString:@"RPM"])
            heightRpm = 30;
        
        CGFloat xPosition = (UUYLabelwidth+heightRpm + _xLabelWidth/2.0);
        CGFloat chartCavanHeight = self.frame.size.height-100 - UULabelHeight*3;
        
        float grade = ((float)firstValue-_yValueMin) / ((float)_yValueMax-_yValueMin);
       
        //第一个点
        BOOL isShowMaxAndMinPoint = YES;
        if (self.showMaxMinArray) {
            if ([self.showMaxMinArray[i] intValue]>0) {
                isShowMaxAndMinPoint = (max_i==0 || min_i==0)?NO:YES;
            }else{
                isShowMaxAndMinPoint = YES;
            }
        }
        [self addPoint:CGPointMake(xPosition, chartCavanHeight - grade * chartCavanHeight+UULabelHeight)
                 index:i
                isShow:isShowMaxAndMinPoint
                 value:firstValue rowValue:0];
        [progressline moveToPoint:CGPointMake(xPosition, chartCavanHeight - grade * chartCavanHeight+UULabelHeight)];
        [progressline setLineWidth:2.0];
        [progressline setLineCapStyle:kCGLineCapRound];
        [progressline setLineJoinStyle:kCGLineJoinRound];
        NSInteger index = 0;
        for (NSString * valueString in childAry) {
            
            float grade =([valueString floatValue]-_yValueMin) / ((float)_yValueMax-_yValueMin);
            if (index != 0) {
                
                CGPoint point = CGPointMake(xPosition+index*_xLabelWidth, chartCavanHeight - grade * chartCavanHeight+UULabelHeight);
                [progressline addLineToPoint:point];
                
                BOOL isShowMaxAndMinPoint = YES;
                if (self.showMaxMinArray) {
                    if ([self.showMaxMinArray[i] intValue]>0) {
                        isShowMaxAndMinPoint = (max_i==index || min_i==index)?NO:YES;
                    }else{
                        isShowMaxAndMinPoint = YES;
                    }
                }
                [progressline moveToPoint:point];
                [self addPoint:point
                         index:i
                        isShow:isShowMaxAndMinPoint
                         value:[valueString floatValue] rowValue:index];
            }
            index += 1;
        }
        
        _chartLine.path = progressline.CGPath;
        if ([[_colors objectAtIndex:i] CGColor]) {
            _chartLine.strokeColor = [[_colors objectAtIndex:i] CGColor];
        }else{
            _chartLine.strokeColor = kStrokeColor.CGColor;
        }
        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        pathAnimation.duration = childAry.count*0.4;
        pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
        pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
        pathAnimation.autoreverses = NO;
        //[_chartLine addAnimation:pathAnimation forKey:@"strokeEndAnimation"];
        
        _chartLine.strokeEnd = 1.0;
    }
}

- (void)addPoint:(CGPoint)point index:(NSInteger)index isShow:(BOOL)isHollow value:(CGFloat)value rowValue:(NSUInteger)row
{
    CustomChartView *view = [[CustomChartView alloc]initWithFrame:CGRectMake(5, 5, 8, 8)];
    view.value=value;
    NSString *labelText = _xLabels[row];
    view.date=labelText;
    
    view.center = point;
    view.layer.masksToBounds = YES;
    view.layer.cornerRadius = 4;
    view.layer.borderWidth = 2;
    view.layer.borderColor = [[_colors objectAtIndex:index] CGColor]?[[_colors objectAtIndex:index] CGColor]:kStrokeColor.CGColor;
    
    if (isHollow) {
        view.backgroundColor = kStrokeColor;
    }else{
        view.backgroundColor = [_colors objectAtIndex:index]?[_colors objectAtIndex:index]:kStrokeColor;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(point.x-UUTagLabelwidth/2.0, point.y-UULabelHeight*2, UUTagLabelwidth, UULabelHeight)];
        label.font = [UIFont systemFontOfSize:10];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = view.backgroundColor;
        label.text = [NSString stringWithFormat:@"%d",(int)value];
        [self addSubview:label];
        
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [view addGestureRecognizer:tapGestureRecognizer];
    [self addSubview:view];
}
-(void)handleTapFrom:(UITapGestureRecognizer *)sender
{
    
    CustomChartView *lable = (CustomChartView *)sender.view;
    if([customView superview])
        [customView removeFromSuperview];  
   
    NSString *title =lable.date;
    NSString *message =[NSString stringWithFormat:@"%@ %f",[TabBarSingleTon sharedObj].chartTypeFullString,lable.value];

    customView = [[CMPopTipView alloc] initWithTitle:title message:message];
    customView.backgroundColor = [UIColor whiteColor];
    customView.textColor = [UIColor blackColor];
    customView.animation = arc4random() % 2;
    customView.dismissTapAnywhere = YES;
    [customView autoDismissAnimated:YES atTimeInterval:3.0];
    [customView presentPointingAtView:lable inView:self animated:YES];
    
    
    
}
- (NSArray *)chartLabelsForX
{
    return [_chartLabelsForX allObjects];
}

@end
