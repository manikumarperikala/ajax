//
//  popUpView.h
//  MyNavi
//
//  Created by sridhar on 26/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomChartView : UIView
@property(nonatomic,assign)CGFloat value;
@property(nonatomic,strong)NSString *date;

@end
