//
//  DetailsCell.h
//  MyNavi
//
//  Created by sridhar on 26/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UILabel *detailLable;


@end
