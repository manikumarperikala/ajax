//
//  HomeDetailVC.m
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "DetailsVC.h"
#import "DetailsCell.h"
#import "TabViewController.h"
#import "VehicleObject.h"

@interface DetailsVC ()
{
    NSArray *titlesArray;
    NSArray *detailsArray;

}
@end

@implementation DetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tabBarController setTitle:@"Details"];
    [self getLocationName:APP_DELEGATE.vehicleObject.lat longitude:APP_DELEGATE.vehicleObject.lng];
    self.listView.rowHeight = UITableViewAutomaticDimension;
    self.listView.estimatedRowHeight = 140;
    self.listView.tableFooterView = [UIView new];
    
}

-(void)getLocationName:(NSString*)lat longitude:(NSString*)lng
{
    [APP_DELEGATE showActivityIndicatorOnFullScreen];
    NSString *api = [NSString stringWithFormat:kGeocodelocation,lat,lng];
    [HttpManager apiDetails:nil serviceName:api httpType:kGet  WithBlock:^(id responce, NSError *error) {
        if(!error)
        {
            NSDictionary *result = [responce objectForKey:@"result"];
            NSString *address = [result objectForKey:@"display_name"];
            [self getaddress:address];
        }
        else
        {
            [self getaddress:@"-"];
        }
    }];
}
-(void)getaddress: (NSString*)address
{
    dispatch_async(dispatch_get_main_queue(), ^{
        titlesArray = [[NSArray alloc]initWithObjects:@"Vehicle Number :",@"Model Code :",/*@"Device Number :",*/@"Engine Last On :",@"Total Engine Hours :",@"Last Data Captured On :",@"Last Location :", nil];
        detailsArray = [[NSArray alloc]initWithObjects:APP_DELEGATE.vehicleObject.vehicleNumber,APP_DELEGATE.vehicleObject.deviceModel,/*delegate.vehicleObject.deviceID,*/APP_DELEGATE.vehicleObject.engineLastOn,APP_DELEGATE.vehicleObject.totalEngineHours,APP_DELEGATE.vehicleObject.lastDataReceivedAt,address,nil];
        [self.listView reloadData];
        [APP_DELEGATE hideActivityIndcicator];
    });
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (titlesArray.count)
    {
        return titlesArray.count;
    }
    return 0;
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    DetailsCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[DetailsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.titleLable.text = [titlesArray objectAtIndex:indexPath.row];
    cell.detailLable.text = [detailsArray objectAtIndex:indexPath.row];
    
   
    return cell;
}

@end
