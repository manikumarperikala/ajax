//
//  EngineHoursObject.h
//  MyNavi
//
//  Created by sridhar on 03/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeartBeatObject : NSObject

@property(nonatomic,strong)NSString *time;
@property(nonatomic,strong)NSString *fuelLevel;
@property(nonatomic,strong)NSString *rpm;
@property(nonatomic,strong)NSString *coolantTemp;
@property(nonatomic,strong)NSString *batteryLevel;
@property(nonatomic,strong)NSString *ignitionStatus;
@property(nonatomic,strong)NSString *opsStatus;
@property(nonatomic,strong)NSString *travelSpeed;
@property(nonatomic,strong)NSString *parkingSwitch;
@property(nonatomic,strong)NSString *gearStatus;
@property(nonatomic,strong)NSString *key;
@property(nonatomic,strong)NSString *keyStatus;
@property(nonatomic,strong)NSString *hydraulicOilFilterStatus;
@property(nonatomic,strong)NSString *devicePublishTime;
@property(nonatomic,strong)NSString *fuelpersentage;
@property(nonatomic,assign) double latitude;
@property(nonatomic,assign) double longitude;
@property(nonatomic,strong)NSArray *translations;

@end
