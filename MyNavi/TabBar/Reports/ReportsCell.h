//
//  AllDistrictsCell.h
//  eRakshaPolice
//
//  Created by SridharReddy on 05/01/17.
//  Copyright © 2017 rocks & INC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLable;
@property (weak, nonatomic) IBOutlet UIImageView *imgLow;
@property (weak, nonatomic) IBOutlet UIImageView *imgNormal;
@property (weak, nonatomic) IBOutlet UIImageView *imgHigh;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Lheight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Nheight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *Hheight;
@end
