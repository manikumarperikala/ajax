//
//  HomeDetailVC.h
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HeartBeatObject;

@interface ReportDetailsVC : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *listView;
@property (strong, nonatomic) HeartBeatObject *heartBeatObject;

@end
