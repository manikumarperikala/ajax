//
//  HomeDetailVC.m
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ReportDetailsVC.h"
#import "DetailsCell.h"
#import "TabViewController.h"
#import "HeartBeatObject.h"
@interface ReportDetailsVC ()
{
    NSMutableArray *translations;
}
@end

@implementation ReportDetailsVC
@synthesize heartBeatObject;
- (void)viewDidLoad {
    [super viewDidLoad];

    self.listView.rowHeight = UITableViewAutomaticDimension;
    self.listView.estimatedRowHeight = 140;
    self.listView.tableFooterView = [UIView new];
    
    UIBarButtonItem *rightbarBtn = [[CustomControls sharedObj]rightBarButtonMethod:nil withTitle:@"Close" selector:@selector(popAction) delegate:self];
    rightbarBtn.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightbarBtn;
    
    translations = [[NSMutableArray alloc]init];
  
    
        NSDictionary *dict1 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Battery Level",@"label",heartBeatObject.batteryLevel,@"status", nil];
        [translations addObject:dict1];
    
     if (![[NSString stringWithFormat:@"%@",heartBeatObject.coolantTemp] isEqualToString:@"(null)"]){
         NSDictionary *dict2 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Coolent Temperature",@"label",[NSString stringWithFormat:@"%@ \u2103",heartBeatObject.coolantTemp],@"status", nil];
         [translations addObject:dict2];
     }
     if (![[NSString stringWithFormat:@"%@",heartBeatObject.fuelpersentage] isEqualToString:@"(null)"]){
         NSDictionary *dict3 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Fuel Percentage",@"label",[NSString stringWithFormat:@"%@ %@",heartBeatObject.fuelpersentage,@"%"],@"status", nil];
         [translations addObject:dict3];
     }

     if (![[NSString stringWithFormat:@"%@", heartBeatObject.hydraulicOilFilterStatus] isEqualToString:@"(null)"]){
         NSDictionary *dict4 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Hydraulic Oil Filter Status Status",@"label",heartBeatObject.hydraulicOilFilterStatus,@"status", nil];
         [translations addObject:dict4];
     }

    if (![[NSString stringWithFormat:@"%@", heartBeatObject.travelSpeed] isEqualToString:@"(null)"]){
        NSDictionary *dict4 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Travel Speed",@"label",[NSString stringWithFormat:@"%@ Kmph",heartBeatObject.travelSpeed],@"status", nil];
        [translations addObject:dict4];
    }
     if (![[NSString stringWithFormat:@"%@",heartBeatObject.ignitionStatus] isEqualToString:@"(null)"]){
         NSDictionary *dict5 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Engine Status",@"label",heartBeatObject.ignitionStatus,@"status", nil];
         [translations addObject:dict5];
     }

     if (![[NSString stringWithFormat:@"%@",heartBeatObject.keyStatus] isEqualToString:@"(null)"]){
         NSDictionary *dict6 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Key Status",@"label",heartBeatObject.keyStatus,@"status", nil];
         [translations addObject:dict6];
     }

     if (![[NSString stringWithFormat:@"%@",heartBeatObject.opsStatus] isEqualToString:@"(null) bar"]){
         NSDictionary *dict7 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Oil Pressure",@"label",heartBeatObject.opsStatus,@"status", nil];
         [translations addObject:dict7];
     }

     if (![[NSString stringWithFormat:@"%@",heartBeatObject.parkingSwitch] isEqualToString:@"(null)"]){
         NSDictionary *dict8 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Parking Switch",@"label",heartBeatObject.parkingSwitch,@"status", nil];
         [translations addObject:dict8];
     }

     if (![heartBeatObject.rpm isEqualToString:@"0"]||![[NSString stringWithFormat:@"%@",heartBeatObject.rpm] isEqualToString:@"(null)"]){
         NSDictionary *dict9 = [[NSDictionary alloc]initWithObjectsAndKeys:@"RPM",@"label",heartBeatObject.rpm,@"status", nil];
         [translations addObject:dict9];
     }

    
}

-(void)popAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return translations.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    DetailsCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[DetailsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary *dict = [translations objectAtIndex:indexPath.row];
    cell.titleLable.text = [dict objectForKey:@"label"];
    cell.detailLable.text = [dict objectForKey:@"status"];
    
       return cell;
}

@end
