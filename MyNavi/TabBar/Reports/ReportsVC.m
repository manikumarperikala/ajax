//
//  EngineHoursVC.m
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "ReportsVC.h"
#import "ReportsCell.h"
#import "HeartBeatObject.h"
#import "SVPullToRefresh.h"
#import "ReportDetailsVC.h"

#define Norecords -1

@interface ReportsVC ()
{
    UIButton *previousBtn;
    AppDelegate *delegate;
    NSMutableArray *reportsArray;
    NSString *viewtype;
    int pageNumber;
    BOOL isAlertType;
}
@end

@implementation ReportsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewtype = @"heartbeat";
    pageNumber = 1;
    previousBtn = _alertButton;
    
    reportsArray = [[NSMutableArray alloc]init];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

 [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchAction) name:@"searchAction" object:nil];
    [self getHeartBeat];
    
    __weak ReportsVC *weakSelf = self;
    [_reportsGridView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    // Do any additional setup after loading the view.
}
-(void)searchAction
{
//    if([TabBarSingleTon.sharedObj compareDates]){
        pageNumber = 1;
        [reportsArray removeAllObjects];
        [self getHeartBeat];
    /*}
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    
    }*/
}

- (void)insertRowAtBottom {
    if(pageNumber != Norecords)
    {
        pageNumber+=1;
        [self getHeartBeat];
    }
    else
        [_reportsGridView.infiniteScrollingView stopAnimating];
    
    
}
-(void)getHeartBeat
{
    NSString *heartBeat = [NSString stringWithFormat:Reports,viewtype,delegate.vehicleObject.deviceID,[TabBarSingleTon sharedObj].endDate,pageNumber,[TabBarSingleTon sharedObj].startDate];
    
    if(!reportsArray.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:heartBeat httpType:kGet  WithBlock:^(id responce, NSError *error) {
        
        if(!error)
        {
            NSArray *rows = [responce objectForKey:@"rows"];
            if(rows.count)
            {
                for(NSDictionary *dict in rows )
                {
                    NSDictionary *extradict = [dict objectForKey:@"extras"];
                    HeartBeatObject *obj = [[HeartBeatObject alloc]init];
                   // obj.time = [dict objectForKey:@"devicePublishTime"];
                    obj.time = [[CustomControls sharedObj] addFiveHoursthirtymins:[dict objectForKey:@"devicePublishTime"]];
                    
                    if([[NSString stringWithFormat:@"%@",[extradict objectForKey:@"ignitionStatus"]] isEqualToString:@"0"])
                        obj.ignitionStatus = @"OFF";
                    else
                        obj.ignitionStatus = @"ON";
                    
                    obj.travelSpeed = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"travelSpeed"]];
                    
                    obj.opsStatus = [NSString stringWithFormat:@"%@ bar",[extradict objectForKey:@"opsStatus"]];
        
                    obj.parkingSwitch = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"parkingSwitch"]];
                    obj.gearStatus = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"gearStatus"]];
                    obj.fuelLevel = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"fuelLevel"]];
                    if ([obj.fuelLevel  isEqual: 0])
                    {
                        obj.fuelLevel = @"0 ltr";
                    }
                    else
                    {
                        obj.fuelLevel = [obj.fuelLevel stringByAppendingString:@" ltr"];
                    }
                    obj.rpm = [NSString stringWithFormat:@"%@",[NSNumber numberWithDouble:trunc([[extradict objectForKey:@"rpm"]doubleValue])]];
                    if([extradict objectForKey:@"coolantTemp"] != nil)
                    obj.coolantTemp = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"coolantTemp"]];
                    if(![[NSString stringWithFormat:@"%@",[extradict objectForKey:@"batteryLevel"]] isEqualToString:@"(null)"])
                    {
                        obj.batteryLevel = [NSString stringWithFormat:@"%@ V",[extradict objectForKey:@"batteryLevel"]];
                    }
                    else
                    {
                        obj.batteryLevel = @"0 V";
                    }
                    if([extradict objectForKey:@"keyStatus"] != nil)
                    {
                        obj.keyStatus = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"keyStatus"]];
                    }
                    if([extradict objectForKey:@"fuelpersentage"] != nil)
                    {
                        obj.fuelpersentage = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"fuelpersentage"]];
                    }
                    if([extradict objectForKey:@"hydraulicOilFilterStatus"] != nil)
                    {
                        obj.hydraulicOilFilterStatus = [NSString stringWithFormat:@"%@",[extradict objectForKey:@"hydraulicOilFilterStatus"]];
                    }
                    if ([(NSArray*)[dict objectForKey:@"translations"] isEqual:[NSNull null]])
                        obj.translations = [NSArray array];
                    else
                        obj.translations = [dict objectForKey:@"translations"];
                    
                    [reportsArray addObject:obj];
                    obj = nil;
                }
            }
            else
            {
                pageNumber = Norecords;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_reportsGridView.infiniteScrollingView stopAnimating];
                [_reportsGridView reloadData];
            });
        }
    }];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5,0, 5,0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = (IDIOM == IPAD)?64:44;
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/5-10,height );
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    
    if (reportsArray.count) {
        collectionView.backgroundView = nil;
        return reportsArray.count;
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:collectionView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        collectionView.backgroundView = messageLabel;
    }
    
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionview cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReportsCell *cell = (ReportsCell *) [collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    HeartBeatObject *obj = [reportsArray objectAtIndex:indexPath.section];

    if(indexPath.row %5 == 0)
        cell.dateLable.text = [self getDateString:obj.time];
    else if(indexPath.row%5 == 1)
    {
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.fuelLevel];
        if([cell.dateLable.text isEqualToString:@"(null)"])
        {
            cell.dateLable.text = @"NA";
        }
    }
    else if(indexPath.row%5 == 2)
        cell.dateLable.text = [NSString stringWithFormat:@"%ld",(long)[obj.rpm integerValue]];
    else if(indexPath.row%5 == 3)
        cell.dateLable.text = [NSString stringWithFormat:@"%ld",(long)[obj.coolantTemp integerValue]];
    else if(indexPath.row%5 == 4)
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.batteryLevel];
    

    if(indexPath.row%5 == 0)
        [cell.dateLable setTextColor:[UIColor colorWithRed:0.4862 green:0.5764 blue:0.7450 alpha:1]];
    else
        [cell.dateLable setTextColor:[UIColor blackColor]];
    
   
        return cell;
}

-(NSString *)getDateString:(NSString *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *date = [dateFormatter dateFromString:sender];
    [dateFormatter setDateFormat:@"yyyy/MM/dd\nHH:mm:ss"];
    NSString *convertedDateString = [dateFormatter stringFromDate:date];
    return convertedDateString;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSArray *translations = [(HeartBeatObject*)[reportsArray objectAtIndex:indexPath.section] translations];
    
   
        if(indexPath.row%5 == 0)
        {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ReportDetailsVC *reportDetailsVCObj = (ReportDetailsVC *)[storyBoard instantiateViewControllerWithIdentifier:@"ReportDetailsVC"];
            if(isAlertType)
                reportDetailsVCObj.title = @"Alert Status";
            else
                reportDetailsVCObj.title = @"Detailed Status";
            
            reportDetailsVCObj.heartBeatObject = [reportsArray objectAtIndex:indexPath.section];
            UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:reportDetailsVCObj];
            [self.navigationController presentViewController:navi animated:YES completion:nil];
        }
    /*else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Report" message:@"Report detail not available" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }*/
}

-(IBAction)selectReportsType:(UIButton *)sender
{
    if(![sender.titleLabel.text isEqualToString:previousBtn.titleLabel.text])
    {
        if([sender.titleLabel.text isEqualToString:@"Alert View"])
        {
            [sender setImage:[UIImage imageNamed:@"bullet_selection"] forState:UIControlStateNormal];
            [previousBtn setImage:[UIImage imageNamed:@"bullet-1"] forState:UIControlStateNormal];
            viewtype = @"findalerts";
            isAlertType = YES;
        }
        else
        {
            [sender setImage:[UIImage imageNamed:@"bullet_selection"] forState:UIControlStateNormal];
            [previousBtn setImage:[UIImage imageNamed:@"bullet-1"] forState:UIControlStateNormal];
            viewtype = @"heartbeat";
            isAlertType = NO;
        }
        [reportsArray removeAllObjects];
        previousBtn = sender;
        pageNumber = 1;
        [self getHeartBeat];
    }
}

@end
