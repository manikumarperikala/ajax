//
//  EngineHoursVC.h
//  MyNavi
//
//  Created by sridhar on 27/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportsVC : UIViewController
-(IBAction)selectReportsType:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *reportsGridView;
@property (weak, nonatomic) IBOutlet UIButton *alertButton;
@end
