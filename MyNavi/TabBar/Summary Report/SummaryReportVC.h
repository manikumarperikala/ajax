//
//  SummaryReportVC.h
//  MyNavi
//
//  Created by TrakitNow on 10/29/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryReportVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIView *fromDate;
@property (weak, nonatomic) IBOutlet UIView *daytype;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UICollectionView *reportsGridView;

@end
