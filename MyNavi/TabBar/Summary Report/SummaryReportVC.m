//
//  SummaryReportVC.m
//  MyNavi
//
//  Created by TrakitNow on 10/29/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import "SummaryReportVC.h"
#import "DatePicker.h"
#import "CustomPickerView.h"
#import "CustomControls.h"
#import "SVPullToRefresh.h"
#import "ReportsCell.h"
#import "ReportObject.h"

#define Norecords -1

@interface SummaryReportVC ()<UICollectionViewDelegate,UICollectionViewDataSource,DatePickerProtocal,PickerViewProtocal>
{
    AppDelegate *delegate;
    DatePicker *datePickerView;
    CustomPickerView *customPickerViewObj;
    NSString *pickerViewSelectedString;
    int selectedRow;
    NSDate *selectedDate;
    NSMutableArray *reportsArray;
    int pageNumber;
}
@end


@implementation SummaryReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageNumber = 1;
    reportsArray = [[NSMutableArray alloc]init];
    _fromDate.layer.cornerRadius = 5.0f;
    _fromDate.layer.masksToBounds = YES;
    _fromDate.layer.borderColor = [[UIColor blackColor]CGColor];
    _fromDate.layer.borderWidth = 1.0f;
    
    _daytype.layer.cornerRadius = 5.0f;
    _daytype.layer.masksToBounds = YES;
    _daytype.layer.borderColor = [[UIColor blackColor]CGColor];
    _daytype.layer.borderWidth = 1.0f;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSDate *date = [NSDate date];
    selectedDate = date;
    _dateLbl.text = [formatter stringFromDate:date];
    _typeLbl.text = @"Day";
    
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController setTitle:[NSString stringWithFormat:@"%@ (%@)",delegate.vehicleObject.deviceModel,delegate.vehicleObject.vehicleNumber]];
    pickerViewSelectedString = @"Day";
    selectedRow = 0;
    UITapGestureRecognizer *fromDateGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(datePickerAction:)];
    fromDateGesture.numberOfTapsRequired = 1;
    _fromDate.tag = 12;
    [_fromDate addGestureRecognizer:fromDateGesture];
    
    UITapGestureRecognizer *DatetypeGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sendDatetype:)];
    DatetypeGesture.numberOfTapsRequired = 1;
    _daytype.tag = 13;
    [_daytype addGestureRecognizer:DatetypeGesture];
    
    [_searchButton addTarget:self action:@selector(searchAction:) forControlEvents:UIControlEventTouchUpInside];
    [self getSummaryReport:@"Day" row:selectedRow];
     __weak SummaryReportVC *weakSelf = self;
 /*   [_reportsGridView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];*/
    
}
- (void)datePickerAction:(UITapGestureRecognizer*)sender {
    
    if(![datePickerView superview])
    {
        datePickerView = [[DatePicker alloc]initWithFrame:CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height - 110)];
        datePickerView.selectedButton = sender.view;
        datePickerView.delegate = self;
        [datePickerView setPickerWithTag:(int)sender.view.tag];
        [self.view addSubview:datePickerView];
    }
}
-(void)sendDatetype:(UITapGestureRecognizer*)sender
{
    NSArray *array = @[@"Day",@"Month",@"Year"];
    if(![customPickerViewObj superview])
    {
        customPickerViewObj = [[CustomPickerView alloc]initWithFrame:CGRectMake(0, 110, self.view.frame.size.width, self.view.frame.size.height-110)];
        customPickerViewObj.dataArray = array;
        customPickerViewObj.delegate = self;
        [customPickerViewObj setSelectedValue:(int)selectedRow];
        [self.view addSubview:customPickerViewObj];
    }
}

-(void)datePickerSelectedDate:(NSDate *)sender forTag:(int)tag
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSString *dateString = [formatter stringFromDate:sender];
    [(UILabel*)datePickerView.selectedButton.subviews[1] setText:dateString];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    selectedDate = sender;
    dateString = [formatter stringFromDate:sender];
    NSLog(@"%@",dateString);
    [formatter setDateFormat:@"MMM dd, yyyy"];
    NSString *datestr = [formatter stringFromDate:sender];
    _dateLbl.text = datestr;
    datePickerView = nil;
}
-(void)pickerViewSelectedValue:(NSString *)sender forRow:(int)row{
    NSLog(@"%@%d",sender,row);
    _typeLbl.text = sender;
    selectedRow = row;
}
-(void)searchAction:(UIButton *)sender
{
    pageNumber = 1;
    [reportsArray removeAllObjects];
    [self getSummaryReport:_typeLbl.text row:selectedRow];
    
}

- (void)insertRowAtBottom {
    if(pageNumber != Norecords)
    {
        pageNumber+=1;
         [self getSummaryReport:_typeLbl.text row:selectedRow];
    }
    else
        [_reportsGridView.infiniteScrollingView stopAnimating];
}
-(void)getSummaryReport:(NSString *)type row:(int)row
{
    NSString *beggingOfDay = [[CustomControls sharedObj] beginningOfDay:selectedDate];
    NSString *endOfDay = [[CustomControls sharedObj]endOfDay:selectedDate];
    NSString *beggingOfMonth = [[CustomControls sharedObj]beggingOfMonth:selectedDate];
    NSString *endOfMonth = [[CustomControls sharedObj]endOfMonth:selectedDate];
    NSString *beggingOfYear = [[CustomControls sharedObj]beggingOfYear:selectedDate];
    NSString *endOfYear = [[CustomControls sharedObj]endOfYear:selectedDate];
    NSString *pinno = delegate.vehicleObject.pinno;
    
    switch (row) {
        case 0:
            [self getReports:beggingOfDay enddate:endOfDay pinno:pinno datetype:@""];
            break;
        case 1:
            [self getReports:beggingOfMonth enddate:endOfMonth pinno:pinno datetype:@"day"];
            break;
        case 2:
            [self getReports:beggingOfYear enddate:endOfYear pinno:pinno datetype:@"month"];
            break;
        default:
            break;
    }
}

-(void)getReports:(NSString *)startdate enddate:(NSString *)end pinno:(NSString *)pin datetype:(NSString *)type
{
    NSString *reports = nil;
    if ([type isEqualToString:@""])
    {
        reports = [NSString stringWithFormat:SummaryReportDay,pin,end,startdate];
    }
    else
    {
       reports = [NSString stringWithFormat:SummaryReport,pin,end,startdate,type];
    }
    if(!reportsArray.count)
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [HttpManager apiDetails:nil serviceName:reports httpType:kGet  WithBlock:^(id responce, NSError *error) {
        
        if(!error)
        {
            NSArray *rows = [responce objectForKey:@"rows"];
            if(rows.count)
            {
                for(NSDictionary *dict in rows )
                {
                    ReportObject *obj = [[ReportObject alloc]init];
                    if ([type isEqualToString:@""])
                    {
                        obj.date = [NSString stringWithFormat:@"%@-%@-%@",[[dict objectForKey:@"_id"]objectForKey:@"day"],[[dict objectForKey:@"_id"] objectForKey:@"month"],[[dict objectForKey:@"_id"] objectForKey:@"year"]];
                    }
                    else
                    {
                        if ([type isEqualToString:@"day"])
                        {
                             obj.date = [NSString stringWithFormat:@"%@-%@-%@",[[dict objectForKey:@"_id"]objectForKey:@"day"],[[dict objectForKey:@"_id"] objectForKey:@"month"],[[dict objectForKey:@"_id"] objectForKey:@"year"]];
                        }
                        else
                        {
                            
                             NSNumber *month = [[dict objectForKey:@"_id"] objectForKey:@"month"];
                             obj.date = [NSString stringWithFormat:@"%@-%@",[self MonthNameString: [month intValue]],[[dict objectForKey:@"_id"] objectForKey:@"year"]];
                        }
                    }
                    
                    obj.enginehours = [NSString stringWithFormat: @"%@", [dict objectForKey:@"engineHours"]];
                    obj.totaldistance = [NSString stringWithFormat:@"%@",[dict objectForKey:@"totalDistance"]];
                    //coolant Time
                    
                    NSNumber *coolantlow = ([[dict objectForKey:@"coolantTempTime"] objectForKey:@"low"]);
                    NSNumber *coolantnormal = ([[dict objectForKey:@"coolantTempTime"] objectForKey:@"normal"]);
                    NSNumber *coolanthigh = ([[dict objectForKey:@"coolantTempTime"] objectForKey:@"high"]);
                    NSNumber *rpmlow = ([[dict objectForKey:@"rpmTime"] objectForKey:@"low"]);
                    NSNumber *rpmnormal = ([[dict objectForKey:@"rpmTime"] objectForKey:@"normal"]);
                    NSNumber *rpmhigh = ([[dict objectForKey:@"coolantTempTime"] objectForKey:@"high"]);
                    //coolant
                    obj.coolantlow = coolantlow;
                    obj.coolantnormal = coolantnormal;
                    obj.coolanthigh = coolanthigh;
                    // rpm
                    obj.rpmlow = rpmlow;
                    obj.rpmnormal = rpmnormal;
                    obj.rpmhigh = rpmhigh;
                    
                    [reportsArray addObject:obj];
                    obj = nil;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_reportsGridView.infiniteScrollingView stopAnimating];
                    [_reportsGridView reloadData];
                });
            }
            else
            {
                pageNumber = Norecords;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_reportsGridView.infiniteScrollingView stopAnimating];
                [_reportsGridView reloadData];
            });
            
        }
    }];
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5,0, 5,0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = (IDIOM == IPAD)?120:70;
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/5-10,height );
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    
    if (reportsArray.count) {
        collectionView.backgroundView = nil;
        return reportsArray.count;
        
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:collectionView.frame];
        messageLabel.text = @"No data available.";
        messageLabel.textAlignment = NSTextAlignmentCenter;
        collectionView.backgroundView = messageLabel;
    }
    
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionview cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReportsCell *cell = (ReportsCell *) [collectionview dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    ReportObject *obj = [reportsArray objectAtIndex:indexPath.section];
    
    if(indexPath.row %5 == 0){
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.date];
        cell.Lheight.constant = 0;
        cell.Nheight.constant = 0;
        cell.Hheight.constant = 0;
        cell.imgLow.image = nil;
        cell.imgNormal.image = nil;
        cell.imgHigh.image = nil;
    }
    else if(indexPath.row%5 == 1)
    {
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.enginehours];
        cell.imgLow.image = nil;
        cell.imgNormal.image = nil;
        cell.imgHigh.image = nil;
        cell.Lheight.constant = 0;
        cell.Nheight.constant = 0;
        cell.Hheight.constant = 0;
    }
    else if(indexPath.row%5 == 2)
    {
        cell.dateLable.text = [NSString stringWithFormat:@"%@ %@ %@",[self timeFormatted:[obj.rpmlow intValue]],[self timeFormatted:[obj.rpmnormal intValue]],[self timeFormatted:[obj.rpmhigh intValue]]];
        cell.imgLow.image = [UIImage imageNamed:@"ImageLow"];
        cell.imgNormal.image = [UIImage imageNamed:@"ImageNormal"];
        cell.imgHigh.image = [UIImage imageNamed:@"ImageHigh"];
        cell.Lheight.constant = 15;
        cell.Nheight.constant = 15;
        cell.Hheight.constant = 15;
        
    }
    else if(indexPath.row%5 == 3)
    {
        cell.dateLable.text = [NSString stringWithFormat:@"%@ %@ %@",[self timeFormatted:[obj.coolantlow intValue]],[self timeFormatted:[obj.coolantnormal intValue]],[self timeFormatted:[obj.coolanthigh intValue]]];
        cell.imgLow.image = [UIImage imageNamed:@"ImageLow"];
        cell.imgNormal.image = [UIImage imageNamed:@"ImageNormal"];
        cell.imgHigh.image = [UIImage imageNamed:@"ImageHigh"];
        cell.Lheight.constant = 15;
        cell.Nheight.constant = 15;
        cell.Hheight.constant = 15;
        
    }
    
    else if(indexPath.row%5 == 4)
    {
        cell.dateLable.text = [NSString stringWithFormat:@"%@",obj.totaldistance];
        cell.imgLow.image = nil;
        cell.imgNormal.image = nil;
        cell.imgHigh.image = nil;
        cell.Lheight.constant = 0;
        cell.Nheight.constant = 0;
        cell.Hheight.constant = 0;
    }
    
    
//    if(indexPath.row%5 == 0)
//        [cell.dateLable setTextColor:[UIColor colorWithRed:0.4862 green:0.5764 blue:0.7450 alpha:1]];
//    else
//        [cell.dateLable setTextColor:[UIColor blackColor]];
    return cell;
}
-(NSString*)MonthNameString:(int)monthNumber
{
    NSDateFormatter *formate = [NSDateFormatter new];
    
    NSArray *monthNames = [formate shortStandaloneMonthSymbols];
    
    NSString *monthName = [monthNames objectAtIndex:(monthNumber - 1)];
    
    return monthName;
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
