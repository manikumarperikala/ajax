//
//  ReportObject.h
//  MyNavi
//
//  Created by TrakitNow on 10/30/19.
//  Copyright © 2019 Ramakrishna. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ReportObject : NSObject

@property(nonatomic,strong) NSString *date;
@property(nonatomic,strong) NSString *enginehours;
@property(nonatomic,strong) NSString *totaldistance;
@property(nonatomic,assign) NSNumber *rpmlow;
@property(nonatomic,assign) NSNumber *rpmhigh;
@property(nonatomic,assign) NSNumber *rpmnormal;
@property(nonatomic,assign) NSNumber* coolantlow;
@property(nonatomic,assign) NSNumber* coolanthigh;
@property(nonatomic,assign) NSNumber* coolantnormal;
@end


