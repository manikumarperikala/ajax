//
//  ViewController.m
//  MyNavi
//
//  Created by sridhar on 21/04/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    BOOL keyBoardEndCalled;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // CODE FOR APP STORE VERSION CHECK
   /* [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [self needsUpdate:^(NSDictionary *dictionary) {
       
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        if ([dictionary[@"resultCount"] integerValue] == 1){
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP_DELEGATE hideActivityIndcicator];
            });
            NSString* appStoreVersion = dictionary[@"results"][0][@"version"];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            if (![appStoreVersion isEqualToString:currentVersion]){
                NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                [self showAlert];
            }
            else
            {
                NSLog(@"App is Uptodate");
            }
            
        }
        
        // if you want to update UI or model, dispatch this to the main queue:
        // dispatch_async(dispatch_get_main_queue(), {
        // do your UI stuff here
        //    do nothing
        // });
    }];*/
    
    keyBoardEndCalled = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.contentScrollView.contentInset = UIEdgeInsetsMake(0,0,0,0);
    [self.navigationController setNavigationBarHidden:YES];

//    _emailTextField.text=@"vijaytalluri";
//    _passwordTextField.text=@"test";
    self.title = @"Login";
    
    [_passwordView.layer setCornerRadius:5.0f];
    [_usernameView.layer setCornerRadius:5.0f];
    [_loginButton.layer setCornerRadius:5.0f];



    // Do any additional setup after loading the view, typically from a nib.
}

-(void)needsUpdate:(void (^)(NSDictionary * dictionary))completionHandler
{
    NSString *identifier = [[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSString *urlString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@",identifier];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                      
                                      if (completionHandler) {
                                          completionHandler(lookup);
                                      }
                                  }];
    
    [task resume];
}
-(void)showAlert
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Please Update app"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Update!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    @try
                                    {
                                        NSLog(@"tapped ok");
                                        BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                        if (canOpenSettings)
                                        {
                                            NSURL *url = [NSURL URLWithString:@"itms://itunes.apple.com/app/apple-store/id1434687527?mt=8"];
                                            
                                            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                                        }
                                    }
                                    @catch (NSException *exception)
                                    {
                                        
                                    }
                                }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
   /* UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:alertController animated:YES completion:nil];*/
    
}

- (IBAction)loginButtonAction:(id)sender {
    
    [self dismissKeyboard];
    
    if([self areAllFieldValid])
    {
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
        NSMutableString *credentials = [[NSMutableString alloc] initWithFormat:@"%@:%@", _emailTextField.text, _passwordTextField.text];
        [APP_DELEGATE showActivityIndicatorOnFullScreen];
        [HttpManager loginApiAuthorization:credentials WithBlock:^(id responce, NSError *error) {
            if(!error)
            {
                NSString *status = [responce objectForKey:@"status"];
                if ([status  isEqual: @"success"])
                {
                    [kUserDefaults setBool:YES forKey:@"logged_in"];
                    [self registerDeviceforPushNotifications];
                    [controls saveUserInfo:[responce objectForKey:@"userInfo"]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *role = [[responce objectForKey:@"userInfo"] valueForKey:@"useType"];
                        if([role isEqualToString:kDealerID])
                        {
                            [self setRootView:kDealerDashboard forStoryBoard:kDealerIdentifier];
                        }
                        else if([role isEqualToString:kCustomerID])
                        {
                            [self setRootView:kCustomerDashboard forStoryBoard:kCustomerIdentifier];
                        }
                        else if([role isEqualToString:kClientID])
                        {
                            [self setRootView:kClientDashboard forStoryBoard:kClientIdentifier];
                        }
                        else
                        {
                            [self setRootView:kServiceManDashboard forStoryBoard:kServiceManIdentifier];
                           
                        }
                    });
                }
                else
                {
                  [controls presentAlertController:status delegate:self];
                }
                
            }
            else
            {
                NSLog(@"%@",error);
            }
        }];
    }
}

-(void)registerDeviceforPushNotifications
{
    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] != nil)
        
        [HttpManager apiDetails:@{@"deviceId":deviceID,@"pushToken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],@"platform":@"IOS"} serviceName:[NSString stringWithFormat:@"%@",DeviceDataApi] httpType:@"POST" WithBlock:^(id responce, NSError *error) {
            NSLog(@"%@RESPONSE:",responce);
        }];
}

-(void)setRootView:(NSString*)storyBoardName forStoryBoard:(NSString*)name
{
     SWRevealViewController *revealController = [self revealViewController];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];
    UINavigationController *navigationController = (UINavigationController *)[storyBoard instantiateViewControllerWithIdentifier:name];
    [revealController setFrontViewController:navigationController animated:YES];
    
}
-(BOOL)areAllFieldValid{
    
    _emailTextField.text  = [_emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([_emailTextField.text length]<1) {
        [controls presentAlertController:@"Please enter Username." delegate:self];
        [_emailTextField becomeFirstResponder];
        return FALSE;
    }
    
    _passwordTextField.text  = [_passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([_passwordTextField.text length]<1) {
        [controls presentAlertController:@"Please enter Password." delegate:self];
        [_passwordTextField becomeFirstResponder];
        return FALSE;
    }
    return TRUE;
}
-(void)dismissKeyboard
{
    if([_emailTextField isFirstResponder]) //check
        [_emailTextField resignFirstResponder];
    
    
    if([_passwordTextField isFirstResponder]) //check
        [_passwordTextField resignFirstResponder];
}

-(void)tecladoON:(NSNotification *)notification
{
    if(keyBoardEndCalled)
    {
        CGFloat  keyBoardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        keyBoardEndCalled=NO;
        CGFloat height=_contentScrollView.contentSize.height+keyBoardHeight;
        [_contentScrollView setContentSize:CGSizeMake(self.view.frame.size.width, height)];
    }
    
}
-(void)tecladoOFF:(NSNotification *)notification
{
    if(!keyBoardEndCalled)
    {
        CGFloat  keyBoardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        
        keyBoardEndCalled=YES;
        CGFloat height=_contentScrollView.contentSize.height-keyBoardHeight;
        [_contentScrollView setContentSize:CGSizeMake(self.view.frame.size.width, height)];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [self.navigationController setNavigationBarHidden:NO];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tecladoOFF:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tecladoON:) name:UIKeyboardWillShowNotification object:nil];
    
   /* [APP_DELEGATE showActivityIndicatorOnFullScreen];
    [self needsUpdate:^(NSDictionary *dictionary) {
        
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        if ([dictionary[@"resultCount"] integerValue] == 1){
            dispatch_async(dispatch_get_main_queue(), ^{
                [APP_DELEGATE hideActivityIndcicator];
            });
            NSString* appStoreVersion = dictionary[@"results"][0][@"version"];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
            if (![appStoreVersion isEqualToString:currentVersion]){
                NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                [self showAlert];
            }
            else
            {
                NSLog(@"App is Uptodate");
            }
            
        }
        
        // if you want to update UI or model, dispatch this to the main queue:
        // dispatch_async(dispatch_get_main_queue(), {
        // do your UI stuff here
        //    do nothing
        // });
    }];*/
}
- (IBAction)forgotPasswordAction:(id)sender {
}


@end
